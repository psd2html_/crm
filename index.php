<?php
// change the following paths if necessary
$yii=dirname(__FILE__).'/../yii/yii.php';

$config=dirname(__FILE__).'/protected/config/main.php';

if ($_SERVER['HTTP_HOST'] == 'crm.local')
    defined('YII_DEBUG') or define('YII_DEBUG', true);
else
    defined('YII_DEBUG') or define('YII_DEBUG', false);



// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

if ($_SERVER['HTTP_HOST'] == 'crm.local')
    defined('YII_LOCAL') or define('YII_LOCAL', true);
else
    defined('YII_LOCAL') or define('YII_LOCAL', false);

require_once($yii);
Yii::createWebApplication($config)->run();