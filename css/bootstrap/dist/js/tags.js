var tagBlock = {
    cms: ['cms',

        {
            amiro: ['amiro', 'amiro_cms', 'amiro cms', 'амиро'],
            bitrix: ['bitrix', 'битрикс'],
            cs_cart: ['cs-cart', 'cscart'],
            diafan: ['diafan', 'definitie', 'диафан'],
            dle: ['dle', 'datalife', 'data life'],
            dotnetnuke: ['dotnetnuke', 'dnn', 'дотнетнуке'],
            drupal: ['drupal', 'друпал'],
            hostcms: ['hostcms', 'host cms', 'хост цмс'],
            imagecms: ['imagecms', 'image cms', 'имаге цмс'],
            insales: ['insales'],
            instantcms: ['instantcms', 'инстант цмс'],
            ipb: ['ipb'],
            joomla: ['joomla', 'джумла'],
            joostina: ['joostina', 'джостина'],
            livestreet: ['livestreet', 'live street'],
            magento: ['magento', 'мадженто'],
            modx: ['modx', 'модх'],
            netcat: ['netcat', 'net cat'],
            opencart: ['opencart', 'опенкарт', 'опэнкарт'],
            oscommerce: ['oscommerce'],
            phpbb: ['phpbb', 'php bb'],
            phpshop: ['phpshop', 'php shop'],
            prestashop: ['prestashop', 'presta shop'],
            shop_script: ['shop-script', 'shop script'],
            simpla: ['simpla', 'симпла'],
            siteedit: ['siteedit', 'site edit', 'сайт эдит'],
            sugarcrm: ['SugarCRM'],
            typo3: ['typo3', 'typo 3'],
            ubercart: ['ubercart'],
            ucoz: ['ucoz'],
            umi: ['umi', {
                xslt: ['xslt']
            }],
            vamshop: ['vamshop'],
            vbulletin: ['vbulletin'],
            virtuemart: ['virtuemart', 'virtue mart'],
            webasyst: ['webasyst', 'web asyst'],
            wp: ['wp', 'wordpress', 'word press', 'вордпрес'],
            zen_cart: ['zen cart', 'зен карт', 'зэн карт']
        }
    ],

    fw: ['framework', {
        cakephp: ['cakephp'],
        codeigniter: ['codeigniter', 'кодеигнайтер'],
        kohana: ['kohana', 'кохана'],
        laravel: ['laravel', 'кодигнайтер'],
        symfony: ['symfony', 'симфони'],
        yii: ['yii'],
        zend: ['zend', 'зенд']
    }],
    self: ['самопис'],
    pageProof: ['pageProof', {
        adapted: ['адаптивная'],
        validation: ['validation', 'валидная'],
        cross_browser: ['cross-browser', 'кроссбраузерная'],
        table: ['table', 'табличная'],
        div_design: ['div-design', 'дивная']
    }],
    design: ['дизайн', {
        corporate_identity: ['фирменный стиль'],
        banners: ['баннеры'],
        print_design: ['полиграфический дизайн'],
        outdoor_advertising: ['наружная реклама'],
        site_design: ['дизайн сайтов'],
        interfaces: ['интерфейсы'],
        infographics: ['инфографика'],
        logos: ['логотипы'],
        icons: ['иконки'],
        psd: ['.psd']
    }],
    changes: ['правка/доработка', 'исправлен'],
    fullProject: ['под ключ', {
        cutaway: ['визитка'],
        corporate: ['корпоративный'],
        blog: ['блог'],
        onlineShop: ['интернет-магазин'],
        portal: ['портал'],
        landing: ['лендинг']
    }],
    crm: ['crm', {
        erp: ['erp', 'ерп']
    }],
    js: ['javascript', {
        angularjs: ['angular.js'],
        nodejs: ['node.js'],
        emberjs: ['ember.js'],
        backbonejs: ['backbone.js']
    }],
    jquery: ['jquery'],
    scripts: ['скрипты'],
    modules: ['модули'],
    withzero: ['с нуля'],
    ajax: ['ajax'],
    xhtml: ['xhtml'],
    webapplication: ['веб-приложения'],
    audit: ['аудит'],
    socnet: ['соц. сети', {
        facebook: ['facebook'],
        vkontakte: ['vkontakte', 'вконтакте'],
        googleplus: ['google+', 'гугл плюс'],
        twitter: ['twitter', 'твиттер'],
        instagram: ['instagram', 'инстаграм']
    }],
    testing: ['тестирование'],
    serverconfig: ['настройка сервера'],
    cmsdevelopment: ['разработка cms'],
    drafting: ['составление тз'],
    google: ['google', {
        android: ['android', 'андройд']
    }],
    ruby: ['ruby', {
        rubyonrails: ['rubyonrails']
    }],
    smarty: ['smarty'],
    shopify: ['shopify'],
    apple: ['apple', {
        ipad: ['ipad', 'айпэд'],
        iphone: ['iphone', 'айфон'],
        objectivec: ['objective-c'],
        swift: ['swift']
    }],
    database: ['database', {
        mysql: ['mysql'],
        postgressql: ['postgressql'],
        plsql: ['plsql']
    }],
    python: ['python', {
        django: ['django', 'джанго'],
        flask: ['flask']
    }],
    aspnet: ['asp.net'],
    bugs: ['bugs'],
    viruses: ['viruses'],
    parsers: ['parsers'],
    html5: ['html5'],
    php: ['php'],
    names: {
        cms: 'CMS',
			amiro: '/Amiro',
			bitrix: '/Bitrix',
			cs_cart: '/CS-Cart',
			diafan: '/Diafan',
			dle: '/DLE',
			dotnetnuke: '/DotNetNuke',
			drupal: '/Drupal',
			hostcms: '/HostCMS',
			imagecms: '/ImageCMS',
			insales: '/InSales',
			instantcms: '/InstantCMS',
			ipb: '/IPB',
			joomla: '/Joomla',
			joostina: '/Joostina',
			livestreet: '/LiveStreet',
			magento: '/Magento',
			modx: '/ModX',
			netcat: '/NetCat',
			opencart: '/OpenCart',
			oscommerce: '/osCommerce',
			phpbb: '/phpBB',
			phpshop: '/PHPShop',
			prestashop: '/PrestaShop',
			shop_script: '/Shop-Script',
			simpla: '/Simpla',
			siteedit: '/SiteEdit',
			sugarcrm: '/SugarCRM',
			typo3: '/TYPO3',
			ubercart: '/Ubercart',
			ucoz: '/uCoz',
			umi: '/UMI',
			xslt: '/XSLT',
			vamshop: '/VamShop',
			vbulletin: '/vBulletin',
			virtuemart: '/VirtueMart',
			webasyst: '/WebAsyst',
			wp: '/WP',
			zen_cart: '/Zen Cart',
		fw: 'Framework',
			cakephp: '/CakePHP',
			codeigniter: '/CodeIgniter',
			kohana: '/Kohana',
			laravel: '/Laravel',
			symfony: '/Symfony',
			yii: '/Yii',
			zend: '/Zend',
        self: 'Самопис',
        pageProof: 'Вёрстка',
			adapted: '/Адаптивная',
			validation: '/Валидная',
			cross_browser: '/Кроссбраузерная',
			table: '/Табличная',
			div_design: '/Дивная',
        design: 'Дизайн',
			corporate_identity: '/Фирменный стиль',
			banners: '/Баннеры',
			print_design: '/Полиграфический дизайн',
			outdoor_advertising: '/Наружная реклама',
			site_design: '/Дизайн сайтов',
			interfaces: '/Интерфейсы',
			infographics: '/Инфографика',
			logos: '/Логотипы',
			icons: '/Иконки',
			psd: '/.PSD',
        changes: 'Правка/Доработка',
        fullProject: 'Сайт "под ключ"',
			cutaway: '/Визитка',
			corporate: '/Корпоративный',
			blog: '/Блог',
			onlineShop: '/Интернет-магазин',
			portal: '/Портал',
            landing: '/Лендинг',
        crm: 'CRM',
			erp: '/ERP',
        js: 'JavaScript/JS',
            angularjs: '/Angular.js',
            nodejs: '/Node.js',
            emberjs: '/Ember.js',
            backbonejs: '/Backbone.js',
        jquery: 'jQuery',
        scripts: 'Скрипты',
        modules: 'Модули',
        withzero: 'С нуля',
        ajax: 'AJAX',
        xhtml: 'xHTML',
        webapplication: 'Веб-приложения',
        audit: 'Аудит',
        socnet: 'Соц. сети',
			facebook: '/Facebook',
			vkontakte: '/Vkontakte',
			googleplus: '/Google+',
            twitter: '/Twitter',
            instagram: '/Instagram',
        testing: 'Тестирование',
        serverconfig: 'Настройка сервера',
        cmsdevelopment: 'Разработка CMS',
        drafting: 'Составление ТЗ',
        google: 'Google',
			android: '/Android',
        ruby: 'Ruby',
			rubyonrails: '/Ruby on Rails',
        smarty: 'Smarty',
        shopify: 'Shopify',
        apple: 'Apple',
			ipad: '/iPad',
			iphone: '/iPhone',
            objectivec: '/Objective-C',
            swift: '/Swift',
        database: 'Базы данных',
			mysql: '/MySql',
			postgressql: '/PostgresSQL',
			plsql: '/Pl/SQL',
        python: 'Python',
            django: '/Django',
            flask: '/Flask',
        aspnet: 'ASP.NET',
        bugs: 'Баги',
        viruses: 'Вирусы',
        parsers: 'Парсеры',
        html5: 'HTML5',
        php: 'PHP'
    },



    setTaskBaseData: function (tagsList) {
        var tags, element, i;
        if (arguments[1]) {
            tags = arguments[1];
        } else {
            tags = this;
        }
        for (element in tags) {
            if (typeof tags[element] == 'function' || element == 'names') continue;
            for (i in tags[element]) {
                if (typeof tags[element][i] == 'object') {
                    this.setTaskBaseData(tagsList, tags[element][i]);
                }
            }
            if (~tagsList.indexOf(element)) {
                tags[element]['isset'] = true;
            } else {
                tags[element]['isset'] = false;
            }
        }
    },

    displayTags: function () {

        var tags = this;

        for (element in tags.names) {

            $('.meta-tag.' + element).each(function () {

                if ($(this).text() == '') $(this).text(tags.names[element]);

            });

        }

    },

    getTagsList: function () {

        var tags, element, i, list, mode, form_id;

        if (arguments[0]) {
            form_id = arguments[0];
        } else {
            form_id = '';
        }

        if (arguments[1]) {
            tags = arguments[1];
        } else {
            tags = this;
        }

        if (arguments[2]) {
            list = arguments[2];
        } else {
            list = '';
        }

        if (arguments[3]) {
            mode = arguments[3];
        } else {
            mode = this.mode;
        }

        //alert(mode);

        for (element in tags) {

            if (mode == 'auto' && tags[element]['isset']) {
                list += (list) ? ',' + element : element;
            };

            if (mode == 'manual' && $(form_id + ' .meta-tag.' + element).hasClass('set')) {
                list += (list) ? ',' + element : element;
            };



            for (i in tags[element]) {

                if (typeof tags[element][i] == 'object') {



                    if (mode == 'auto' && tags[element]['isset']) {

                        list = this.getTagsList(form_id, tags[element][i], list, mode);

                    }

                    if (mode == 'manual' && $(form_id + ' .meta-tag.' + element).hasClass('set')) {

                        list = this.getTagsList(form_id, tags[element][i], list, mode);

                    }

                }

            }

        }

        return list;

    },

    displayMailButtons: function () {

        for (element in this.names) {

            $('.proposed .meta-tag.' + element).after('<div class="mail-btn"></div>');

        }

    },

    setMailTagsList: function (mailTagsList) {

        for (element in this.names) {

            if (~mailTagsList.indexOf(element)) {

                $('.proposed .meta-tag.' + element).addClass('set');
                $('.proposed .meta-tag.' + element).next('.mail-btn').addClass('set');

                if ($('.proposed .meta-tag.' + element).parent('.cms-types')[0] || element == 'cms') {
                    $('.cms-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.js-types')[0] || element == 'js') {
                    $('.js-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.pageProof-types')[0] || element == 'pageProof') {
                    $('.pageProof-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.fw-types')[0] || element == 'fw') {
                    $('.fw-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.crm-types')[0] || element == 'crm') {
                    $('.crm-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.fullProject-types')[0] || element == 'fullProject') {
                    $('.fullProject-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.design-types')[0] || element == 'design') {
                    $('.design-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.socnet-types')[0] || element == 'socnet') {
                    $('.socnet-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.google-types')[0] || element == 'google') {
                    $('.google-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.apple-types')[0] || element == 'apple') {
                    $('.apple-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.umi-types')[0] || element == 'umi') {
                    $('.umi-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.ruby-types')[0] || element == 'ruby') {
                    $('.ruby-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.database-types')[0] || element == 'database') {
                    $('.database-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.python-types')[0] || element == 'python') {
                    $('.python-types').addClass('set');
                }

            }

        }

    },

    getMailTagsList: function () {

        var tags, element, i, list;

        if (arguments[0]) {
            tags = arguments[0];
        } else {
            tags = this;
        }

        if (arguments[1]) {
            list = arguments[1];
        } else {
            list = '';
        }



        for (element in tags) {

            if ($('.proposed .meta-tag.' + element).next('.mail-btn').hasClass('set'))

            {
                list += (list) ? ',' + element : element;
            };



            for (i in tags[element]) {

                if (typeof tags[element][i] == 'object') {

                    if ($('.proposed .meta-tag.' + element).next('.mail-btn').hasClass('set'))

                    {
                        list = this.getMailTagsList(tags[element][i], list);
                    }

                }

            }

        }

        return list;

    },

    setShowTagsList: function (showTagsList) {

        for (element in this.names) {

            if (~showTagsList.indexOf(element)) {

                $('.proposed .meta-tag.' + element).addClass('set');
                if ($('.proposed .meta-tag.' + element).parent('.cms-types')[0] || element == 'cms') {
                    $('.cms-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.js-types')[0] || element == 'js') {
                    $('.js-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.pageProof-types')[0] || element == 'pageProof') {
                    $('.pageProof-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.fw-types')[0] || element == 'fw') {
                    $('.fw-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.crm-types')[0] || element == 'crm') {
                    $('.crm-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.fullProject-types')[0] || element == 'fullProject') {
                    $('.fullProject-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.design-types')[0] || element == 'design') {
                    $('.design-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.socnet-types')[0] || element == 'socnet') {
                    $('.socnet-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.google-types')[0] || element == 'google') {
                    $('.google-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.apple-types')[0] || element == 'apple') {
                    $('.apple-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.umi-types')[0] || element == 'umi') {
                    $('.umi-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.ruby-types')[0] || element == 'ruby') {
                    $('.ruby-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.database-types')[0] || element == 'database') {
                    $('.database-types').addClass('set');
                }
                if ($('.proposed .meta-tag.' + element).parent('.python-types')[0] || element == 'python') {
                    $('.python-types').addClass('set');
                }

            }
        }

    },

    getShowTagsList: function () {

        var tags, element, i, list;

        if (arguments[0]) {
            tags = arguments[0];
        } else {
            tags = this;
        }

        if (arguments[1]) {
            list = arguments[1];
        } else {
            list = '';
        }



        for (element in tags) {

            if ($('.proposed .meta-tag.' + element).hasClass('set'))

            {
                list += (list) ? ',' + element : element;
            };



            for (i in tags[element]) {

                if (typeof tags[element][i] == 'object') {

                    if ($('.proposed .meta-tag.' + element).hasClass('set'))

                    {
                        list = this.getShowTagsList(tags[element][i], list);
                    }

                }

            }

        }

        return list;

    },

    mode: 'manual'

}; // close tagBlock



var reloader = {
    timer: null,

    clock: null,

    update: function () {

        var date = new Date(),
            duration;



        if (!this.clock) {

            this.clock = date.getTime();

            return;

        }

        duration = (date.getTime() - this.clock) / 1000;

        $('.notification-area>.msg').text('Список подходящих проектов обновлён');

        if (duration > 240) {
            process
            TagsList('show_tasks');
        } else if (duration > 180) {
            $('.notification-area>.dur').text('3 минуты назад');
        } else if (duration > 120) {
            $('.notification-area>.dur').text('2 минуты назад');
        } else if (duration > 60) {
            $('.notification-area>.dur').text('минуту назад');
        } else if (duration > 30) {
            $('.notification-area>.dur').text('30 сек назад');
        }

    },

    start: function () {

        if (this.timer) {
            clearInterval(this.timer);
            this.clock = null;
        }

        this.timer = setInterval(function () {
            reloader.update.call(reloader)
        }, 30100);

    }

};



function getSelectedTags() {

    var list = tagBlock.getShowTagsList();

    if (list == '') {

        $('.notification-area>.msg').text('Выберите теги тех проектов, которые вам интересны');

        $('div.tasks-wrap').html('');

        return false;

    }

    return list;

}



function processCustomTagsList(action) {

    var fd = new FormData(),
        list;

    fd.append('user_id', $('input#user_id').val());

    var filters = '';

    $('#filters_form input[checked]').each(function(idx){
        if (idx != 0)
            filters += ',';
        filters += this.value;
    });

    fd.append('filters', filters);

    if (action == 'show_tasks') {

        list = getSelectedTags();
        if (!list) return;

        fd.append('show_tasks', list);

        $('.notification-area>.msg').text('Обновление списка подходящих проектов');

    }

    if (action == 'update_show_list') {

        list = getSelectedTags();
        if (!list) return;

        fd.append('update_show_tags_list', list);

        $('.notification-area>.msg').text('Обновление списка show_tags');

    }

    if (action == 'update_mail_list') {

        fd.append('update_mail_tags_list', tagBlock.getMailTagsList());

        $('.notification-area>.msg').text('Обновление списка mail_tags');

    }



    $('.notification-area>.msg').addClass('update');

    $('.notification-area>.dur').text('');

    if (action != 'update_mail_list') reloader.start();



    $.ajax({

        url: ('http://' + window.location.host + '/modules/custom_tags_handler.php'),

        data: fd,

        type: 'POST',

        contentType: false,

        processData: false,



        success: function (back) {

            $('.notification-area>.msg').removeClass('update');



            if (action == 'update_mail_list') {

                $('.notification-area>.msg').text('Список тегов для оповещения по email обновлён');

            } else {

                if (back != 'none') {

                    $('.tasks-not-found').removeClass('set');

                    $('div.tasks-wrap').html(back);

                    $('.notification-area>.msg').text('Список подходящих проектов обновлён');

                    $('.notification-area>.dur').text('только что');

                    tagBlock.displayTags();

                } else {

                    $('.notification-area>.msg').text('Такого рода проектов пока нет в нашем трекере.');

                    $('div.tasks-wrap').html('');

                    $('.tasks-not-found').addClass('set');

                }

            }

            //console.log(back);	

        },

        error: function (back) {

            console.log(back);

        }

    })

}



$(document).ready(function () {

    var $mode = $('.creation-mode-switch').children('.mode');

    $mode.attr('unselectable', 'on');



    if ($('div.description-tags')[0]) {

        tagBlock.displayTags();

    }



    $('.meta-tag').each(function () {

        $(this).attr('unselectable', 'on');

    });



    if ($('input#t_tags_list')[0]) {

        var tagsList = $('input#t_tags_list').val();



        if ($mode.hasClass('auto')) {

            $mode.text('auto');

            tagBlock.mode = 'auto';

            updateTags();

        } else {

            $mode.text('в ручную');

            tagBlock.mode = 'manual';

            tagBlock.setTaskBaseData(tagsList);

            drawTags();

        }

    }



    if ($('div.proposed')[0]) {

        tagBlock.displayMailButtons();

        tagBlock.setShowTagsList($('input#u_show_tags_list').val());

        tagBlock.setMailTagsList($('input#u_mail_tags_list').val());

        processCustomTagsList('show_tasks');

    }



    $('.tags-block .meta-tag').not('.head').click(function () {

        $(this).toggleClass('set');
        if ($(this).next('.mail-btn').hasClass('set')) {
            $(this).next('.mail-btn').toggleClass('set');
        }

        if ($(this).parent('.cms-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.cms').addClass('set');
            }
            updateCmsTypes();
        }
        if ($(this).hasClass('cms')) {
            $('.cms-types').children('.meta-tag').not('.head').each(function () {
                $(this).removeClass('set');
            });
            $('.cms-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateCmsTypes();
        }

        if ($(this).parent('.js-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.js').addClass('set');
            }
            updateJSTypes();
        }
        if ($(this).hasClass('js')) {
            $('.js-types').children('.meta-tag').not('.head').each(function () {
                $(this).removeClass('set');
            });
            $('.js-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateJSTypes();
        }

        if ($(this).parent('.crm-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.crm').addClass('set');
            }
            updateCrmTypes();
        }
        if ($(this).hasClass('crm')) {
            $('.crm-types').children('.meta-tag').not('.head').each(function () {
                $(this).removeClass('set');
            });
            $('.crm-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateCrmTypes();
        }

        if ($(this).parent('.fullProject-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.fullProject').addClass('set');
            }
            updateFullProjectTypes();
        }
        if ($(this).hasClass('fullProject')) {
            $('.fullProject-types').children('.meta-tag').not('.head').each(function () {
                $(this).removeClass('set');
            });
            $('.fullProject-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateFullProjectTypes();
        }

        if ($(this).parent('.socnet-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.socnet').addClass('set');
            }
            updateSocNetTypes();
        }
        if ($(this).hasClass('socnet')) {
            $('.socnet-types').children('.meta-tag').not('.head').each(function () {
                $(this).removeClass('set');
            });
            $('.socnet-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateSocNetTypes();
        }

        if ($(this).parent('.pageProof-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.pageProof').addClass('set');
            }
            updatePageProofTypes();
        }
        if ($(this).hasClass('pageProof')) {
            $('.pageProof-types').children('.meta-tag').not('.head').each(function () {
                $(this).removeClass('set');
            });
            $('.pageProof-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updatePageProofTypes();
        }

        if ($(this).parent('.design-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.design').addClass('set');
            }
            updateDesignTypes();
        }
        if ($(this).hasClass('design')) {
            $('.design-types').children('.meta-tag').not('.head').each(function () {
                $(this).removeClass('set');
            });
            $('.design-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateDesignTypes();
        }

        if ($(this).parent('.fw-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.fw').addClass('set');
            }
            updateFwTypes();
        }
        if ($(this).hasClass('fw')) {
            $('.fw-types').children('.meta-tag').not('.head').each(function () {
                $(this).removeClass('set');
            });
            $('.fw-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateFwTypes();
        }

        if ($(this).parent('.google-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.google').addClass('set');
            }
            updateGoogleTypes();
        }
        if ($(this).hasClass('google')) {
            $('.google-types').children('.meta-tag').not('.head').each(function () {
                $(this).removeClass('set');
            });
            $('.google-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateGoogleTypes();
        }

        if ($(this).parent('.apple-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.apple').addClass('set');
            }
            updateAppleTypes();
        }
        if ($(this).hasClass('apple')) {
            $('.apple-types').children('.meta-tag').not('.head').each(function () {
                $(this).removeClass('set');
            });
            $('.apple-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateAppleTypes();
        }

        if ($(this).parent('.umi-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.umi').addClass('set');
            }
            updateUmiTypes();
        }
        if ($(this).hasClass('umi')) {
            $('.umi-types').children('.meta-tag').not('.head').each(function () {
                $(this).removeClass('set');
            });
            $('.umi-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateUmiTypes();
        }

        if ($(this).parent('.ruby-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.ruby').addClass('set');
            }
            updateRubyTypes();
        }
        if ($(this).hasClass('ruby')) {
            $('.ruby-types').children('.meta-tag').not('.head').each(function () {
                $(this).removeClass('set');
            });
            $('.ruby-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateRubyTypes();
        }

        if ($(this).parent('.database-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.database').addClass('set');
            }
            updateDataBaseTypes();
        }
        if ($(this).hasClass('database')) {
            $('.database-types').children('.meta-tag').not('.head').each(function () {
                $(this).removeClass('set');
            });
            $('.database-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateDataBaseTypes();
        }

        if ($(this).parent('.python-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.python').addClass('set');
            }
            updatePythonTypes();
        }
        if ($(this).hasClass('python')) {
            $('.python-types').children('.meta-tag').not('.head').each(function () {
                $(this).removeClass('set');
            });
            $('.python-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updatePythonTypes();
        }


        if ($(this).parents('.proposed')) {
            processCustomTagsList('update_show_list');
        }
        if (tagBlock.mode == 'auto') {
            $mode.trigger('click');
        }
    });

    $('.tags-block .mail-btn').click(function () {


        if ($(this).prev('.meta-tag').hasClass('set')) {
            $(this).toggleClass('set');
        } else {
            $(this).prev('.meta-tag').toggleClass('set');
            $(this).toggleClass('set');
        }

        if ($(this).parent('.cms-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.cms').next('.mail-btn').addClass('set');
            }
            updateCmsTypes();
        }
        if ($(this).prev('.meta-tag.cms')[0]) {
            $('.cms-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateCmsTypes();
        }

        if ($(this).parent('.js-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.js').next('.mail-btn').addClass('set');
            }
            updateJSTypes();
        }
        if ($(this).prev('.meta-tag.js')[0]) {
            $('.js-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateJSTypes();
        }

        if ($(this).parent('.crm-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.crm').next('.mail-btn').addClass('set');
            }
            updateCrmTypes();
        }
        if ($(this).prev('.meta-tag.crm')[0]) {
            $('.crm-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateCrmTypes();
        }

        if ($(this).parent('.fullProject-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.fullProject').next('.mail-btn').addClass('set');
            }
            updateFullProjectTypes();
        }
        if ($(this).prev('.meta-tag.fullProject')[0]) {
            $('.fullProject-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateFullProjectTypes();
        }

        if ($(this).parent('.socnet-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.socnet').next('.mail-btn').addClass('set');
            }
            updateSocNetTypes();
        }
        if ($(this).prev('.meta-tag.socnet')[0]) {
            $('.socnet-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateSocNetTypes();
        }

        if ($(this).parent('.pageProof-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.pageProof').next('.mail-btn').addClass('set');
            }
            updatePageProofTypes();
        }
        if ($(this).prev('.meta-tag.pageProof')[0]) {
            $('.pageProof-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updatePageProofTypes();
        }

        if ($(this).parent('.design-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.design').next('.mail-btn').addClass('set');
            }
            updateDesignTypes();
        }
        if ($(this).prev('.meta-tag.design')[0]) {
            $('.design-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateDesignTypes();
        }

        if ($(this).parent('.fw-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.fw').next('.mail-btn').addClass('set');
            }
            updateFwTypes();
        }
        if ($(this).prev('.meta-tag.fw')[0]) {
            $('.fw-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateFwTypes();
        }

        if ($(this).parent('.google-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.google').next('.mail-btn').addClass('set');
            }
            updateGoogleTypes();
        }
        if ($(this).prev('.meta-tag.google')[0]) {
            $('.google-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateGoogleTypes();
        }

        if ($(this).parent('.apple-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.apple').next('.mail-btn').addClass('set');
            }
            updateAppleTypes();
        }
        if ($(this).prev('.meta-tag.apple')[0]) {
            $('.apple-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateAppleTypes();
        }

        if ($(this).parent('.umi-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.umi').next('.mail-btn').addClass('set');
            }
            updateUmiTypes();
        }
        if ($(this).prev('.meta-tag.umi')[0]) {
            $('.umi-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateUmiTypes();
        }

        if ($(this).parent('.ruby-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.ruby').next('.mail-btn').addClass('set');
            }
            updateRubyTypes();
        }
        if ($(this).prev('.meta-tag.ruby')[0]) {
            $('.ruby-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateRubyTypes();
        }

        if ($(this).parent('.database-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.database').next('.mail-btn').addClass('set');
            }
            updateDataBaseTypes();
        }
        if ($(this).prev('.meta-tag.database')[0]) {
            $('.database-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updateDataBaseTypes();
        }

        if ($(this).parent('.python-types')[0]) {
            if ($(this).hasClass('set')) {
                $('.meta-tag.python').next('.mail-btn').addClass('set');
            }
            updatePythonTypes();
        }
        if ($(this).prev('.meta-tag.python')[0]) {
            $('.python-types').children('.mail-btn').each(function () {
                $(this).removeClass('set');
            });
            updatePythonTypes();
        }

        processCustomTagsList('update_mail_list');

    });

    function updateCmsTypes() {
        var cmsTypes = $('.cms-types'),
            isset = false;
        $('.cms-types').children('.mail-btn').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        $('.cms-types').children('.meta-tag').not('.head').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        if ($('.tags-block .meta-tag.cms').hasClass('set')) {
            isset = true;
        }
        if ($('.tags-block .meta-tag.cms').next('.mail-btn').hasClass('set')) {
            isset = true;
        }
        if (isset) cmsTypes.addClass('set');
        else {
            cmsTypes.removeClass('set');
            cmsTypes.next('.mail-btn').removeClass('set');
        }
    }

    function updateJSTypes() {
        var jsTypes = $('.js-types'),
            isset = false;
        $('.js-types').children('.mail-btn').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        $('.js-types').children('.meta-tag').not('.head').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        if ($('.tags-block .meta-tag.js').hasClass('set')) {
            isset = true;
        }
        if ($('.tags-block .meta-tag.js').next('.mail-btn').hasClass('set')) {
            isset = true;
        }
        if (isset) jsTypes.addClass('set');
        else {
            jsTypes.removeClass('set');
            jsTypes.next('.mail-btn').removeClass('set');
        }
    }

    function updateCrmTypes() {
        var crmTypes = $('.crm-types'),
            isset = false;
        $('.crm-types').children('.mail-btn').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        $('.crm-types').children('.meta-tag').not('.head').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        if ($('.tags-block .meta-tag.crm').hasClass('set')) {
            isset = true;
        }
        if ($('.tags-block .meta-tag.crm').next('.mail-btn').hasClass('set')) {
            isset = true;
        }
        if (isset) crmTypes.addClass('set');
        else {
            crmTypes.removeClass('set');
            crmTypes.next('.mail-btn').removeClass('set');
        }
    }

    function updateFullProjectTypes() {
        var fullProjectTypes = $('.fullProject-types'),
            isset = false;
        $('.fullProject-types').children('.mail-btn').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        $('.fullProject-types').children('.meta-tag').not('.head').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        if ($('.tags-block .meta-tag.fullProject').hasClass('set')) {
            isset = true;
        }
        if ($('.tags-block .meta-tag.fullProject').next('.mail-btn').hasClass('set')) {
            isset = true;
        }
        if (isset) fullProjectTypes.addClass('set');
        else {
            fullProjectTypes.removeClass('set');
            fullProjectTypes.next('.mail-btn').removeClass('set');
        }
    }

    function updateSocNetTypes() {
        var socNetTypes = $('.socnet-types'),
            isset = false;
        $('.socnet-types').children('.mail-btn').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        $('.socnet-types').children('.meta-tag').not('.head').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        if ($('.tags-block .meta-tag.socnet').hasClass('set')) {
            isset = true;
        }
        if ($('.tags-block .meta-tag.socnet').next('.mail-btn').hasClass('set')) {
            isset = true;
        }
        if (isset) socNetTypes.addClass('set');
        else {
            socNetTypes.removeClass('set');
            socNetTypes.next('.mail-btn').removeClass('set');
        }
    }

    function updatePageProofTypes() {
        var pageProofTypes = $('.pageProof-types'),
            isset = false;
        $('.pageProof-types').children('.mail-btn').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        $('.pageProof-types').children('.meta-tag').not('.head').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        if ($('.tags-block .meta-tag.pageProof').hasClass('set')) {
            isset = true;
        }
        if ($('.tags-block .meta-tag.pageProof').next('.mail-btn').hasClass('set')) {
            isset = true;
        }
        if (isset) pageProofTypes.addClass('set');
        else {
            pageProofTypes.removeClass('set');
            pageProofTypes.next('.mail-btn').removeClass('set');
        }
    }

    function updateDesignTypes() {
        var designTypes = $('.design-types'),
            isset = false;
        $('.design-types').children('.mail-btn').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        $('.design-types').children('.meta-tag').not('.head').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        if ($('.tags-block .meta-tag.design').hasClass('set')) {
            isset = true;
        }
        if ($('.tags-block .meta-tag.design').next('.mail-btn').hasClass('set')) {
            isset = true;
        }
        if (isset) designTypes.addClass('set');
        else {
            designTypes.removeClass('set');
            designTypes.next('.mail-btn').removeClass('set');
        }
    }

    function updateFwTypes() {
        var fwTypes = $('.fw-types'),
            isset = false;
        $('.fw-types').children('.mail-btn').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        $('.fw-types').children('.meta-tag').not('.head').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        if ($('.tags-block .meta-tag.fw').hasClass('set')) {
            isset = true;
        }
        if ($('.tags-block .meta-tag.fw').next('.mail-btn').hasClass('set')) {
            isset = true;
        }
        if (isset) fwTypes.addClass('set');
        else {
            fwTypes.removeClass('set');
            fwTypes.next('.mail-btn').removeClass('set');
        }
    }

    function updateGoogleTypes() {
        var googleTypes = $('.google-types'),
            isset = false;
        $('.google-types').children('.mail-btn').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        $('.google-types').children('.meta-tag').not('.head').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        if ($('.tags-block .meta-tag.google').hasClass('set')) {
            isset = true;
        }
        if ($('.tags-block .meta-tag.google').next('.mail-btn').hasClass('set')) {
            isset = true;
        }
        if (isset) googleTypes.addClass('set');
        else {
            googleTypes.removeClass('set');
            googleTypes.next('.mail-btn').removeClass('set');
        }
    }

    function updateAppleTypes() {
        var appleTypes = $('.apple-types'),
            isset = false;
        $('.apple-types').children('.mail-btn').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        $('.apple-types').children('.meta-tag').not('.head').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        if ($('.tags-block .meta-tag.apple').hasClass('set')) {
            isset = true;
        }
        if ($('.tags-block .meta-tag.apple').next('.mail-btn').hasClass('set')) {
            isset = true;
        }
        if (isset) appleTypes.addClass('set');
        else {
            appleTypes.removeClass('set');
            appleTypes.next('.mail-btn').removeClass('set');
        }
    }

    function updateUmiTypes() {
        var umiTypes = $('.umi-types'),
            isset = false;
        $('.umi-types').children('.mail-btn').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        $('.umi-types').children('.meta-tag').not('.head').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        if ($('.tags-block .meta-tag.umi').hasClass('set')) {
            isset = true;
        }
        if ($('.tags-block .meta-tag.umi').next('.mail-btn').hasClass('set')) {
            isset = true;
        }
        if (isset) umiTypes.addClass('set');
        else {
            umiTypes.removeClass('set');
            umiTypes.next('.mail-btn').removeClass('set');
        }
    }

    function updateRubyTypes() {
        var rubyTypes = $('.ruby-types'),
            isset = false;
        $('.ruby-types').children('.mail-btn').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        $('.ruby-types').children('.meta-tag').not('.head').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        if ($('.tags-block .meta-tag.ruby').hasClass('set')) {
            isset = true;
        }
        if ($('.tags-block .meta-tag.ruby').next('.mail-btn').hasClass('set')) {
            isset = true;
        }
        if (isset) rubyTypes.addClass('set');
        else {
            rubyTypes.removeClass('set');
            rubyTypes.next('.mail-btn').removeClass('set');
        }
    }

    function updateDataBaseTypes() {
        var dataBaseTypes = $('.database-types'),
            isset = false;
        $('.database-types').children('.mail-btn').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        $('.database-types').children('.meta-tag').not('.head').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        if ($('.tags-block .meta-tag.database').hasClass('set')) {
            isset = true;
        }
        if ($('.tags-block .meta-tag.database').next('.mail-btn').hasClass('set')) {
            isset = true;
        }
        if (isset) dataBaseTypes.addClass('set');
        else {
            dataBaseTypes.removeClass('set');
            dataBaseTypes.next('.mail-btn').removeClass('set');
        }
    }

    function updatePythonTypes() {
        var pythonTypes = $('.python-types'),
            isset = false;
        $('.python-types').children('.mail-btn').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        $('.python-types').children('.meta-tag').not('.head').each(function () {
            if ($(this).hasClass('set')) {
                isset = true;
            }
        });
        if ($('.tags-block .meta-tag.python').hasClass('set')) {
            isset = true;
        }
        if ($('.tags-block .meta-tag.python').next('.mail-btn').hasClass('set')) {
            isset = true;
        }
        if (isset) pythonTypes.addClass('set');
        else {
            pythonTypes.removeClass('set');
            pythonTypes.next('.mail-btn').removeClass('set');
        }
    }

    $mode.click(function () {
        if ($(this).hasClass('auto')) {
            $(this).removeClass('auto').addClass('manual').text('в ручную');
            tagBlock.mode = 'manual';
        } else {
            $(this).removeClass('manual').addClass('auto').text('auto');
            tagBlock.mode = 'auto';
            updateTags();
        }
    });



    function getMatches(text) {

        //alert('1');

        var tags, element, i, searchStr, searchPat, childExists, thisExists;

        if (arguments[1]) {
            tags = arguments[1];
        } else {
            tags = tagBlock;
        }

        thisExists = false;

        childExists = false;



        for (element in tags) {

            if (typeof tags[element] == 'function' || element == 'names') continue;

            searchStr = '';

            for (i in tags[element]) {

                if (typeof tags[element][i] == 'string') {

                    if (searchStr == '') {
                        searchStr = '(' + tags[element][i];
                    } else {
                        searchStr += '|' + tags[element][i];
                    }

                }

                if (typeof tags[element][i] == 'object') {

                    childExists = getMatches(text, tags[element][i]);

                }

            }

            if (searchStr != '') searchStr += ')';

            //alert(searchStr);

            searchPat = new RegExp(searchStr, 'i');

            if (~text.search(searchPat) || childExists) {

                tags[element]['isset'] = true;

                thisExists = true;

                //if (tag == 'wp' || tag == 'joomla' || tag == 'drupal'){ tagBlock.cms['isset'] = true; }

                //if (tag == 'zend' || tag == 'yii' || tag == 'kohana'){ tagBlock.fw['isset'] = true; }

            } else {

                tags[element]['isset'] = false;

            }

            childExists = false;

        }

        return thisExists;

    }



    function drawTags() {

        var tags, element, i;

        if (arguments[0]) {
            tags = arguments[0];
        } else {
            tags = tagBlock;
        }



        for (element in tags) {
            if (tags[element]['isset']) {
                $('.meta-tag.' + element).addClass('set');
                if (element == 'fw') {
                    $('.fw-types').addClass('set');
                }
                if (element == 'cms') {
                    $('.cms-types').addClass('set');
                }
                if (element == 'js') {
                    $('.js-types').addClass('set');
                }
                if (element == 'pageProof') {
                    $('.pageProof-types').addClass('set');
                }
                if (element == 'crm') {
                    $('.crm-types').addClass('set');
                }
                if (element == 'fullProject') {
                    $('.fullProject-types').addClass('set');
                }
                if (element == 'design') {
                    $('.design-types').addClass('set');
                }
                if (element == 'socnet') {
                    $('.socnet-types').addClass('set');
                }
                if (element == 'google') {
                    $('.google-types').addClass('set');
                }
                if (element == 'apple') {
                    $('.apple-types').addClass('set');
                }
                if (element == 'umi') {
                    $('.umi-types').addClass('set');
                }
                if (element == 'ruby') {
                    $('.ruby-types').addClass('set');
                }
                if (element == 'database') {
                    $('.database-types').addClass('set');
                }
                if (element == 'python') {
                    $('.python-types').addClass('set');
                }

            } else {
                $('.meta-tag.' + element).removeClass('set');
                if (element == 'fw') {
                    $('.fw-types').removeClass('set');
                }
                if (element == 'cms') {
                    $('.cms-types').removeClass('set');
                }
                if (element == 'js') {
                    $('.js-types').removeClass('set');
                }
                if (element == 'pageProof') {
                    $('.pageProof-types').removeClass('set');
                }
                if (element == 'crm') {
                    $('.crm-types').removeClass('set');
                }
                if (element == 'fullProject') {
                    $('.fullProject-types').removeClass('set');
                }
                if (element == 'design') {
                    $('.design-types').removeClass('set');
                }
                if (element == 'socnet') {
                    $('.socnet-types').removeClass('set');
                }
                if (element == 'google') {
                    $('.google-types').removeClass('set');
                }
                if (element == 'apple') {
                    $('.apple-types').removeClass('set');
                }
                if (element == 'umi') {
                    $('.umi-types').removeClass('set');
                }
                if (element == 'ruby') {
                    $('.ruby-types').removeClass('set');
                }
                if (element == 'database') {
                    $('.database-types').removeClass('set');
                }
                if (element == 'python') {
                    $('.python-types').removeClass('set');
                }

            }



            for (i in tags[element]) {

                if (typeof tags[element][i] == 'object') {

                    drawTags(tags[element][i]);

                }

            }

        }

    }



    function updateTags() {

        var text = $('#t_description').val();



        text = text.replace(/\s+/gm, ' ');

        text = text.replace(/('|"|`)/gm, '');

        //alert(text);

        getMatches(text);

        drawTags();

    }



    $('#t_description').keyup(function (e) {

        if (tagBlock.mode == 'auto' && e.which >= 8 && e.which <= 222) {

            updateTags();

        }

    });



    $('#create-project').click(function () {

        $('input#t_tags_list').val(tagBlock.getTagsList('#task_form'));

        if (tagBlock.mode == 'manual') {
            $('input#t_tags_creation_mode').val('manual');
        } else {
            $('input#t_tags_creation_mode').val('auto');
        }

    });




    function setListTags() {

        var $listTags = $('input#t_tags_list'),

            listStr = '';



        $('.meta-tag').not('.head').each(function () {

            if ($(this).hasClass('set')) {

                if (listStr == '') {
                    listStr = $(this).attr('id');
                } else {
                    listStr += ',' + $(this).attr('id');
                }

            }

        });

        alert(listStr);

    }



});