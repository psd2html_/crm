$(window).on('load', function(){
    $(document).on('change', '#download-photo', function() {
        $(this).parent().next('.result-download-photo').text($(this).val());
    });
    $('.ch').iCheck({
        checkboxClass: 'icheckbox_flat-green'
    });



    //get selected tags
    //get selected tags
    var checkTags = (function () {
        var preloadedSkills = $('.preloaded-skills').val(),
            hasOptions = $('.select2 option'),
            selectedArray = [];
        if(preloadedSkills) {
            preloadedSkills = JSON.parse(preloadedSkills);
            for(var key in preloadedSkills) {
                var temp = 0;
                for(var j=0; j<hasOptions.length; j++) {
                    if(preloadedSkills[key] == hasOptions[j]['outerText']) {
                        temp++;
                    }
                }
                if(!temp) {
                    $('.select2').append('<option value="'+preloadedSkills[key]+'">'+preloadedSkills[key]+'</option>option>');
                }
                selectedArray.push(preloadedSkills[key]);
            }
            return selectedArray;
        } else {
            return '';
        }
    })();
    $('.select2').val(checkTags).select2({
        tags: true,
        selectOnBlur: true
    });
    //$('.select2').select2({
    //    tags: true,
    //    selectOnBlur: true,
    //});



    $('input').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
        increaseArea: '20%' // optional
    });

    $('#confirm').on('ifChecked', function(event){
        $('#register').removeAttr('disabled');
    });
    $('#confirm').on('ifUnchecked', function (event) {
        $('#register').attr('disabled', "disabled");
    });

    $("#reg").click(function(){
        $('#confirm').iCheck('uncheck');
    });

    $(document).on('click', '.delete-button', function() {
        $(this).closest('.row-info').remove();
    });
    $(document).on('click', '.add-row-info', function() {
        var newRow = $(this).closest('.row-info').clone();
        $(this).closest('.my-elements').append(newRow);
    });

    //editor
    //$('#textarea').wysihtml5();

    $('#create-project').on('click', function(event) {
        event.preventDefault();
        setTimeout(function() {
            alert($('#t_tags_list').val());
        },500);

    });
});