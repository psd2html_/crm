$(window).on('load', function() {

    function hexc(colorval) {
        var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        delete(parts[0]);
        for (var i = 1; i <= 3; ++i) {
            parts[i] = parseInt(parts[i]).toString(16);
            if (parts[i].length == 1) parts[i] = '0' + parts[i];
        }
        return '#' + parts.join('');
    }

    function setValueInput(_this) {
        var tempValue = $('#t_tags_list').val() || '[]',
            thisColor = hexc(_this.css('background-color')),
            oneElement = {
                "id": _this.attr('id'),
                "name": _this.text(),
                "color": thisColor
            };
        tempValue = JSON.parse(tempValue);
        tempValue.push(oneElement);
        var inInput = JSON.stringify(tempValue);
        $('#t_tags_list').val(inInput);
    }

    function deleteElFromInput(_this) {
        var tempValue = $('#t_tags_list').val(),
            thisColor = hexc(_this.css('background-color')),
            oneElement = {
                "id": _this.attr('id'),
                "name": _this.text(),
                "color": thisColor
            };
        tempValue = JSON.parse(tempValue);
        for(var i=0; i<tempValue.length; i++) {
            if(tempValue[i]["name"] === oneElement["name"]) {
                tempValue.splice(i, 1);
            }
        }
        var inInput = JSON.stringify(tempValue);
        $('#t_tags_list').val(inInput);
    }

    function clearChildren(parent) {
        $('.sub-items').each(function(i, el) {
            if($(el).hasClass(parent)) {
                $(el).find('.meta-tag').each(function(j, elem) {
                    if($(elem).hasClass('set')) {
                        deleteElFromInput($(elem));
                        $(elem).removeClass('set');
                    }
                });
            }
        });
    }

    $('.general-tags .meta-tag').on('click', function() {
        var thisAttr = $(this).attr('class').replace("meta-tag", "").replace("set", "").trim() + '-types';
        $(this).toggleClass('set');
        if($(this).hasClass('set')) {
            setValueInput($(this));
        } else {
            deleteElFromInput($(this));
            clearChildren(thisAttr);
        }
        $('.sub-items').each(function(i, el) {
            if($(el).hasClass(thisAttr)) {
                $(el).toggleClass('set');
            }
        });
    });

    $('.sub-items .meta-tag').on('click', function() {
        $(this).toggleClass('set');
        if($(this).hasClass('set')) {
            setValueInput($(this));
        } else {
            deleteElFromInput($(this));
        }
    });
});