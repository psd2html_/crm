<?php

/**
 * This is the model class for table "{{projects}}".
 *
 * The followings are the available columns in table '{{projects}}':
 * @property string $id
 * @property string $user_id
 * @property string $title
 * @property string $description
 * @property string $careate_at
 * @property string $update_at
 * @property integer $status
 * @property string $budget
 */
class Projects extends CActiveRecord
{
    const STATUS_OPEN           = 1;
    const STATUS_CLOSED         = 2; // закрыт или завершен
    const STATUS_ASSIGN         = 3; // назначенный на какого то юзера
    const STATUS_DELETED        = 4; // Удален
    const STATUS_ARCHIVED       = 5; // в архиве, в корзине
    const STATUS_UNPUBLISHED    = 6; // снят с публикации


    public $files;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{projects}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, description, category_id', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('title, budget', 'length', 'max' => 255),
            //array('file', 'file', 'types' => 'jpg, gif, png, doc, docx, xls,'),
            array('files', 'file', 'allowEmpty' => true),


			//array('update_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, tags, category_id, description, status, budget', 'safe', 'on'=>'search'),
			array('files', 'unsafe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'LinkUser' => array(self::BELONGS_TO, 'User', 'user_id'), // вытащим юзера с профилем
            'LinkProposal' => array(self::HAS_MANY, 'ProjectsProposal', 'project_id'),

            'pAssignUser'       => array(self::HAS_ONE, 'ProjectsAssignUser', 'project_id'),

            // кол-во заявок
            'proposalCount' => array(self::STAT, 'ProjectsProposal', 'project_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'Пользователь',
			'title' => 'Заголовок',
			'description' => 'Описание проекта',
			'status' => 'Статус',
			'budget' => 'бюджет',
			'tags' => 'Теги',
			'category_id' => 'Специализация проекта',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('create_at',$this->create_at,true);
		$criteria->compare('update_at',$this->update_at,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('budget',$this->budget,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('category_id',$this->category_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


    public function scopes()
    {
        return array(
            'published' => array(
                'condition' => '`t`.`status` = '. self::STATUS_OPEN,
            ),
            'recently' => array(
                'order' => '`t`.`create_at` DESC',
                'limit' => 5,
            ),
        );
    }


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Projects the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


    protected function beforeSave()
    {
        if (parent::beforeSave())
        {
            if ($this->isNewRecord)
            {
                $this->create_at = date('Y-m-d H:i:s', time());
                $this->user_id = Yii::app()->user->id;
                $this->status = self::STATUS_OPEN;
            }
            else
                $this->update_at = date('Y-m-d H:i:s', time());

            return true;
        }
        else
            return false;
    }
}
