<?php

/**
 * This is the model class for table "{{projects_proposal_comments}}".
 *
 * The followings are the available columns in table '{{projects_proposal_comments}}':
 * @property integer $id
 * @property integer $project_proposal_id
 * @property integer $user_id
 * @property string $comments
 * @property string $create_at
 */
class ProjectsProposalComments extends CActiveRecord
{

    const STATUS_UNREAD    = 0;
    const STATUS_READ      = 1;


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{projects_proposal_comments}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_proposal_id, user_id, comments, create_at, project_id', 'required'),
			array('project_proposal_id, user_id, status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().

            array('project_id', 'unsafe'),

			// @todo Please remove those attributes that should not be searched.
			array('project_proposal_id, user_id, comments, create_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'ProjectProposal' => array(self::BELONGS_TO, 'ProjectsProposal', 'project_proposal_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'project_proposal_id' => 'Project Proposal',
			'user_id' => 'User',
			'comments' => 'Comments',
			'create_at' => 'Create At',
			'status' => 'Status',
			'project_id' => 'ID Project',
		);
	}


    public function scopes()
    {
        return array(
            'unread' => array(
                'condition' => 'status = 0',
            ),
            'recently'=>array(
                'order'=>'create_time DESC',
                'limit'=>5,
            ),
        );
    }


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('project_proposal_id',$this->project_proposal_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('comments',$this->comments,true);
		$criteria->compare('create_at',$this->create_at,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('project_id',$this->project_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProjectsProposalComments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


    protected function beforeSave()
    {
        if (parent::beforeSave())
        {
            if ($this->isNewRecord)
            {
                $this->create_at = date('Y-m-d H:i:s', time());
                $this->user_id = Yii::app()->user->id;
            }
            return true;
        }
        else
            return false;
    }
}
