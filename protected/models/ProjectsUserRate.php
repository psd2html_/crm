<?php

/**
 * This is the model class for table "{{projects_user_rate}}".
 *
 * The followings are the available columns in table '{{projects_user_rate}}':
 * @property integer $id
 * @property integer $pau_id
 * @property double $skills
 * @property double $availability
 * @property double $quality
 * @property double $deadlines
 * @property double $cooperation
 * @property double $communication
 */
class ProjectsUserRate extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{projects_user_rate}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pau_id, skills, availability, quality, deadlines, cooperation, communication', 'required'),
			array('pau_id', 'numerical', 'integerOnly'=>true),
			array('skills, availability, quality, deadlines, cooperation, communication', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, pau_id, skills, availability, quality, deadlines, cooperation, communication', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'pau_id' => 'Pau',
			'skills' => 'Навыки',
			'availability' => 'Доступность',
			'quality' => 'Качество',
			'deadlines' => 'Сроки выполнения',
			'cooperation' => 'Сотрудничество',
			'communication' => 'Коммуникация',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('pau_id',$this->pau_id);
		$criteria->compare('skills',$this->skills);
		$criteria->compare('availability',$this->availability);
		$criteria->compare('quality',$this->quality);
		$criteria->compare('deadlines',$this->deadlines);
		$criteria->compare('cooperation',$this->cooperation);
		$criteria->compare('communication',$this->communication);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProjectsUserRate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
