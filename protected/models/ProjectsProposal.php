<?php

/**
 * This is the model class for table "{{projects_proposal}}".
 *
 * The followings are the available columns in table '{{projects_proposal}}':
 * @property string $id
 * @property string $user_id
 * @property string $project_id
 * @property string $create_at
 * @property string $budget
 * @property string $period
 * @property string $description
 */
class ProjectsProposal extends CActiveRecord
{

    const STATUS_UNREAD    = 0; // непрочитано
    const STATUS_READ      = 1; // прочитано

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{projects_proposal}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('description', 'required'),
			//array('user_id, project_id', 'length', 'max'=>11),
			array('budget, period', 'length', 'max'=>255),
            array('status', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('budget, status, period, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(

            'lUser' => array(self::BELONGS_TO, 'User', 'user_id'),

            'lProjects' => array(self::BELONGS_TO, 'Projects', 'project_id'),

            // все коментарии
            'ppComments' => array(self::HAS_MANY, 'ProjectsProposalComments', 'project_proposal_id'),

            // кол-во комментариев по предложениям
            'ppCountComments' => array(self::STAT, 'ProjectsProposalComments', 'project_proposal_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'budget' => 'Бюджет',
			'period' => 'Срок',
			'description' => 'Описание',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('project_id',$this->project_id,true);
		$criteria->compare('create_at',$this->create_at,true);
		$criteria->compare('budget',$this->budget,true);
		$criteria->compare('period',$this->period,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProjectsProposal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


    protected function beforeSave()
    {
        if (parent::beforeSave())
        {
            if ($this->isNewRecord)
            {
                $this->create_at = date('Y-m-d H:i:s', time());
                $this->user_id = Yii::app()->user->id;
                $this->status = self::STATUS_UNREAD;
            }
            return true;
        }
        else
            return false;
    }
}
