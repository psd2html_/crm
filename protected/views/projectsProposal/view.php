<?php
/* @var $this ProjectsProposalController */
/* @var $model ProjectsProposal */

$this->breadcrumbs=array(
	'Projects Proposals'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ProjectsProposal', 'url'=>array('index')),
	array('label'=>'Create ProjectsProposal', 'url'=>array('create')),
	array('label'=>'Update ProjectsProposal', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ProjectsProposal', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProjectsProposal', 'url'=>array('admin')),
);
?>

<h1>View ProjectsProposal #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'project_id',
		'create_at',
		'budget',
		'period',
		'description',
	),
)); ?>
