<?php
/* @var $this ProjectsProposalController */
/* @var $model ProjectsProposal */

$this->breadcrumbs=array(
	'Projects Proposals'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProjectsProposal', 'url'=>array('index')),
	array('label'=>'Create ProjectsProposal', 'url'=>array('create')),
	array('label'=>'View ProjectsProposal', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProjectsProposal', 'url'=>array('admin')),
);
?>

<h1>Update ProjectsProposal <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>