<?php
/* @var $this ProjectsProposalController */
/* @var $model ProjectsProposal */

$this->breadcrumbs=array(
	'Projects Proposals'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProjectsProposal', 'url'=>array('index')),
	array('label'=>'Manage ProjectsProposal', 'url'=>array('admin')),
);
?>

<h1>Create ProjectsProposal</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>