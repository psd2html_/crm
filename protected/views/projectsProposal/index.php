<?php
/* @var $this ProjectsProposalController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Projects Proposals',
);

$this->menu=array(
	array('label'=>'Create ProjectsProposal', 'url'=>array('create')),
	array('label'=>'Manage ProjectsProposal', 'url'=>array('admin')),
);
?>

<h1>Projects Proposals</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
