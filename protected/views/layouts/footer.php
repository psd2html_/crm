<?php $cs->registerScriptFile($baseUrl .$path_style. '/plugins/jQuery/jquery-2.2.3.min.js'); ?>
<?php $cs->registerScriptFile($baseUrl .$path_style. '/bootstrap/js/bootstrap.min.js'); ?>
<?php $cs->registerScriptFile($baseUrl .$path_style. '/plugins/iCheck/icheck.min.js'); ?>
<?php $cs->registerScriptFile($baseUrl .$path_style. '/plugins/fastclick/fastclick.js'); ?>
<?php $cs->registerScriptFile($baseUrl .$path_style. '/dist/js/app.min.js'); ?>
<?php $cs->registerScriptFile($baseUrl .$path_style. '/dist/js/select2.js'); ?>
<?php $cs->registerScriptFile($baseUrl .$path_style. '/dist/js/icheck.js'); ?>

<!--tags js-->
<?php $cs->registerScriptFile($baseUrl .$path_style. '/dist/js/tags-my.js'); ?>

<?php $cs->registerScriptFile($baseUrl .$path_style. '/dist/js/main.js'); ?>

</body>
</html>