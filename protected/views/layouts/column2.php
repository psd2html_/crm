<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>



<div class="col-md-3">
    <div class="box box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Операции</h3>

            <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body no-padding">
            <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items'=>$this->menu,
                    'htmlOptions'=>array('class'=>'nav nav-pills nav-stacked'),
                ));
            ?>
        </div>
    </div>
</div>

<?php echo $content; ?>


<?php $this->endContent(); ?>