<br/>
<br/>
<br/>

<?php if (Yii::app()->user->isGuest) : ?>
    <ul class="sidebar-menu">
        <li><a href="/user/login"><i class="fa fa-circle-o text-aqua"></i> <span>Войти</span></a></li>
        <li class="header">Зарегистрироваться</li>
        <li><a href="/user/registration?type=freelancer"><i class="fa fa-circle-o text-red"></i><span>Как исполнитель</span></a></li>
        <li><a href="/user/registration?type=manager"><i class="fa fa-circle-o text-red"></i><span>Как менеджер</span></a></li>
    </ul>
<?php endif; ?>

<?php

$admin_menu = array(

    // Menu for Super Administrator
    array(
        'label' => 'Роли',
        'url' => array('/rights'),
        'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['admin']))
    ),
    array(
        'label' => 'Список юзеров',
        'url' => array('/user'),
        'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['admin']))
    ),
    array(
        'label' => 'Управление юзерами',
        'url' => array('/user/admin'),
        'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['admin']))
    ),
    array(
        'label' => 'Создать юзера',
        'url' => array('/user/admin/create'),
        'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['admin']))
    ),
    array(
        'label' => 'Настройка полей',
        'url' => array('/user/profileField/admin'),
        'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['admin']))
    ),
    array(
        'label' => 'Работа с сайтом',
        'itemOptions' => array('class' => 'header'),
        'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['admin']))
    ),
    array(
        'label' => 'Новости',
        'url' => array('/news'),
        'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['admin']))
    ),
    array('label' => 'Страницы',
        'url' => array('/pages'),
        'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['admin']))),

    array(
        'label' => 'Дополнительно',
        'itemOptions' => array('class' => 'header'),
        'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['admin']))
    ),
    array(
        'label' => 'Список фрилансеров',
        'url' => array('/admin/freelancers'),
        'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['admin']))
    ),
    array(
        'label' => '--------------',
        'itemOptions' => array('class' => 'header'),
        'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['admin']))
    ),
);

?>

<?php
    $menu_profile = array (
        array('label'=> '<i class="fa fa-bars"></i><span>Профиль</span>', 'url'=>array('/user/profile')),
        array('label'=> '<i class="fa fa-bars"></i><span>Редактировать профиль</span>', 'url'=>array('/user/profile/edit')),
        array('label'=> '<i class="fa fa-bars"></i><span>Изменить пароль</span>', 'url'=>array('/user/profile/changepassword')),
    );
?>


<?php

    $all_menu = array_merge($admin_menu, $menu_profile, $this->menu);

    $this->widget('zii.widgets.CMenu', array(
        'htmlOptions' => array('class' => 'sidebar-menu'),
        'encodeLabel' => false,

        //'linkLabelWrapper' => 'span',

        'items' => $all_menu

            /*


        )*/
    ));
?>
