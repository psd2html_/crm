<?php //if (!Yii::app()->user->isGuest and (Yii::app()->controller->id !== 'site')) : ?>

<?php
$this->widget('zii.widgets.CMenu', array(
    'htmlOptions' => array(
        'class' => 'nav navbar-nav'
    ),
    'items' => array(

        // Menu for Super Administrator
//        array('label' => 'Роли',                        'url' => array('/rights'),                  'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['admin']))),
//        array('label' => 'Список юзеров',               'url' => array('/user'),                    'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['admin']))),
//        array('label' => 'Управление юзерами',          'url' => array('/user/admin'),              'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['admin']))),
//        array('label' => 'Создать юзера',               'url' => array('/user/admin/create'),       'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['admin']))),
//        array('label' => 'Настройка полей',             'url' => array('/user/profileField/admin'), 'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['admin']))),
//        array('label' => 'Новости',                     'url' => array('/news'),                    'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['admin']))),
//        array('label' => 'Страницы',                    'url' => array('/pages'),                   'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['admin']))),







        // Menu for Manager
        array('label' => 'Проекты',
            'url' => array('/projects/indexManager'),
            'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['manager']))
        ),
        array('label' => 'Проекты c заявками',
            'url' => array('/projects/proposal'),
            'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['manager']))
        ),
        array('label' => 'Проекты в работе',
            'url' => array('/projects/active'),
            'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['manager']))
        ),
        array('label' => 'Завершенные',
            'url' => array('/projects/completed'), // архивные/удаленные проекты менеджера
            'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['manager']))
        ),
        array('label' => 'Скрытые проекты',  // неопубликованные,скрытые
            'url' => array('/projects/hidden'),
            'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['manager']))
        ),
        array('label' => 'В корзине',
            'url' => array('/projects/deleted'), // архивные/удаленные проекты менеджера
            'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['manager']))
        ),


        // Общие пункты  меню
        array('label' => 'Главная',
            'url' => '/',
            //'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['manager']))
            'visible' => Yii::app()->user->isGuest
        ),
        array('label' => 'Контакты',
            'url' => array('/site/contact'),
            //'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['manager']))
            'visible' => Yii::app()->user->isGuest
        ),
        array('label' => 'FAQ',
            'url' => array('/site/faq'),
            //'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['manager']))
            'visible' => Yii::app()->user->isGuest
        ),
        array('label' => 'О нас',
            'url' => array('/site/about'),
            //'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['manager']))
            'visible' => Yii::app()->user->isGuest
        ),


        // для фрилансера
        array('label' => 'Проекты',
            'url' => array('/projects/index'),
            'visible' => C_Rights::isFreelancer()
        ),
        array('label' => 'Ваши заказы',
            'url' => array('/projects/offers'),
            'visible' => C_Rights::isFreelancer()
        ),
        array('label' => 'Проекты в работе' . C_User::inJob(),
            'url' => array('/projects/myprojects'),
            'visible' => C_Rights::isFreelancer()
        ),
        array('label' => 'Завершенные',
            'url' => array('/projects/completed'),
            'visible' => C_Rights::isFreelancer()
        ),


        // Common
        //array('label' => 'Выход', 'url' => array('/user/logout')),
    ),
    'encodeLabel' => false,
));
?>
<?php //endif; ?>