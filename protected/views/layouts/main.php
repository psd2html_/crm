<?php
    $user = C_User::getCurrentUser();
?>
<?php require('header.php'); ?>
    <body class="sidebar-mini skin-green-light" style="height: auto;">

        <div class="wrapper-777" style="height: auto;">
            <header class="main-header">

                <a href="/" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini">CRM</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg">CRM</span>
                </a>
                <nav class="navbar navbar-static-top">

                    <div class="container-fluid">
                        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </a>

                        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                            <?php require('mainmenu.php'); ?>
                        </div>

                        <?php if (!Yii::app()->user->isGuest) : ?>
                            <div class="navbar-custom-menu">
                                <ul class="nav navbar-nav">

                                    <!-- Комментарии Фрилансера -->
                                    <?php if (C_Rights::checkRoleCurrentUser('freelancer')) : ?>
                                        <?php $UnreadComments = C_User::getUnreadCommentsForFreelancer(); ?>
                                        <?php
                                            $sum = 0;
                                            foreach ($UnreadComments as $proposal) {
                                                if (isset($proposal->ppComments) and !empty($proposal->ppComments)) {
                                                    $sum += count($proposal->ppComments);
                                                }
                                            }
                                        ?>
                                        <li class="dropdown messages-menu">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-envelope-o"></i>
                                                <?php if ($sum) : ?>
                                                    <span class="label label-warning"><?php echo $sum; ?></span>
                                                <?php endif; ?>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li class="header"><?php echo 'У вас '.$sum. ' непрочитанных сообщений'; ?></li>

                                                <?php $sum = 0; foreach ($UnreadComments as $proposal) : ?>
                                                    <?php
                                                    if (isset($proposal->ppComments) and !empty($proposal->ppComments)) {
                                                        $sum = count($proposal->ppComments);
                                                    }
                                                    ?>
                                                    <li>
                                                        <ul class="menu">
                                                            <li>
                                                                <a href="/projects/view?id=<?php echo $proposal->project_id; ?>">
                                                                    <i class="fa fa-users text-aqua"></i> <?php echo $sum; ?> сообщений по "<b><?php echo substr(C_Projects::getProject($proposal->project_id)->title, 0, 30); ?></b>"
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </li>
                                    <?php endif; ?>

                                    <!-- Заявки Менеджера -->
                                    <?php if (C_Rights::checkRoleCurrentUser('manager')) : ?>
                                        <?php $UnreadProposal = C_User::getUnreadProposalForManager(); ?>
                                        <?php
                                            $sum = 0;
                                            foreach ($UnreadProposal as $mmp) {
                                                if (isset($mmp->proposalCount) and !empty($mmp->proposalCount)) {
                                                    $sum = $sum + $mmp->proposalCount;
                                                }
                                            }
                                        ?>
                                        <li class="dropdown notifications-menu">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-bell-o"></i>
                                                <?php if ($sum) : ?>
                                                    <span class="label label-warning"><?php echo $sum; ?></span>
                                                <?php endif; ?>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li class="header"><?php echo 'У вас '.$sum. ' новых заявок'; ?></li>
                                                <?php foreach ($UnreadProposal as $projects) : ?>
                                                    <li>
                                                        <ul class="menu">
                                                            <li>
                                                                <a href="/projects/view?id=<?php echo $projects->id; ?>">
                                                                    <i class="fa fa-users text-aqua"></i> <?php echo $projects->proposalCount; ?> заявок в <b><?php echo substr($projects->title, 0, 35); ?>...</b>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </li>
                                    <?php endif; ?>

                                    <!-- Комментарии -->
                                    <?php if (C_Rights::checkRoleCurrentUser('manager')) : ?>
                                        <?php $UnreadComments = C_User::getUnreadCommentsForManager(); ?>
                                        <?php
                                            $sum = 0;
                                            foreach ($UnreadComments as $proposal) {
                                                if (isset($proposal->ppComments) and !empty($proposal->ppComments)) {
                                                    $sum += count($proposal->ppComments);
                                                }
                                            }
                                        ?>
                                        <li class="dropdown messages-menu">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-envelope-o"></i>
                                                <?php if ($sum) : ?>
                                                    <span class="label label-warning"><?php echo $sum; ?></span>
                                                <?php endif; ?>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li class="header"><?php echo 'У вас '.$sum. ' непрочитанных комметариев'; ?></li>

                                                <?php $sum = 0; foreach ($UnreadComments as $proposal) : ?>
                                                <?php
                                                    if (isset($proposal->ppComments) and !empty($proposal->ppComments)) {
                                                        $sum = count($proposal->ppComments);
                                                    }
                                                ?>
                                                    <li>
                                                        <ul class="menu">
                                                            <li>
                                                                <a href="/projects/view?id=<?php echo $proposal->project_id; ?>">
                                                                    <i class="fa fa-users text-aqua"></i> <?php echo $sum; ?> сообщений от <b><?php echo C_User::getUsernameByID($proposal->user_id); ?></b>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </li>
                                    <?php endif; ?>

                                    <li class="dropdown user user-menu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <img src="<?php echo C_User::getAvatarCurrentUser(); ?>" class="user-image">
                                            <?php if (isset($user->username) and !empty($user->username)) : ?>
                                                <span class="hidden-xs"><?php echo $user->username; ?></span>
                                            <?php endif; ?>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <!-- User image -->
                                            <li class="user-header">

                                                    <img src="<?php echo C_User::getAvatarCurrentUser(); ?>" class="img-circle">


                                                <p>
                                                    <?php if (isset($user->profile->first_name) and isset($user->profile->last_name)) : ?>
                                                        <?php echo $user->profile->first_name . ' ' . $user->profile->last_name; ?>
                                                    <?php else :  ?>
                                                        Неизвестный
                                                    <?php endif; ?>
                                                    <small><?php echo 'Зарегистрирован: ' . $user->create_at ?></small>
                                                </p>

                                            </li>
                                            <!-- Menu Footer-->
                                            <li class="user-footer">
                                                <div class="pull-left">
                                                    <a href="/user/profile" class="btn btn-default btn-flat">Профиль</a>
                                                </div>
                                                <div class="pull-right">
                                                    <a href="/user/logout" class="btn btn-default btn-flat">Выйти</a>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        <?php  endif; ?>
                    </div>
                </nav>
            </header>

            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <?php //$this->widget('UserMenu'); ?>
                    <?php require('sidebar.php'); ?>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper" style="min-height: 976px;">

                <!-- Main content -->
                <section class="content">
                    <?php echo $content; ?>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
        </div>

<?php require('footer.php'); ?>