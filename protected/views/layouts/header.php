<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="en">
    <?php
    $baseUrl = Hp::baseUrl();
    $cs = Hp::cs();
    $path_style = '/css/bootstrap';
    ?>
    <!-- jQuery UI CSS -->
    <?php $cs->registerCssFile($baseUrl . $path_style .  '/bootstrap/css/bootstrap.min.css'); ?>
    <?php $cs->registerCssFile($baseUrl . $path_style .  '/dist/css/select2.css'); ?>
    <?php $cs->registerCssFile($baseUrl . $path_style .  '/dist/css/AdminLTE.min.css'); ?>
    <?php $cs->registerCssFile($baseUrl . $path_style .  '/plugins/iCheck/square/green.css'); ?>

    <?php $cs->registerCssFile($baseUrl . $path_style .  '/dist/css/skins/_all-skins.min.css'); ?>
    <?php $cs->registerCssFile($baseUrl . $path_style .  '/plugins/iCheck/flat/green.css'); ?>
    <?php $cs->registerCssFile($baseUrl . $path_style .  '/dist/css/main.css'); ?>
    <?php $cs->registerCssFile($baseUrl . $path_style .  '/dist/css/tags.css'); ?>
    <?php $cs->registerCssFile($baseUrl .  '/css/custom.css'); ?>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>