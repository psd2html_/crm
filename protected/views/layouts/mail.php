<table style="border-spacing:0;width:100%;background-color:#f1f2f6;table-layout:fixed;">
    <tbody>
    <tr>
        <td align="center" style="padding-right:0;padding-left:0;vertical-align:top;padding-top:15px;padding-bottom:0;">
            <center>
                <div style="font-size:20px;line-height:20px;display:block;"></div>
                <table style="border-spacing:0;width:650px;min-width:650px;"><tbody><tr><td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;font-size:1px;line-height:1px;"></td></tr></tbody></table>
                <img src="<?php echo Yii::app()->getBaseUrl(true) . '/images/logo.png'; ?>" style="border-width:0;-ms-interpolation-mode:bicubic;margin:0 auto;">
            </center>
        </td>
    </tr>
    </tbody>
</table>

<table style="border-spacing:0;width:100%;background-color:#f1f2f6;table-layout:fixed;">
    <tbody>
    <tr>
        <td align="center" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;">
            <div style="font-size:40px;line-height:20px;height:20px;display:block;"></div>
            <center>
                <table width="610" style="border-spacing:0;margin:0 auto;">
                    <tbody>
                    <tr>
                        <td width="100%" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;">
                            <table width="100%" style="border-spacing:0;">
                                <tbody>
                                <tr>
                                    <td width="100%" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;height:3px;font-size:3px;line-height:3px;border-left-width:1px;border-left-style:solid;border-left-color:#e2e3e7;border-top-width:1px;border-top-style:solid;border-top-color:#e2e3e7;border-right-width:1px;border-right-style:solid;border-right-color:#e2e3e7;border-top-left-radius:3px;border-top-right-radius:3px;background-color:#ffffff;border-bottom-style:none;">

                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <?php echo $content; ?>

                <table width="612" style="border-spacing:0;margin:0 auto;">
                    <tbody>
                    <tr>
                        <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;background-color:#edeff3;width:1px;"></td>
                        <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;background-color:#e3e6ec;width:1px;"></td>
                        <td width="608" bgcolor="#ffffff" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;margin:0 auto;">
                            <table width="100%" style="border-spacing:0;">
                                <tbody>
                                <tr>
                                    <td style="vertical-align:top;padding-bottom:20px;padding-left:70px;padding-right:70px;padding-top:30px;">
                                        <table width="100%" style="border-spacing:0;">
                                            <tbody>
                                            <tr>
                                                <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;">
                                                    <p style="margin-top:0;font-weight:normal;color:#677483;font-family:sans-serif;font-size:14px;line-height:25px;margin-bottom:15px;">Спасибо, <br>
                                                        <em style="color:#a9b7c8;font-style:italic;">команда <?php echo CHtml::encode(Yii::app()->name); ?>.</em></p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                        <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;background-color:#e3e6ec;width:1px;"></td>
                        <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;background-color:#edeff3;width:1px;"></td>
                    </tr>
                    </tbody>
                </table>
                <table width="610" style="border-spacing:0;margin:0 auto;">
                    <tbody>
                    <tr>
                        <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;">
                            <table width="100%" style="border-spacing:0;">
                                <tbody>
                                <tr>
                                    <td width="610" style="background-color:#ffffff;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;height:3px;font-size:3px;line-height:3px;border-left-width:1px;border-left-style:solid;border-left-color:#e2e3e7;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#e2e3e7;border-right-width:1px;border-right-style:solid;border-right-color:#e2e3e7;border-bottom-left-radius:3px;border-bottom-right-radius:3px;">

                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table width="608" style="border-spacing:0;margin:0 auto;">
                    <tbody>
                    <tr style="font-size:1px;line-height:1px;"><td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;background-color:#e3e6ec;width:1px;"></td></tr>
                    </tbody>
                </table>
                <table width="606" style="border-spacing:0;margin:0 auto;">
                    <tbody>
                    <tr style="font-size:1px;line-height:1px;"><td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;background-color:#e6e9ee;width:1px;"></td></tr>
                    </tbody>
                </table>
                <table width="604" style="border-spacing:0;margin:0 auto;">
                    <tbody>
                    <tr style="font-size:1px;line-height:1px;"><td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;background-color:#edeff3;width:1px;"></td></tr>
                    </tbody>
                </table>
                <table width="600" style="border-spacing:0;margin:0 auto;">
                    <tbody>
                    <tr style="font-size:1px;line-height:1px;"><td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;background-color:#edeff3;width:1px;"></td></tr>
                    </tbody>
                </table>
            </center>
        </td>
    </tr>
    </tbody>
</table>

<div style="background-color:#f1f2f6;line-height:40px;height:40px;display:block;">&nbsp;</div>