<?php
$this->menu=array(
	array('label'=>'<i class="fa fa-circle-o text-aqua"></i><span>Создать проект</span>', 'url'=>array('create'), 'visible' => C_Rights::checkRole(array(Yii::app()->params['roles']['manager']))),
);
?>
<div class="col-md-12">

    <?php foreach(Yii::app()->user->getFlashes() as $key => $message) : ?>
        <div class="col-md-8 row">
            <div class="alert alert-<?php echo $key; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Уведомление!</h4>
                <?php echo $message; ?>
            </div>
        </div>
    <?php endforeach; ?>




    <div class="row border-bottom">
        <div class="projects col-md-9 no-gutter">

            <div class="col-md-10">
                <div class="row2">
                    <p class="text-orange" style="font-size: 30px"><?php echo $model['title']; ?></p>
                </div>
            </div>
            <div class="col-md-2">
                <div class="row">

                    <div class="font-op-s fz-22  right text-left">
                        Бюджет: <label class="text-green middle text-bold"><?php echo $model['budget']; ?></label>
                    </div>
                </div>
            </div>

            <div class="mb15 col-xs-12 border-bottom mb30">
                <div class="left mr15">
                    <span class="block">Добавил:</span>
                    <span class="block">Дата:</span>
                </div>
                <div class="left mr30">
                    <a class="block hov" href="/user/user/review/id/<?php echo $model['user_id']; ?>"><?php echo C_User::getUsernameByID($model['user_id']); ?></a>
                    <span class="block"><?php echo $model['create_at']; ?></span>
                </div>
                <!--                <div class="left ml15">-->
                <!--                    <span class="block">Название:<strong class="label bg-red ml15">Значение</strong></span>-->
                <!--                    <span class="block">Название:<strong class="label bg-green ml15">Значение</strong></span>-->
                <!--                </div>-->
                <!--                <div class="left ml15">-->
                <!--                    <span class="block">Название:<strong class="label bg-red ml15">Значение</strong></span>-->
                <!--                    <span class="block">Название:<strong class="label bg-green ml15">Значение</strong></span>-->
                <!--                </div>-->

            </div>




    <!--            <div class="mb15 col-xs-12">-->
    <!--                <span class="mr15">Важность: <strong>Низкая</strong></span>-->
    <!--                <span class="mr15">Сложность: <strong>Нормальная</strong></span>-->
    <!--                <span class="mr15">Тип: <strong>Обычный проект</strong></span>-->
    <!--                <span>Статус: <strong>На согласовании</strong></span>-->
    <!--            </div>-->
            <div class="col-md-12">
                <div class="order-description">
                    <p><?php echo $model['description']; ?></p>
                </div>
            </div>



            <!--ФАЙЛЫ-->
            <br/>

            <div class="col-md-12">
                <?php if (isset($files) and !empty($files)) : ?>
                    <h4>Прикрепленные файлы</h4>
                    <ul class="mailbox-attachments clearfix">
                        <?php foreach ($files as $file) : ?>
                            <li>
                                <span class="mailbox-attachment-icon"><i class="fa fa-file-pdf-o"></i></span>
                                <div class="mailbox-attachment-info">
                                    <?php echo CHtml::link('<i class="fa fa-paperclip"></i>'.$file, array('/projects/download', 'project_id' => $model->id, 'file' => $file), array('class'=>'mailbox-attachment-name')); ?>
                                </div>
                            </li>
                        <?php endforeach; ?>

                    </ul>
                <?php endif; ?>

                <?php if (isset($model['category_id']) and !empty($model['category_id'])) : ?>
                    <?php $_services = Yii::app()->params['services'];  ?>
                    <?php foreach ($_services as $s_key => $s_value) : ?>
                        <?php foreach ($s_value as $k_key => $val) : ?>
                            <?php if ($model['category_id'] == $k_key) : ?>
                                <div class="mb30">
                                    <strong class="block">Разделы:</strong>
                                    <a class="hov" href="#"><?php echo $s_key ?></a>
                                    <span>/</span>
                                    <a class="hov" href="#"><?php echo $val; ?></a>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                <?php endif; ?>

                <!-- ТЕГИ -->
                <?php if (isset($model['tags']) and !empty($model['tags'])) : ?>
                    <div class="tags-block big">
                        <div class="description-tags">
                            <?php
                                $parse = json_decode($model['tags']);
                            ?>
                            <?php foreach ($parse as $_value) : ?>
                                <span class="meta-tag fw" style="background-color: <?php echo $_value->color; ?>" unselectable="on"><?php echo $_value->name; ?></span>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>

        </div>
    </div>
</div>


<div class="col-md-8">

    <?php $z=0; ?>

    <div class="row">
        <?php if (isset($modelProjectProposal) and !empty($modelProjectProposal)) : ?>

            <h2>Предложения пользователей</h2>

            <?php foreach ($modelProjectProposal as $proposal) :  ?>

                <?php if (C_Rights::checkRoleCurrentUser('freelancer')) : ?>

                    <?php if ($proposal->user_id == Yii::app()->user->id) : $z++; ?>

                        <div class="box box-success direct-chat direct-chat-success">
                            <div class="box-body">
                                <div class="direct-chat-messages77">
                                    <div class="direct-chat-msg">
                                        <div class="direct-chat-info clearfix">
                                            <span class="direct-chat-name pull-left">Ваше предложение</span>
                                            <span class="direct-chat-timestamp pull-right"><?php echo $proposal->create_at; ?></span>
                                        </div>
                                        <img src="<?php echo C_User::getAvatarCurrentUser(); ?>" class="direct-chat-img">
                                        <div class="direct-chat-text">
                                            <p><?php echo $proposal->description; ?></p>
                                            <div class="invoice-col">
                                                <b>Бюджет: </b><?php echo $proposal->budget; ?><br>
                                                <b>Срок выполнения:</b> <?php echo $proposal->period; ?><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <ul class="info-about-order col-sm-12 no-gutter clearfix">
                                <?php
                                    if ($proposal->ppComments)
                                    {
                                        $xc = 0;
                                        $c_count = count($proposal->ppComments);
                                        echo '<li class="left">Комментариев: <small class="label bg-blue">'. $c_count .'</small></li>';

                                        foreach ($proposal->ppComments as $comments)
                                        {
                                            if ( ($comments['status'] == ProjectsProposalComments::STATUS_UNREAD)
                                                and ($comments['user_id'] != Yii::app()->user->id))
                                            {
                                                $xc++;
                                            }
                                        }
                                        if ($xc > 0)
                                            echo '<li class="left"><small class="label bg-red">новых: '. $xc .'</small></li>';
                                        else
                                            echo '<li class="left">новых: '. $xc .'</li>';
                                    }
                                ?>
                            </ul>


                            <div class="box-footer">
                                <?php
                                    echo CHtml::link('Перейти в комментарии', array('ProjectsProposalComments/index',
                                        //'id' => $proposal->user_id,
                                        //'project_id' => $model->id,
                                        'project_proposal_id' => $proposal->id
                                        ),
                                        array('class' => 'btn btn-success')
                                    );
                                ?>
                                <?php

                                    // есть есть исполнитель
                                    // если есть исполнители
                                    if (isset($model->pAssignUser) and !empty($model->pAssignUser))
                                    {

                                        if ($model->pAssignUser['user_id'] == $proposal->user_id)
                                        {
                                            echo '--- исполнитель ---';
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>

                <?php if (C_Rights::checkRoleCurrentUser('manager')) : ?>

                    <div class="box box-success direct-chat direct-chat-success">
                        <div class="box-body">
                            <div class="direct-chat-messages77">
                                <div class="direct-chat-msg">
                                    <div class="direct-chat-info clearfix">
                                        <span class="direct-chat-name pull-left"><?php echo User::model()->findByPk($proposal->user_id)->username; ?>

                                            <?php if (isset($model->pAssignUser) and !empty($model->pAssignUser)) : ?>
                                                <?php if ($model->pAssignUser['user_id'] == $proposal->user_id) : ?>
                                                    ---- <span class="label label-danger">ИСПОЛНИТЕЛЬ</span>
                                                <?php endif; ?>
                                            <?php endif; ?>


                                        </span>
                                        <span class="direct-chat-timestamp pull-right"><?php echo $proposal->create_at; ?></span>
                                    </div>
                                    <img src="<?php echo C_User::getAvatarUserByID($proposal->user_id); ?>" class="direct-chat-img">
                                    <div class="direct-chat-text">
                                        <p><?php echo $proposal->description; ?></p>
                                        <div class="invoice-col">
                                            <b>Бюджет: </b><?php echo $proposal->budget; ?><br>
                                            <b>Срок выполнения:</b> <?php echo $proposal->period; ?><br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">


                            <ul class="info-about-order col-sm-12 no-gutter clearfix">
                                <?php
                                    if ($proposal->ppComments)
                                    {
                                        $xc = 0;
                                        $c_count = count($proposal->ppComments);
                                        echo '<li class="left">Комментариев: <small class="label bg-blue">'. $c_count . '</small></li>';

                                        foreach ($proposal->ppComments as $comments)
                                        {
                                            if ( ($comments['status'] == ProjectsProposalComments::STATUS_UNREAD)
                                             and ($comments['user_id'] != Yii::app()->user->id))
                                            {
                                                $xc++;
                                            }
                                        }
                                        if ($xc > 0)
                                            echo '<li class="left"><small class="label bg-red">новых: '. $xc .'</small></li>';
                                        else
                                            echo '<li class="left">новых: '. $xc .'</li>';
                                    }
                                ?>
                            </ul>

                            <?php
                            echo CHtml::link('Перейти в комментарии', array('ProjectsProposalComments/index',
                                //'id' => $proposal->user_id,
                                //'project_id' => $model->id,
                                'project_proposal_id' => $proposal->id
                            ),
                                array('class' => 'btn btn-success')
                            );
                            ?>
                            <?php
                                if (C_Rights::checkRoleCurrentUser('manager'))
                                {
                                    // если есть исполнители
                                    if (!isset($model->pAssignUser) and empty($model->pAssignUser))
                                    {
                                        echo CHtml::link('Выбрать исполнителем', array('projects/assign',
                                            'id' => $proposal->user_id,
                                            'project_id' => $model->id,
                                            'project_proposal_id' => $proposal->id
                                        ), array('class' => 'btn btn-info'));

                                    }
                                }
                            ?>
                        </div>
                    </div>
                <?php endif; ?>



            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>

<br/>
<br/>
<br/>
<br/>

<?php if (C_Rights::checkRoleCurrentUser('freelancer')) : ?>
    <?php if ($z == 0) : ?>
        <?php $this->renderPartial('_form2', array('model'=> $modelProposal)); ?>
    <?php endif; ?>
<?php endif; ?>