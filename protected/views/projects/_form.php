<?php
/* @var $this ProjectsController */
/* @var $model Projects */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'projects-form',
    'htmlOptions' => array('enctype' => 'multipart/form-data'),

	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>



	<?php echo $form->errorSummary($model); ?>


        <p class="bold text-orange col-sm-12"><i class="fa fa-bug"></i>Добавить задачу</p>


        <div class="form-group col-sm-9">
            <?php echo $form->labelEx($model,'title'); ?>
            <?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255, 'class' => 'form-control', 'placeholder' => 'название')); ?>
            <?php echo $form->error($model,'title'); ?>
        </div>


        <div class="col-sm-12 no-gutter">
            <div class="box-body pad col-md-9">
                <?php echo $form->labelEx($model,'description'); ?>
                <?php //echo $form->textArea($model,'description',array('rows'=>10, 'cols'=>50, 'class' => 'col-md-12 textarea', 'id' => 'textarea')); ?>

                <?php
                    $this->widget('application.extensions.ckeditor.ECKEditor', array(
                        'model' => $model,
                        'attribute' => 'description',
                        'language' => 'ru',
                        'editorTemplate' => 'full', // 'full','basic','advanced'
                        'height' => '400px'
                    ));
                ?>

                <?php echo $form->error($model,'description'); ?>
            </div>
        </div>



        <div class="col-sm-12 add-project-button-box">
            <label for="download-files">
<!--                <input placeholder="+Добавить" type="file" multiple id="download-files">-->
                <?php echo CHtml::activeFileField($model, 'file[]', array('multiple' => 'multiple')); ?>
            </label>
            <span class="block">Не более 10 файлов общим объемом до 5 Мб</span>
        </div>

        <div class="col-sm-12 no-gutter">
            <div class="form-group mb30 specialisation no-gutter">
                <div class="col-sm-4 mr15">
                    <label>Специализация проекта</label>
                    <?php
                        $_services = Yii::app()->params['services'];

                    ?>

                    <select class="form-control" id="top_category" name="Projects[category_id]">
                        <option selected disabled>Выберите раздел</option>
                        <?php $x=0; foreach ($_services as $_key => $_value) : $x++; ?>

                            <optgroup label="<?php echo $_key; ?>">
                                <?php foreach ($_value as $__key => $__value) : ?>
                                    <option value="<?php echo $__key; ?>"><?php echo $__value; ?></option>
                                <?php endforeach; ?>
                            </optgroup>


                        <?php endforeach; ?>
                    </select>
                </div>

            </div>



            <div class="tags-block mb30 clearfix">
                <div class="description-tags col-sm-9 mb15">
                    <div class="general-tags">
                        <span class="meta-tag pageProof" unselectable="on">Вёрстка</span>
                        <span class="meta-tag design" unselectable="on">Дизайн</span>
                        <span class="meta-tag mobapplication" unselectable="on">Mobile-приложения</span>
                        <span class="meta-tag fullProject" unselectable="on">Сайт "под ключ"</span>
                        <span class="meta-tag withzero" unselectable="on">С нуля</span>
                    </div>
                    <div class="general-tags">
                        <span id="self" class="meta-tag self" unselectable="on">Самопис</span>
                        <span class="meta-tag changes" unselectable="on">Правка/Доработка</span>
                    </div>
                    <div class="general-tags">
                        <span id="fw" class="meta-tag fw" unselectable="on">Framework</span>
                        <span class="meta-tag database" unselectable="on">Базы данных</span>
                        <span class="meta-tag php" unselectable="on">PHP</span>
                        <span class="meta-tag cms" unselectable="on">CMS</span>
                        <span class="meta-tag crm" unselectable="on">CRM/ERP</span>
                        <span class="meta-tag cmsdevelopment" unselectable="on">Разработка CMS</span>
                    </div>
                    <div class="general-tags">
                        <span class="meta-tag html5" unselectable="on">HTML/CSS</span>
                        <span class="meta-tag js" unselectable="on">JavaScript/JS</span>
                        <span class="meta-tag less" unselectable="on">LESS</span>
                        <span class="meta-tag bootstrap" unselectable="on">Bootstrap</span>
                    </div>
                    <div class="general-tags">
                        <span class="meta-tag socnet" unselectable="on">Соц. сети</span>
                        <span class="meta-tag testing" unselectable="on">Тестирование</span>
                        <span class="meta-tag serverconfig" unselectable="on">Настройка сервера</span>
                    </div>
                    <div class="general-tags">
                        <span class="meta-tag drafting" unselectable="on">Составление ТЗ</span>
                        <span class="meta-tag smarty" unselectable="on">Smarty</span>
                    </div>
                    <div class="general-tags">
                        <span class="meta-tag viruses" unselectable="on">Вирусы</span>
                        <span class="meta-tag parsers" unselectable="on">Парсеры</span>
                        <span class="meta-tag modules" unselectable="on">Модули</span>
                    </div>
                </div>

                <div class="description-tags col-sm-9">
                    <div class="sub-items fw-types">
                        <span class="meta-tag head" unselectable="on">Frameworks:</span>
                        <span class="meta-tag yii" unselectable="on">/Yii</span>
                        <span class="meta-tag laravel" unselectable="on">/Laravel</span>
                        <span class="meta-tag symfony" unselectable="on">/Symfony</span>
                        <span class="meta-tag codeigniter" unselectable="on">/CodeIgniter</span>
                        <span class="meta-tag kohana" unselectable="on">/Kohana</span>
                        <span class="meta-tag zend" unselectable="on">/Zend</span>
                    </div>
                    <div class="sub-items pageProof-types">
                        <span class="meta-tag head" unselectable="on">Верстка:</span>
                        <span class="meta-tag adapted" unselectable="on">/Адаптивная</span>
                        <span class="meta-tag responsive" unselectable="on">/Резиновая</span>
                        <span class="meta-tag cross_browser" unselectable="on">/Кроссбраузерная</span>
                        <span class="meta-tag block-type" unselectable="on">/Блочная</span>
                        <span class="meta-tag pixel-perfect" unselectable="on">/PixelPerfect</span>
                    </div>
                    <div class="sub-items design-types">
                        <span class="meta-tag head" unselectable="on">Дизайн:</span>
                        <span class="meta-tag site_design" unselectable="on">/Дизайн сайтов</span>
                        <span class="meta-tag interfaces" unselectable="on">/Интерфейсы</span>
                        <span class="meta-tag logos" unselectable="on">/Логотипы</span>
                        <span class="meta-tag prototype" unselectable="on">/Прототипирование</span>
                        <span class="meta-tag corporate_identity" unselectable="on">/Фирменный стиль</span>
                        <span class="meta-tag app-design" unselectable="on">/Дизайн приложения</span>
                        <span class="meta-tag print_design" unselectable="on">/Полиграфический дизайн</span>
                        <span class="meta-tag infographics" unselectable="on">/Инфографика</span>
                        <span class="meta-tag icons" unselectable="on">/Иконки</span>
                        <span class="meta-tag banners" unselectable="on">/Баннеры</span>
                    </div>
                    <div class="sub-items fullProject-types">
                        <span class="meta-tag head" unselectable="on">Сайт "под ключ":</span>
                        <span class="meta-tag cutaway" unselectable="on">/Визитка</span>
                        <span class="meta-tag corporate" unselectable="on">/Корпоративный</span>
                        <span class="meta-tag blog" unselectable="on">/Блог</span>
                        <span class="meta-tag onlineShop" unselectable="on">/Интернет-магазин</span>
                        <span class="meta-tag portal" unselectable="on">/Портал</span>
                        <span class="meta-tag landing" unselectable="on">/Лендинг</span>
                        <span class="meta-tag service-type" unselectable="on">/Сервис</span>
                    </div>
                    <div class="sub-items cms-types">
                        <span class="meta-tag head" unselectable="on">CMS:</span>
                        <span class="meta-tag wordpress" unselectable="on">/Wordpress</span>
                        <span class="meta-tag opencart" unselectable="on">/OpenCart</span>
                        <span class="meta-tag joomla" unselectable="on">/Joomla</span>
                        <span class="meta-tag magento" unselectable="on">/Magento</span>
                        <span class="meta-tag modx" unselectable="on">/ModX</span>
                        <span class="meta-tag bitrix" unselectable="on">/Bitrix</span>
                        <span class="meta-tag drupal" unselectable="on">/Drupal</span>
                        <span class="meta-tag prestashop" unselectable="on">/PrestaShop</span>
                        <span class="meta-tag cs_cart" unselectable="on">/CS-Cart</span>
                        <span class="meta-tag imagecms" unselectable="on">/ImageCMS</span>
                        <span class="meta-tag oscommerce" unselectable="on">/osCommerce</span>
                        <span class="meta-tag phpshop" unselectable="on">/PHPShop</span>
                        <span class="meta-tag shop_script" unselectable="on">/Shop-Script</span>
                        <span class="meta-tag phpbb" unselectable="on">/phpBB</span>
                        <span class="meta-tag vbulletin" unselectable="on">/vBulletin</span>
                        <span class="meta-tag virtuemart" unselectable="on">/VirtueMart</span>
                        <span class="meta-tag zen_cart" unselectable="on">/Zen Cart</span>
                    </div>
                    <div class="sub-items js-types">
                        <span class="meta-tag head" unselectable="on">JavaScript/JS:</span>
                        <span class="meta-tag jquery" unselectable="on">/jQuery</span>
                        <span class="meta-tag angularjs" unselectable="on">/Angular.js</span>
                        <span class="meta-tag nodejs" unselectable="on">/Node.js</span>
                        <span class="meta-tag backbonejs" unselectable="on">/Backbone.js</span>
                        <span class="meta-tag ajax-type" unselectable="on">/AJAX</span>
                    </div>
                    <div class="sub-items socnet-types">
                        <span class="meta-tag head" unselectable="on">Соц. сети:</span>
                        <span class="meta-tag facebook" unselectable="on">/Facebook</span>
                        <span class="meta-tag vkontakte" unselectable="on">/Vkontakte</span>
                        <span class="meta-tag googleplus" unselectable="on">/Google+</span>
                        <span class="meta-tag twitter" unselectable="on">/Twitter</span>
                        <span class="meta-tag instagram" unselectable="on">/Instagram</span>
                    </div>
                    <div class="sub-items mobapplication-types">
                        <span class="meta-tag head" unselectable="on">Mobile приложение:</span>
                        <span class="meta-tag android" unselectable="on">/Android</span>
                        <span class="meta-tag java" unselectable="on">/Java</span>
                        <span class="meta-tag ios" unselectable="on">/iOS</span>
                        <span class="meta-tag ipad" unselectable="on">/iPad</span>
                        <span class="meta-tag iphone" unselectable="on">/iPhone</span>
                        <span class="meta-tag objectivec" unselectable="on">/Objective-C</span>
                        <span class="meta-tag swift" unselectable="on">/Swift</span>
                    </div>
                    <div class="sub-items database-types">
                        <span class="meta-tag head" unselectable="on">Базы данных:</span>
                        <span class="meta-tag mysql" unselectable="on">/MySql</span>
                        <span class="meta-tag mongo-db" unselectable="on">/Mongo-DB</span>
                        <span class="meta-tag postgressql" unselectable="on">/PostgresSQL</span>
                        <span class="meta-tag no-sql" unselectable="on">/NoSQL</span>
                        <span class="meta-tag memcashed" unselectable="on">/Memcashed</span>
                    </div>
                </div>

                <input type="hidden" name="tags" id="t_tags_list" value="">
            </div>
        </div>

        <div class="col-sm-12 budget no-gutter">


            <strong class="block">Бюджет</strong>
            <div class="form-group col-sm-2">
                <?php echo $form->textField($model,'budget',array('size'=>60,'maxlength'=>255,'class' => 'form-control', 'placeholder' => 'Например: 700 руб.')); ?>
                <?php echo $form->error($model,'budget'); ?>
            </div>




<!--            <div class="form-group left">-->
<!--                <select class="form-control left col-sm-2">-->
<!--                    <option selected value="Руб.">Руб.</option>-->
<!--                    <option value="Грн.">Грн.</option>-->
<!--                    <option value="Еда">Еда</option>-->
<!--                </select>-->
<!--            </div>-->
<!--            <div class="form-group left">-->
<!--                <select class="form-control left col-sm-1">-->
<!--                    <option selected value="За проект">За проект</option>-->
<!--                    <option value="За час">За час</option>-->
<!--                    <option value="За месяц">За месяц</option>-->
<!--                </select>-->
<!--            </div>-->
<!--            <div class="form-group left">-->
<!--                <div class="checkbox">-->
<!--                    <label>-->
<!--                        или-->
<!--                        <input class="ch" type="checkbox">-->
<!--                        по договоренности-->
<!--                    </label>-->
<!--                </div>-->
<!--            </div>-->
        </div>


<!--        <div class="col-sm-12 no-gutter mb5">-->
<!--            <div class="form-group col-md-10 col-lg-9">-->
<!--                <label class="col-sm-5 col-md-4 col-lg-3 control-label">Срок, дней:</label>-->
<!--                <div class="col-sm-7 col-md-8 col-lg-6">-->
<!--                    <input type="text" class="form-control" placeholder="Срок">-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="col-sm-12 no-gutter mb5">-->
<!--            <div class="form-group col-md-10 col-lg-9">-->
<!--                <label class="col-sm-5 col-md-4 col-lg-3 control-label">Срок хранения, дней:</label>-->
<!--                <div class="col-sm-7 col-md-8 col-lg-6">-->
<!--                    <input type="text" class="form-control" placeholder="7">-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="col-sm-12 no-gutter mb5">-->
<!--            <div class="form-group col-md-10 col-lg-9">-->
<!--                <label class="col-sm-5 col-md-4 col-lg-3 control-label" for="complexity">Сложность</label>-->
<!--                <div class="col-sm-7 col-md-8 col-lg-6">-->
<!--                    <select class="form-control" id="complexity">-->
<!--                        <option selected value="Сложный">Сложный</option>-->
<!--                        <option value="Простой">Простой</option>-->
<!--                        <option value="Нереальный">Нереальный</option>-->
<!--                    </select>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->


<!--        <div class="col-sm-12 no-gutter mb5">-->
<!--            <div class="form-group col-md-10 col-lg-9">-->
<!--                <label class="col-sm-5 col-md-4 col-lg-3 control-label" for="importance">Важность</label>-->
<!--                <div class="col-sm-7 col-md-8 col-lg-6">-->
<!--                    <select class="form-control" id="importance">-->
<!--                        <option selected value="Важный">Важный</option>-->
<!--                        <option value="Неважный">Неважный</option>-->
<!--                        <option value="Ненужный">Ненужный</option>-->
<!--                    </select>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->


<!--        <div class="col-sm-12 no-gutter mb5">-->
<!--            <div class="form-group col-md-10 col-lg-9">-->
<!--                <label class="col-sm-5 col-md-4 col-lg-3 control-label" for="status">Статус</label>-->
<!--                <div class="col-sm-7 col-md-8 col-lg-6">-->
<!--                    <select class="form-control" id="status">-->
<!--                        <option selected value="Выполненный">Выполненный</option>-->
<!--                        <option value="Невыполненный">Невыполненный</option>-->
<!--                        <option value="Оконченный">Оконченный</option>-->
<!--                    </select>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->


<!--        <div class="col-sm-12 no-gutter mb5">-->
<!--            <div class="form-group col-md-10 col-lg-9">-->
<!--                <label class="col-sm-5 col-md-4 col-lg-3 control-label" for="type">Тип</label>-->
<!--                <div class="col-sm-7 col-md-8 col-lg-6">-->
<!--                    <select class="form-control" id="type">-->
<!--                        <option selected value="Какой-то">Какой-то</option>-->
<!--                        <option value="Не известный">Не известный</option>-->
<!--                        <option value="Знакомый">Знакомый</option>-->
<!--                    </select>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->


<!--        <div class="row">-->
<!--            --><?php //echo $form->labelEx($model,'status'); ?>
<!--            --><?php //echo $form->textField($model,'status', array('value' => '1')); ?>
<!--            --><?php //echo $form->error($model,'status'); ?>
<!--        </div>-->

        <div class="col-sm-12 add-project-button-box">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array('class'=>'col-sm-2 btn btn-success')); ?>
        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->