<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'projects-proposal-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="projects col-lg-9">
    <p class="bold">Ваш ответ по проекту</p>
    <div class="form-group">
        <?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'form-control', 'placeholder'=>'Кратко опишите суть Вашего предложения, условия сотрудничества, вопросы и необходимые требования. Ваш ответ и переписка по нему видна только менеджеру и вам.')); ?>
        <?php echo $form->error($model,'description'); ?>
    </div>
</div>



<div class="col-lg-12 mb15 col-xs-12">
    <div class="some-box left flex mr30 vertical-bottom">
        <div class="form-group left mr15">
            <?php echo $form->labelEx($model,'budget'); ?>
            <?php echo $form->textField($model,'budget',array('size'=>60,'maxlength'=>255, 'class'=>'form-control', 'placeholder'=>'Например: 1200 руб.')); ?>
            <?php echo $form->error($model,'budget'); ?>
        </div>
<!--        <div class="form-group left">-->
<!--            <select class="form-control" name="" id="">-->
<!--                <option selected value="One">One</option>-->
<!--                <option value="Two">Two</option>-->
<!--                <option value="Three">Three</option>-->
<!--            </select>-->
<!--        </div>-->
    </div>
    <div class="some-box left flex vertical-bottom">
        <div class="form-group left mr15">
            <?php echo $form->labelEx($model,'period'); ?>
            <?php echo $form->textField($model,'period',array('size'=>60,'maxlength'=>255, 'class'=>'form-control', 'placeholder'=>'Например: 5-6 дней')); ?>
            <?php echo $form->error($model,'period'); ?>
        </div>
<!--        <div class="form-group left">-->
<!--            <select class="form-control" name="" id="">-->
<!--                <option selected value="One">One</option>-->
<!--                <option value="Two">Two</option>-->
<!--                <option value="Three">Three</option>-->
<!--            </select>-->
<!--        </div>-->
    </div>
</div>
<div class="col-sm-5 col-md-4 col-lg-3">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Отправить' : 'Отправить', array('class'=>'btn btn-success btn-block')); ?>
</div>
<?php $this->endWidget(); ?>