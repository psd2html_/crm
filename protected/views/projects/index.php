<div class="row">
    <p class="bold text-black col-sm-8">Список проектов</p>
</div>
<?php //$this->widget('zii.widgets.CListView', array(
//	'dataProvider' => $dataProvider,
//	'itemView'=>'_view',
//)); ?>



<?php foreach ($model as $data) : ?>

    <div class="row border-bottom mb30 col-md-9">
        <div class="clearfix ml15">
            <div class="col-md-10">
                <div class="row">
                    <?php echo CHtml::link('<i class="fa fa-bug"></i>'.CHtml::encode($data->title), array('view', 'id' => $data->id), array('class' => 'hov text-orange middle left')); ?>
                </div>
            </div>
            <div class="col-md-2">
                <div class="row">
                    <div class="font-op-s fz-22  right text-left">
                        <label class="text-green middle text-bold"><?php echo $data->budget; ?></label>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-12 mb15">
            <?php echo C_String::printContent($data->description, 50); ?>
        </div>

        <!-- ТЕГИ -->
        <?php if (isset($data->tags) and !empty($data->tags)) : ?>
            <div class="col-md-12">
                <div class="tags-block big">
                    <div class="description-tags">
                        <?php
                        $parse = json_decode($data->tags);
                        ?>
                        <?php foreach ($parse as $_value) : ?>
                            <span class="meta-tag fw" style="background-color: <?php echo $_value->color; ?>" unselectable="on"><?php echo $_value->name; ?></span>
                        <?php endforeach; ?>

                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-md-12 clearfix">
                <span class="left col-md-12"><?php echo date("d.m.Y H:i", strtotime($data->create_at)); ?>



                    <?php if (isset($data['LinkProposal']) and !empty($data['LinkProposal'])) : ?>
                        <?php foreach ($data['LinkProposal'] as $pp) : ?>
                            <?php if ($pp['user_id'] == Yii::app()->user->id) : ?>
                                | <strong><?php echo CHtml::link('Ваше предложение', array('view', 'id' => $data->id), array('class' => 'hov')); ?></strong>
                            <?php break; endif;  ?>
                       <?php endforeach; ?>
                    <?php endif; ?>



                    <!--            | Тип: <strong>Обычный проект</strong> | Важность: <strong class="text-green">Низкая</strong> | Статус: <strong class="text-green">На согласовании</strong></span>-->
    <!--            <div class="like-box right">-->
    <!--                <div class="active-like-box" style="width: 50%"></div>-->
    <!--            </div>-->
<!--                <div class="answers right text-right">-->
<!--                    <i class="fa fa-commenting-o"></i>-->
<!--                    <a class="text-black hov" href="#">17 ответов </a>-->
<!--                </div>-->
            </div>
        </div>
    </div>

<?php endforeach; ?>

<div class="col-lg-9 col-sm-12">
    <?php
        $this->widget('CLinkPager', array(
            'pages' => $pages,
            'header' => '',
            'maxButtonCount' => 6,
            //'prevPageLabel' => '&laquo; назад',
            //'nextPageLabel' => 'далее &raquo;',
            'htmlOptions' => array(
                'class' => 'pagination'
            ),
        ));
    ?>
</div>


<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('budget')); ?>:</b>
	<?php echo CHtml::encode($data->budget); ?>
	<br />

	*/ ?>


<!--<div class="row">-->
<!--    <div class="col-sm-9">-->
<!--        <ul class="pagination">-->
<!--            <li class="paginate_button previous disabled" id="example2_previous">-->
<!--                <a href="#" aria-controls="example2" data-dt-idx="0" tabindex="0">Previous</a>-->
<!--            </li>-->
<!--            <li class="paginate_button active">-->
<!--                <a href="#" aria-controls="example2" data-dt-idx="1" tabindex="0">1</a>-->
<!--            </li>-->
<!--            <li class="paginate_button ">-->
<!--                <a href="#" aria-controls="example2" data-dt-idx="2" tabindex="0">2</a>-->
<!--            </li>-->
<!--            <li class="paginate_button ">-->
<!--                <a href="#" aria-controls="example2" data-dt-idx="3" tabindex="0">3</a>-->
<!--            </li>-->
<!--            <li class="paginate_button ">-->
<!--                <a href="#" aria-controls="example2" data-dt-idx="4" tabindex="0">4</a>-->
<!--            </li>-->
<!--            <li class="paginate_button ">-->
<!--                <a href="#" aria-controls="example2" data-dt-idx="5" tabindex="0">5</a>-->
<!--            </li>-->
<!--            <li class="paginate_button ">-->
<!--                <a href="#" aria-controls="example2" data-dt-idx="6" tabindex="0">6</a>-->
<!--            </li>-->
<!--            <li class="paginate_button next" id="example2_next">-->
<!--                <a href="#" aria-controls="example2" data-dt-idx="7" tabindex="0">Next</a>-->
<!--            </li>-->
<!--        </ul>-->
<!--    </div>-->
<!--</div>-->
