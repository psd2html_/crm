<?php

$this->menu = array(
    array('label' => '<i class="fa fa-circle-o text-aqua"></i><span>Создать проект</span>', 'url'=>array('create')),
    //array('label'=>'<i class="fa fa-circle-o text-aqua"></i><span>Список проектов</span>', 'url'=>array('admin')),
);
?>

<h1>Список проектов</h1>


<div class="row">
    <div class="projects col-sm-9">
        <div class="border-bottom">
            <div class="cover-nav no-gutter">
                <div class="col-sm-2">
                    <a href="/projects/create" class="btn btn-success">Опубликовать проект</a>
                </div>
            </div>
        </div>
    </div>

    <?php foreach ($model as $project): ?>

        <div class="row">
            <div class="projects col-sm-12 col-lg-9 border-bottom clearfix">
                <div class="col-sm-12 no-gutter clearfix">
                    <p class="bold col-sm-10 col-lg-8 col-xs-10"><?php echo CHtml::link($project->title,array('projects/view', 'id' => $project->id)); ?></p>
                    <div class="tools-box bold col-sm-1 col-xs-1">
                        <a class="tools" href="#"><span class="fa fa-gear"></span></a>
                        <ul class="hint-box">
                            <li><?php echo CHtml::link('<i class="fa fa-edit"></i>Редактировать' ,array('projects/update', 'id' => $project->id), array('class' => 'text-gray h5')); ?></li>
                            <li><?php echo CHtml::link('<i class="fa fa-eye-slash"></i>Снять с публикации' ,array('projects/unpublished', 'id' => $project->id, 'status' => Projects::STATUS_UNPUBLISHED), array('class' => 'text-gray h5')); ?></li>
                            <li><?php echo CHtml::link('<i class="fa fa-archive"></i>Положить в корзину' ,array('projects/archived', 'id' => $project->id, 'status' => Projects::STATUS_ARCHIVED), array('class' => 'text-gray h5')); ?></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-12">
                    <strong class="text-green fz-22 text-bold font-op-s"><?php echo $project->budget; ?></strong>
                </div>
                <ul class="info-about-order col-sm-12 no-gutter clearfix">
                    <li class="left"><?php echo $project->create_at; ?></li>

                    <!-- РАЗДЕЛЫ -->
                    <?php if (isset($project->category_id) and !empty($project->category_id)) : ?>
                        <?php $_services = Yii::app()->params['services'];  ?>
                        <?php foreach ($_services as $s_key => $s_value) : ?>
                            <?php foreach ($s_value as $k_key => $val) : ?>
                                <?php if ($project->category_id == $k_key) : ?>
                                    <li class="left">Разделы:  <a class="text-blue hov" href="#"><?php echo $s_key ?></a>  / <a class="text-blue hov" href="#"><?php echo $val; ?></a></li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>

                    <?php
                    $x=0;
                    foreach ($project->LinkProposal as $proposal) {
                        if ($proposal->status == 0)
                            $x++;
                    }
                    ?>
                    <li class="left">
                        <small class="label bg-blue"><?php echo $project->proposalCount;  ?> заявок</small>


                        <?php if ($x > 0) : ?>
                            | <small class="label bg-red"><?php echo $x; ?> новых</small>
                        <?php endif; ?>
                    </li>
                </ul>


                <ul class="info-about-order col-sm-12 no-gutter clearfix">
                    <?php
                        $getUnreadCommentsByProjectID = C_User::getUnreadCommentsByProjectID($project->id);
                        $countC = count($getUnreadCommentsByProjectID);
                        echo '<li class="left">Комментариев: <small class="label bg-blue">'. $countC .'</small></li>';

                        // считаем непрочитанные
                        $x=0;
                        foreach ($getUnreadCommentsByProjectID as $value) {
                            if  ($value['status'] == 0)
                                $x++;
                        }
                        if ($x > 0)
                            echo '<li class="left"><small class="label bg-red">новых: '. $x .'</small></li>';
                        else
                            echo '<li class="left">новых: '. $x .'</li>';
                    ?>
                </ul>

                <?php if (isset($project->pAssignUser) and !empty($project->pAssignUser)) : ?>
                    <div class="col-sm-12">
                        <div class="executor-box col-sm-12 no-gutter">
                            <div class="col-sm-9 col-lg-11">
                                <p class="text-bold">
                                    Исполнитель определен --- [<a class="text-black hov" href="#"><?php echo C_User::getUsernameByID($project->pAssignUser->user_id); ?></a>]
                                    <br/>
                                    <?php echo CHtml::link('Перети к дискусии', array('/projectsProposalComments/index', 'project_proposal_id' => $project->pAssignUser->project_proposal_id));  ?>
                                </p>
                            </div>

                            <?php //echo $project->description; ?>

<!--                            <td>Статус:</td>-->
                            <?php if (isset($project->pAssignUser) and !empty($project->pAssignUser)) : ?>
                                <td><?php //echo $project->pAssignUser->status; ?></td>
                                <td><?php echo CHtml::link('Проставить оценки', array('projectsUserRate/create', 'project_assign_user_id' => $project->pAssignUser->id));  ?></td>
                            <?php endif; ?>

                        </div>
                    </div>
                <?php endif; ?>

            </div>
        </div>
    <?php endforeach; ?>

    <div class="col-lg-9 col-sm-12">
        <?php
            $this->widget('CLinkPager', array(
                'pages' => $pages,
                'header' => '',
                'maxButtonCount' => 6,
                //'prevPageLabel' => '&laquo; назад',
                //'nextPageLabel' => 'далее &raquo;',
                'htmlOptions' => array(
                    'class' => 'pagination'
                ),
            ));
        ?>
    </div>

</div>