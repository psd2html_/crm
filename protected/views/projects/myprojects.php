
<div class="row">
    <p class="bold text-black col-sm-8">Список открытых проектов в работе</p>
</div>
<?php //$this->widget('zii.widgets.CListView', array(
//	'dataProvider' => $dataProvider,
//	'itemView'=>'_view',
//)); ?>



<?php foreach ($model as $data) : ?>

    <div class="row border-bottom mb30 col-md-9">
        <div class="clearfix ml15">
            <div class="col-md-9">
                <div class="row">
                    <?php echo CHtml::link('<i class="fa fa-bug"></i>'.CHtml::encode($data->title), array('view', 'id' => $data->id), array('class' => 'hov text-orange middle left')); ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="font-op-s fz-22  right text-left">
                        <label class="text-green middle text-bold"><?php echo $data->budget; ?></label>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-12 mb15">
            <?php echo CHtml::encode(C_String::printContent(strip_tags($data->description), 50)); ?>
        </div>

        <!-- ТЕГИ -->
        <?php if (isset($data->tags) and !empty($data->tags)) : ?>
            <div class="col-md-12">
                <div class="tags-block big">
                    <div class="description-tags">
                        <?php
                        $parse = json_decode($data->tags);
                        ?>
                        <?php foreach ($parse as $_value) : ?>
                            <span class="meta-tag fw" style="background-color: <?php echo $_value->color; ?>" unselectable="on"><?php echo $_value->name; ?></span>
                        <?php endforeach; ?>

                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-md-12 clearfix">
                <span class="left col-md-12"><?php echo CHtml::encode($data->create_at); ?>
                    <!--            | Тип: <strong>Обычный проект</strong> | Важность: <strong class="text-green">Низкая</strong> | Статус: <strong class="text-green">На согласовании</strong></span>-->
    <!--            <div class="like-box right">-->
    <!--                <div class="active-like-box" style="width: 50%"></div>-->
    <!--            </div>-->
<!--                <div class="answers right text-right">-->
<!--                    <i class="fa fa-commenting-o"></i>-->
<!--                    <a class="text-black hov" href="#">17 ответов </a>-->
<!--                </div>-->
            </div>
        </div>
    </div>

<?php endforeach; ?>




<!--<h1>Мои проекты где я выбран исполнителем</h1>-->
<!---->
<!---->
<!---->
<!---->
<?php //foreach ($model as $value) : ?>
<!--    <table>-->
<!--        <tr>-->
<!--            <td>--><?php //echo $value->title; ?><!--</td>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <td>Manager: --><?php //echo C_User::getUsernameByID($value->user_id);  ?><!--</td>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <td>Создан: --><?php //echo $value->create_at;  ?><!--</td>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <td>-->
<!--                --><?php
//                    echo CHtml::link('Перейти к проекту', array('ProjectsProposalComments/index',
//                        //'id' => $proposal->user_id,
//                        //'project_id' => $model->id,
//                        'project_proposal_id' => $value->pAssignUser->project_proposal_id
//                    ));
//                ?>
<!--            </td>-->
<!--        </tr>-->
<!--    </table>-->
<!---->
<!---->
<?php //endforeach; ?>
