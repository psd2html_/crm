<?php
/* @var $this ProjectsUserRateController */
/* @var $model ProjectsUserRate */

$this->breadcrumbs=array(
	'Projects User Rates'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ProjectsUserRate', 'url'=>array('index')),
	array('label'=>'Create ProjectsUserRate', 'url'=>array('create')),
	array('label'=>'Update ProjectsUserRate', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ProjectsUserRate', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProjectsUserRate', 'url'=>array('admin')),
);
?>

<h1>View ProjectsUserRate #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'pau_id',
		'skills',
		'availability',
		'quality',
		'deadlines',
		'cooperation',
		'communication',
	),
)); ?>
