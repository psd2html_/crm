

<div class="col-md-7">
    <div class="form-horizontal">
        <div class="box-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'projects-user-rate-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>


    <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
    ?>

	<h4>Поставьте плз оценки от 1 до 5 по каждому пункту</h4>

	<?php echo $form->errorSummary($model); ?>

            <div class="form-group">
		<?php echo $form->labelEx($model,'skills', array('class'=>'col-sm-4 control-label')); ?>
                <div class="col-sm-8">
		<?php echo $form->textField($model,'skills', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'skills'); ?>

	</div>
	</div>

            <div class="form-group">
		<?php echo $form->labelEx($model,'availability', array('class'=>'col-sm-4 control-label')); ?>
                <div class="col-sm-8">
		<?php echo $form->textField($model,'availability', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'availability'); ?>
	</div>
	</div>

            <div class="form-group">
		<?php echo $form->labelEx($model,'quality', array('class'=>'col-sm-4 control-label')); ?>
                <div class="col-sm-8">
		<?php echo $form->textField($model,'quality', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'quality'); ?>
	</div>
	</div>

            <div class="form-group">
		<?php echo $form->labelEx($model,'deadlines', array('class'=>'col-sm-4 control-label')); ?>
                <div class="col-sm-8">
		<?php echo $form->textField($model,'deadlines', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'deadlines'); ?>
	</div>
	</div>

            <div class="form-group">
		<?php echo $form->labelEx($model,'cooperation', array('class'=>'col-sm-4 control-label')); ?>
                <div class="col-sm-8">
		<?php echo $form->textField($model,'cooperation', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'cooperation'); ?>
	</div>
	</div>

            <div class="form-group">
		<?php echo $form->labelEx($model,'communication', array('class'=>'col-sm-4 control-label')); ?>
                <div class="col-sm-8">
		<?php echo $form->textField($model,'communication', array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'communication'); ?>
	</div>
	</div>

    <div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Сохранить' : 'Сохранить', array('class'=>'btn btn-block btn-success btn-lg')); ?>
	</div>

<?php $this->endWidget(); ?>

</div>
</div>
</div>
<div class="col-md-5">
    Пояснение:
    <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
        <b>Навыки</b> - уровень знаний технологий в рамках данного проекта. <br/>
        <b>Доступность</b> - был ли всегда на связи исполнитель.<br/>
        <b>Качество</b> - качество выполненного данного проекта.<br/>
        <b>Сроки выполнения</b> - выполнил ли исполнитель в срок данный проект.<br/>
        <b>Сотрудничество</b> - как прошло ваше сотрудничество исполнителем (например: с конфликтами или без).<br/>
        <b>Коммуникация</b> - как проходило общение между вами и исполнителем.
    </p>
</div>