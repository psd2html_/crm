<?php
/* @var $this ProjectsUserRateController */
/* @var $model ProjectsUserRate */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pau_id'); ?>
		<?php echo $form->textField($model,'pau_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'skills'); ?>
		<?php echo $form->textField($model,'skills'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'availability'); ?>
		<?php echo $form->textField($model,'availability'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'quality'); ?>
		<?php echo $form->textField($model,'quality'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'deadlines'); ?>
		<?php echo $form->textField($model,'deadlines'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cooperation'); ?>
		<?php echo $form->textField($model,'cooperation'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'communication'); ?>
		<?php echo $form->textField($model,'communication'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->