<?php
/* @var $this ProjectsUserRateController */
/* @var $model ProjectsUserRate */

$this->breadcrumbs=array(
	'Projects User Rates'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProjectsUserRate', 'url'=>array('index')),
	array('label'=>'Create ProjectsUserRate', 'url'=>array('create')),
	array('label'=>'View ProjectsUserRate', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProjectsUserRate', 'url'=>array('admin')),
);
?>

<h1>Update ProjectsUserRate <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>