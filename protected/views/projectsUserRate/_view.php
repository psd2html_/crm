<?php
/* @var $this ProjectsUserRateController */
/* @var $data ProjectsUserRate */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pau_id')); ?>:</b>
	<?php echo CHtml::encode($data->pau_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('skills')); ?>:</b>
	<?php echo CHtml::encode($data->skills); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('availability')); ?>:</b>
	<?php echo CHtml::encode($data->availability); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('quality')); ?>:</b>
	<?php echo CHtml::encode($data->quality); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deadlines')); ?>:</b>
	<?php echo CHtml::encode($data->deadlines); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cooperation')); ?>:</b>
	<?php echo CHtml::encode($data->cooperation); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('communication')); ?>:</b>
	<?php echo CHtml::encode($data->communication); ?>
	<br />

	*/ ?>

</div>