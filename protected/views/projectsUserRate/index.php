<?php
/* @var $this ProjectsUserRateController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Projects User Rates',
);

$this->menu=array(
	array('label'=>'Create ProjectsUserRate', 'url'=>array('create')),
	array('label'=>'Manage ProjectsUserRate', 'url'=>array('admin')),
);
?>

<h1>Projects User Rates</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
