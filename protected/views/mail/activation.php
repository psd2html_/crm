<table width="612" style="border-spacing:0;margin:0 auto;">
    <tbody>
    <tr>
        <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;background-color:#edeff3;width:1px;"></td>
        <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;background-color:#e3e6ec;width:1px;"></td>
        <td width="608" bgcolor="#ffffff" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;margin:0 auto;">
            <table width="100%" style="border-spacing:0;">
                <tbody>
                <tr>
                    <td style="vertical-align:top;padding-bottom:10px;padding-left:70px;padding-right:70px;padding-top:30px;">
                        <table width="100%" style="border-spacing:0;">
                            <tbody>
                            <tr>
                                <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;">
                                    <h1 style="font-family:sans-serif;font-weight:bold;margin-top:0;text-align:left;font-size:22px;line-height:26px;margin-bottom:20px;color:#445359;"><?php echo $welcome_text; ?></h1>
                                    <p style="margin-top:0;font-weight:normal;color:#677483;font-family:sans-serif;font-size:14px;line-height:25px;margin-bottom:15px;"><?php echo $text; ?></p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;background-color:#e3e6ec;width:1px;"></td>
        <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;background-color:#edeff3;width:1px;"></td>
    </tr>
    </tbody>
</table>
<table width="630" style="border-spacing:0;margin:0 auto;">
    <tbody>
    <tr>
        <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;">
            <table width="100%" style="border-spacing:0;">
                <tbody>
                <tr>
                    <td align="center" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;border-radius:3px;border-width:1px;border-style:solid;border-color:#c8ee99;background-color:#f5ffe4;">
                        <center>
                            <table width="628" style="border-spacing:0;border-radius:3px;">
                                <tbody>
                                <tr>
                                    <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;font-size:9px;line-height:9px;width:9px;border-radius:3px;background-color:#f0fde1;color:#f0fde1;"></td>
                                    <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;">
                                        <table width="100%" style="border-spacing:0;border-left-width:1px;border-left-style:solid;border-left-color:#edfade;border-right-width:1px;border-right-style:solid;border-right-color:#edfade;">
                                            <tbody>
                                            <tr>
                                                <td style="vertical-align:top;text-align:left;padding-left:70px;padding-right:70px;padding-bottom:30px;padding-top:30px;">
                                                    <div>
                                                        <a href="<?php echo $activation_url; ?>" style="mso-hide:all;background-color:#62a30c; background-repeat:repeat-x;border-width:1px;border-style:solid;border-color:#538c02;border-radius:3px;color:#ffffff;display:inline-block;font-weight:normal;font-family:sans-serif;font-size:14px;line-height:45px;text-align:center;text-decoration:none;-webkit-text-size-adjust:none;text-shadow:1px 2px 1px #548e19;box-shadow:0px 2px 2px 0px #e2ecd4;-webkit-box-shadow:0px 2px 2px 0px #e2ecd4;-moz-box-shadow:0px 2px 2px 0px #e2ecd4;width:260px;" target="_blank"><?php echo $activation_text; ?></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;font-size:9px;line-height:9px;width:9px;border-radius:3px;background-color:#f0fde1;color:#f0fde1;"></td>
                                </tr>
                                </tbody>
                            </table>
                        </center>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;">
            <center>
                <table width="610" style="border-spacing:0;margin:0 auto;">
                    <tbody>
                    <tr>
                        <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;background-color:#e3e6ec;width:1px;font-size:3px;line-height:3px;"></td>
                        <td width="608" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;background-color:#eff9e1;font-size:3px;line-height:3px;"></td>
                        <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;background-color:#e3e6ec;width:1px;font-size:3px;line-height:3px;"></td>
                    </tr>
                    </tbody>
                </table>
            </center>
        </td>
    </tr>
    </tbody>
</table>