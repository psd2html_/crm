<table width="612" style="border-spacing:0;margin:0 auto;">
    <tbody>
    <tr>
        <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;background-color:#edeff3;width:1px;"></td>
        <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;background-color:#e3e6ec;width:1px;"></td>
        <td width="608" bgcolor="#ffffff" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;margin:0 auto;">
            <table width="100%" style="border-spacing:0;">
                <tbody>
                <tr>
                    <td style="vertical-align:top;padding-bottom:10px;padding-left:70px;padding-right:70px;padding-top:30px;">
                        <table width="100%" style="border-spacing:0;">
                            <tbody>
                            <tr>
                                <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;">
                                    <h1 style="font-family:sans-serif;font-weight:bold;margin-top:0;text-align:left;font-size:22px;line-height:26px;margin-bottom:20px;color:#445359;"><?php echo $title; ?></h1>
                                    <p style="margin-top:0;font-weight:normal;color:#677483;font-family:sans-serif;font-size:14px;line-height:25px;margin-bottom:15px;">
                                        <?php echo $text; ?>
                                    </p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;background-color:#e3e6ec;width:1px;"></td>
        <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;background-color:#edeff3;width:1px;"></td>
    </tr>
    </tbody>
</table>