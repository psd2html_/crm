<!--<h1><i>--><?php //echo CHtml::encode(Yii::app()->name); ?><!--</i></h1>-->


<h1>Информация</h1>
<br/>

<p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
    Чтобы начать вам необходимо зарегистрироваться в системе. <br/>
    Если вы хотите выполнять заказы, то вам нужно зарегистрироваться как исполнитель.
</p>
<br/>

<?php foreach ($model as $value) : ?>


    <ul class="timeline">
        <li class="time-label">
        <span class="bg-aqua">
            <?php echo date("d.m.Y H:i", strtotime($value->create_at)); ?>
        </span>
        </li>
        <li>
            <i class="fa fa-envelope bg-blue"></i>
            <div class="timeline-item">
<!--                <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>-->

                <h3 class="timeline-header">
                    <?php echo CHtml::link($value->title, array('news/view', 'id' => $value->id)); ?>
                </h3>

                <div class="timeline-body">
                    <?php echo C_String::printContent($value->description, 20); ?>
                </div>

                <div class="timeline-footer">

                    <?php echo CHtml::link('подробнее ...', array('news/view', 'id' => $value->id), array('class'=>'btn btn-primary btn-xs')); ?>
                </div>
            </div>
        </li>
    </ul>


<?php endforeach; ?>

