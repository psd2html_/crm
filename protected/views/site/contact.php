<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle=Yii::app()->name . ' - Contact Us';
$this->breadcrumbs=array(
	'Contact',
);
?>

<h1>Контакты</h1>



<div class="box">
    <div class="box-body">



        <div class="col-md-4">

            <?php if(Yii::app()->user->hasFlash('contact')): ?>

                <div class="flash-success">
                    <?php echo Yii::app()->user->getFlash('contact'); ?>
                </div>

            <?php else: ?>

            <div class="row">

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'contact-form',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                        'validateOnSubmit'=>true,
                    ),
                )); ?>

                <?php echo $form->errorSummary($model); ?>

                <div class="form-group">
                    <?php echo $form->labelEx($model,'name'); ?>
                    <?php echo $form->textField($model,'name', array('class'=>'form-control')); ?>
                    <?php echo $form->error($model,'name'); ?>
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($model,'email'); ?>
                    <?php echo $form->textField($model,'email', array('class'=>'form-control')); ?>
                    <?php echo $form->error($model,'email'); ?>
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($model,'subject'); ?>
                    <?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>128,'class'=>'form-control')); ?>
                    <?php echo $form->error($model,'subject'); ?>
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($model,'body'); ?>
                    <?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50,'class'=>'form-control')); ?>
                    <?php echo $form->error($model,'body'); ?>
                </div>



                <div class="form-group">
                    <?php echo CHtml::submitButton('Отправить', array('class'=>'btn btn-block btn-success')); ?>
                </div>

                <?php $this->endWidget(); ?>

            </div><!-- form -->
            <?php endif; ?>
        </div>

        <div class="col-md-6">
            <div class="invoice-col">
                <h3>Руководитель компании</h3>
                Стёпочкин Артём
                <br/>
                <b>Skype:</b> <a href="skype:artemstepochkin">artemstepochkin</a> <br/>
                <b>Email:</b> <a href="mailto:artemstepochkin@artwebit.ru">artemstepochkin@artwebit.ru</a><br>


                <br/>
                <br/>
                <h3>Наши менеджеры</h3>
                Денис - <b>Skype:</b> <a href="skype:cell_nic">cell_nic</a><br/>
                Владислав - <b>Skype:</b> <a href="skype:vladislav_abrashkin">vladislav_abrashkin</a><br/>
            </div>
        </div>

    </div>
</div>













