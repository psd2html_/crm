<h1>Комментарии по проекту</h1>


<div class="col-md-12">

<!--    <div class="col-md-6">-->
<!--        <div class="row">-->
<!--            --><?php //echo $modelProjectProposal->lProjects->description; ?>
<!--        </div>-->
<!--    </div>-->

    <div class="col-md-8">

        <div class="row">

            <div class="col-md-6">
                <div class="row2">
                    <div class="box box-success box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Данные проекта</h3>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tr>
                                    <td>Название:</td>
                                    <td><?php echo CHtml::link($modelProjectProposal->lProjects->title, array('/projects/view', 'id' => $modelProjectProposal->project_id)); ?></td>
                                </tr>
                                <tr>
                                    <td>Менеджер:</td>
                                    <td><a class="block hov" href="/user/user/review/id/<?php echo $modelProjectProposal->lProjects->user_id; ?>"><?php echo C_User::getUsernameByID($modelProjectProposal->lProjects->user_id); ?></a></td>
                                </tr>
                                <tr>
                                    <td>Дата создания:</td>
                                    <td><?php echo $modelProjectProposal->lProjects->create_at; ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>


                <?php if (C_Rights::checkRoleCurrentUser('manager')) : ?>
                    <?php if ($modelProjectProposal->lProjects->status == Projects::STATUS_CLOSED) : ?>
                        <h2>Проект закрыт</h2>
                    <?php else : ?>
                        <?php echo CHtml::link('Завершить проект', array('/projects/close', 'project_id' => $modelProjectProposal->project_id, 'project_proposal_id' => $modelProjectProposal->id, 'manager_id' => $modelProjectProposal->lProjects->user_id), array('class' => 'btn btn-block btn-danger') ); ?>
                    <?php endif; ?>
                <?php endif; ?>


                <br/>
                <br/>
                <?php echo CHtml::link('Вернуться к проекту', array('/projects/view', 'id' => $modelProjectProposal->project_id)); ?>
            </div>

            <div class="col-md-6">
                <div class="row2">
                    <div class="box box-warning box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Предложение пользователя --- [ <?php echo C_User::getUsernameByID($modelProjectProposal->user_id); ?> ]</h3>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tr>
                                    <td>Дата:</td>
                                    <td><?php echo $modelProjectProposal->create_at; ?></td>
                                </tr>
                                <tr>
                                    <td>Срок:</td>
                                    <td><?php echo $modelProjectProposal->period; ?></td>
                                </tr>
                                <tr>
                                    <td>Бюджет:</td>
                                    <td><?php echo $modelProjectProposal->budget; ?></td>
                                </tr>
                                <tr>
                                    <td colspan="2">Первичное предложение: <p class="text-muted well well-sm no-shadow"><?php echo $modelProjectProposal->description; ?></p></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <?php
                                        if (isset($findAssignUser) and !empty($findAssignUser))
                                        {
                                            $goal = false;
                                            foreach ($findAssignUser as $value)
                                            {
                                                if ($value['user_id'] == Yii::app()->user->id)
                                                    echo '<span class="label label-danger">ВЫ ИСПОЛНИТЕЛЬ</span>';
                                            }
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </table>

                            <?php
                            if (C_Rights::checkRoleCurrentUser('manager'))
                            {
                                // если есть исполнители
                                if (isset($findAssignUser) and !empty($findAssignUser))
                                {
                                    $goal = false;
                                    foreach ($findAssignUser as $f_value)
                                    {
                                        if ($f_value['user_id'] == $modelProjectProposal->user_id)
                                        {
                                            $goal = true;
                                        }
                                    }
                                    if ($goal)
                                        echo '<span class="label label-danger">ИСПОЛНИТЕЛЬ</span>';
                                }
                                else
                                {
                                    echo CHtml::link('Выбрать исполнителем', array('projects/assign',
                                        'id'                    => $modelProjectProposal->user_id,
                                        'project_id'            => $modelProjectProposal->project_id,
                                        'project_proposal_id'   => $modelProjectProposal->id
                                    ), array('class' => 'btn btn-info'));
                                }
                            }
                            ?>

                        </div>
                    </div>
                </div>
            </div>



        </div>

        <div class="row">
            <div class="box box-success direct-chat direct-chat-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Комментарии</h3>
                </div>
                <div class="box-body">
                    <div class="direct-chat-messages-777">

                        <?php foreach ($model as $value) : ?>

                            <?php if (Yii::app()->user->id != $value->user_id) : ?>

                                <div class="direct-chat-msg">
                                    <div class="direct-chat-info clearfix">
                                        <span class="direct-chat-name pull-left">
                                            <a class="block hov" href="/user/user/review/id/<?php echo $value->user_id; ?>">
                                                <?php echo C_User::getUsernameByID($value->user_id); ?>
                                            </a>
                                        </span>
                                        <span class="direct-chat-timestamp pull-right"><?php echo $value->create_at; ?></span>
                                    </div>
                                    <img class="direct-chat-img" src="<?php echo C_User::getAvatarUserByID($value->user_id); ?>" alt="Message User Image"><!-- /.direct-chat-img -->
                                    <div class="direct-chat-text">
                                        <?php echo $value->comments; ?>
                                    </div>
                                </div>

                            <?php endif; ?>

                            <?php if (Yii::app()->user->id == $value->user_id) : ?>

                                <div class="direct-chat-msg right">
                                    <div class="direct-chat-info clearfix">
                                        <span class="direct-chat-name pull-right">Ваш комментарий</span>
                                        <span class="direct-chat-timestamp pull-left"><?php echo $value->create_at; ?></span>
                                    </div>
                                    <img class="direct-chat-img" src="<?php echo C_User::getAvatarCurrentUser(); ?>" alt="Message User Image"><!-- /.direct-chat-img -->
                                    <div class="direct-chat-text">
                                        <?php echo $value->comments; ?>
                                    </div>
                                </div>
                            <?php endif; ?>

                        <?php endforeach; ?>

                    </div>
                </div>

                <?php if ($modelProjectProposal->lProjects->status == Projects::STATUS_CLOSED) : ?>
                    <h2>Комментирование закрыто</h2>
                <?php else : ?>
                    <div class="box-footer">
                        <?php $this->renderPartial('_form', array('model'=> $form)); ?>
                    </div>
                <?php endif; ?>



            </div>
        </div>
    </div>
</div>