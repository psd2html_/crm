<div class="input-group">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'projects-proposal-comments-form',
        'enableAjaxValidation'=>false,
    )); ?>


        <?php //echo $form->textArea($model,'comments',array('rows'=>6, 'cols'=>50, 'class' => 'form-control')); ?>


    <?php
    $this->widget('application.extensions.ckeditor.ECKEditor', array(
        'model' => $model,
        'attribute' => 'comments',
        'language' => 'ru',
        'editorTemplate' => 'basic', // 'full','basic','advanced'
        'height' => '100px'
    ));
    ?>

    <br/>
    <br/>

            <?php echo CHtml::submitButton($model->isNewRecord ? 'Отправить' : 'Отправить', array('class'=>'btn btn-success btn-flat')); ?>



    <?php $this->endWidget(); ?>

</div>
<br/>
<br/>
<br/>