<?php
/* @var $this ProjectsProposalCommentsController */
/* @var $model ProjectsProposalComments */

$this->breadcrumbs=array(
	'Projects Proposal Comments'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProjectsProposalComments', 'url'=>array('index')),
	array('label'=>'Manage ProjectsProposalComments', 'url'=>array('admin')),
);
?>

<h1>Create ProjectsProposalComments</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>