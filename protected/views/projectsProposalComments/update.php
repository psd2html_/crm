<?php
/* @var $this ProjectsProposalCommentsController */
/* @var $model ProjectsProposalComments */

$this->breadcrumbs=array(
	'Projects Proposal Comments'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProjectsProposalComments', 'url'=>array('index')),
	array('label'=>'Create ProjectsProposalComments', 'url'=>array('create')),
	array('label'=>'View ProjectsProposalComments', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProjectsProposalComments', 'url'=>array('admin')),
);
?>

<h1>Update ProjectsProposalComments <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>