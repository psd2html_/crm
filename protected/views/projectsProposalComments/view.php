<?php
/* @var $this ProjectsProposalCommentsController */
/* @var $model ProjectsProposalComments */

$this->breadcrumbs=array(
	'Projects Proposal Comments'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ProjectsProposalComments', 'url'=>array('index')),
	array('label'=>'Create ProjectsProposalComments', 'url'=>array('create')),
	array('label'=>'Update ProjectsProposalComments', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ProjectsProposalComments', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProjectsProposalComments', 'url'=>array('admin')),
);
?>

<h1>View ProjectsProposalComments #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'project_proposal_id',
		'user_id',
		'comments',
		'create_at',
	),
)); ?>
