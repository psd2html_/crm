<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name' => 'Artwebit CRM',

    // Перевод
    'sourceLanguage' => 'en',
    'language' => 'ru',
    'charset' => 'utf-8',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import' => array
    (
		'application.models.*',
		'application.components.*',

        // User
        'application.modules.user.models.*',
        'application.modules.user.components.*',

        // Rights
        'application.modules.rights.*',
        'application.modules.rights.models.*',
        'application.modules.rights.components.*',

        // Helpers
        'application.helpers.*',

        // YiiMailer
        'ext.YiiMailer.YiiMailer',

        // CKEditor
        'ext.ckeditor.*',

	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password' => '123',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),

        'user' => array(
            'hash' => 'md5',
            'sendActivationMail' => true,
            'loginNotActiv' => false,
            'activeAfterRegister' => false,
            'autoLogin' => true,
            'registrationUrl' => array('/user/registration'),
            'registrationUrlFreelancer' => array('/user/registration', 'type'=>'freelancer'),
            'registrationUrlManager' => array('/user/registration', 'type'=>'manager'),
            'recoveryUrl' => array('/user/recovery'),
            'loginUrl' => array('/user/login'),
            'returnUrl' => array('/projects/myprojects'),
            'returnLogoutUrl' => array('/user/login'),
            'captcha' => array(
                'registration' => false
            ),
        ),

        'rights' => array(
            //'install' => true
        ),



	),



	// application components
	'components'=>array(

        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            'class' => 'RWebUser',
            'loginUrl' => array('/user/login'),
        ),

        'authManager' => array(
            'class' => 'RDbAuthManager',
            'connectionID'=>'db',
            'defaultRoles'=>array('Authenticated', 'Guest'),
        ),


		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
            'rules'=>array(
                '<controller:\w+>/<id:\d+>'=>'<controller>/<id>',
                '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>/<id>',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
            ),
            'showScriptName' => false, // Скрываем index.php
            //'urlSuffix' => '.html',
		),

        'clientScript' => array(
            'scriptMap' => array(
                //'jquery.js' => false,
                'jquery.js' => '/css/bootstrap/plugins/jQuery/jquery-2.2.3.min.js',
                //'jquery.min.js' => false
            ),
            //'enableJavaScript' => false,
        ),


		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction' => YII_DEBUG ? null : 'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages

//				array(
//					'class'=>'CWebLogRoute',
//				),

			),
		),

        // Image extensions
        'image' => array(
            'class' => 'application.extensions.image.CImageComponent',
            // GD or ImageMagick
            'driver' => 'GD',
            // ImageMagick setup path
            //'params' => array('directory' => '/opt/local/bin'),
        ),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params' => array
    (
		// this is used in contact page
		'adminEmail'    => 'psd2html@mail.ru',
		'noreplyEmail'  => 'no_reply@artwebit.ru',


        'uploads_projects' => '/uploads/projects/',

        'roles' => array(
            'admin'         => 'Admin', // Administrator
            'manager'       => 'Manager', // Manager
            'authenticated' => 'Authenticated', // Investors and Sellers
            'freelancer'    => 'Freelancer',
        ),
        'roles_name' => array(
            'admin'         => 'Администратора', // Administrator
            'manager'       => 'Менеджер', // Manager
            'authenticated' => 'Простой пользователь', // Investors and Sellers
            'freelancer'    => 'Исполнитель',
        ),

        'services' => array( // эти цифры потом будет ID в БД
            'Разработка сайтов' => array ( // 0
                5 => 'Сайт «под ключ»',
                6 => 'Дизайн сайтов',
                7 => 'Верстка',
                8 => 'Веб-программирование',
                9 => 'Системы администрирования (CMS)',
                10 => 'QA (тестирование)',
                11 => 'Адаптивный дизайн',
                12 => 'Интернет-магазины',
                13 => 'Проектирование',
                14 => 'Юзабилити-анализ',
                15 => 'Доработка сайтов'
                    ),
            'Дизайн и Арт' => array ( // 1
                16 => 'Логотипы',
                17 => 'Векторная графика',
                18 => 'Фирменный стиль',
                19 => 'Баннеры',
                20 => 'Технический дизайн',
                21 => 'Иконки',
                22 => 'Полиграфический дизайн',
                23 => 'Интерфейсы',
                24 => 'Промышленный дизайн',
                25 => 'Дизайн сайтов',
                26 => 'Дизайн интерфейсов приложений',
                27 => 'Инфографика',
                    ),
            'Программирование' => array ( //2
                28 => 'Веб-программирование',
                29 => 'Базы данных',
                30 => 'Прикладное программирование',
                31 => 'Системное программирование',
                32 => '1С-программирование',
                33 => 'Программирование игр',
                34 => 'Программирование для сотовых телефонов и КПК',
                35 => 'QA (тестирование)',
                36 => 'Проектирование',
                37 => 'Плагины/Сценарии/Утилиты',
                38 => 'Разработка CRM и ERP',
                39 => 'Управление проектами разработки',
                    ),
            'Мобильные приложения' => array ( // 3
                40 => 'Google Android',
                41 => 'iOS',
                42 => 'Windows Phone',
                43 => 'Дизайн',
                44 => 'Прототипирование',
            ),
            'Сети и инфосистемы' => array ( // 4
                45 => 'ERP и CRM интеграции',
                46 => 'Администрирование баз данных',
                47 => 'Сетевое администрирование',
            )
        ),

        'skills' => array(
            1 => 'MySQL',
            2 => 'MongoDB',
            3 => 'NoSQL',
            4 => 'PostgreSQL',
            5 => 'NoSQL',
            6 => 'Memcashed',

            7 => 'HTML5',
            8 => 'CSS3',
            9 => 'LESS',
            10 => 'Bootstrap',

            11 => 'PHP',
            12 => 'Python',
            13 => 'Java',

            14 => 'XML',
            15 => 'JSON',

            16 => 'JS',
            17 => 'JavaScript',
            18 => 'jQuery',
            19 => 'Ajax',

            20 => 'NodeJS',
            21 => 'AngularJS',
            22 => 'BackboneJS',

            23 => 'CMS',
            24 => 'WordPress',
            25 => 'Magento',
            26 => 'Joomla',
            27 => 'Drupal',
            28 => 'OpenCart',
            29 => 'ZendCart',
            30 => 'CMSImage',
            31 => 'CS-Cart',
            32 => 'osCommerce',
            33 => 'ModX',
            34 => 'PrestaShop',
            35 => 'YupeCMS',
            36 => '1C-Bitrix',

            37 => 'ZendFramework',
            38 => 'Yii',
            39 => 'Kohana',
            40 => 'Symfony',
            41 => 'CodeIgniter',
            42 => 'Laravel',
            43 => 'Phalcon',

            44 => 'iOS',
            45 => 'Android',
            46 => 'PhoneGap',
            47 => 'Xamarin',
            48 => 'Cordova',

            49 => 'Photoshop',
            50 => 'Sketch',
            51 => 'Illustrator',
            52 => 'CorelDraw',

            53 => 'Unix',
            54 => 'Debian',
            55 => 'Ubuntu',
            56 => 'Apache',
            57 => 'Nginx',
            58 => 'Git',
            59 => 'SVN',




        ),

        'tags' => array(
            'Верстка' => '/Адаптивная, /Резиновая, /Кроссбраузерная, /Блочная, /PixelPerfect',
            'Дизайн' => '/Дизайн сайтов, /Интерфейсы, /Логотипы, /Прототипирование, /Фирменный стиль, /Дизайн приложения,  /Полиграфическйи дизайн, /Инфографика, /Иконки, /Баннеры',
            'Mobile приложение' => '/Android, /Java, /iOS, /iPad, /iPhone, /Objective-C, /Swift',
            'Сайт под ключ' => '/Визитка, /Корпоративный, /Блог, /Интернет-магазин, /Портал, /Лендинг, /Сервис',
            'С нуля' => '',
            'Самопис' => '',
            'Правка/Доработка' => '',
            'Framework' => '/Yii, /Laravel, /Symfony, /CodeIgniter, /Kohana, /Zend',
            'Базы данных' => '/MySQL, /MongoDB, /PostgreSQL,  /NoSQL, /Memcashed',
            'PHP' => '',
            'CMS' => '/Wordpress, /OpenCart, /Joomla, /Magento, /ModX, /1С-Битрикс, /Drupal, /PrestaShop,  /Yupe CMS, /CS-Cart, /ImageCMS, /osCommerce, /PHPShop, /Shop-Script, /phpBB, /vBulletin, /VirueMart, /Zend Cart',
            'CRM/ERP' => '',
            'Разработка CMS' => '',
            'HTML/CSS' => '',
            'JavaScript/JS' => '/jQuery, /AngularJS, /NodeJS, /BackboneJS, /AJAX',
            'LESS' => '',
            'Bootstrap' => '',
            'Соц. сети' => '/Facebok, /Vkontakte, /Google, /Twitter, /Instagram',
            'Тестирование' => '',
            'Настройка сервера' => '',
            'Составление ТЗ' => '',
            'Smarty' => '',
            'Вирусы' => '',
            'Парсеры' => '',
            'Модули' => '',
        ),


        // отключить отправку почты (используется программистом для ручных корректировок)
        'disable_send_mail' => false,

        'path'           => '../',
        'path_module'    => '../../',
        'folder_user_avatar' => 'uploads/users/avatar',
        'thumbnails' => array(
            'b_width'   => 215, // big
            'b_height'  => 70,
            'l_width'   => 205, // large
            'l_height'  => 139,
            'm_width'   => 100, // medium
            'm_height'  => 100
        ),
	),
);