<?php
$this->layout='//layouts/main';
?>

<h1><?php echo $model->username; ?></h1>

<?php

// For all users
$attributes = array(
    'username',
);

$profileFields = ProfileField::model()->forAll()->sort()->findAll();
if ($profileFields)
{
    foreach($profileFields as $field)
    {
        $allow_array = array('skype', 'phone', 'first_name', 'last_name');
        if (in_array($field->varname, $allow_array))
        {
            array_push($attributes,array(
                'label' => UserModule::t($field->title),
                'name' => $field->varname,
                'value' => (($field->widgetView($model->profile))?$field->widgetView($model->profile):(($field->range)?Profile::range($field->range,$model->profile->getAttribute($field->varname)):$model->profile->getAttribute($field->varname))),
            ));
        }
    }
}
array_push($attributes,
    'email'
);

$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => $attributes

));

?>
