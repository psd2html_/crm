<div class="register-box-body">

    <p class="login-box-msg h3">Зарегистрироваться как <?php echo Yii::app()->params['roles_name'][$_GET['type']] ?></p>


    <?php if(Yii::app()->user->hasFlash('registration')): ?>

        <div class="success">
            <?php echo Yii::app()->user->getFlash('registration'); ?>
        </div>

    <?php else: ?>

        <?php
            $form=$this->beginWidget('UActiveForm', array(
                'id'=>'registration-form',
                'enableAjaxValidation'=>true,
                'disableAjaxValidationAttributes'=>array('RegistrationForm_verifyCode'),
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                ),
                'htmlOptions' => array('enctype'=>'multipart/form-data'),
            ));
        ?>

        <!--	<p class="note">--><?php //echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?><!--</p>-->
        <p class="note">Все поля обязательны для заполнения</p>
	
	    <?php echo $form->errorSummary(array($model,$profile)); ?>

        <div class="form-group has-feedback">
	        <?php echo $form->textField($model,'username', array('class' => 'form-control', 'placeholder'=>'Введите логин...')); ?>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
	        <?php echo $form->error($model,'username'); ?>
	    </div>

        <div class="form-group has-feedback">
            <?php echo $form->textField($model,'email', array('class' => 'form-control', 'placeholder'=>'Введите Email...')); ?>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            <?php echo $form->error($model,'email'); ?>
        </div>

        <?php
            $profileFields=$profile->getFields();
            if ($profileFields)
            {
                foreach($profileFields as $field)
                {
                ?>
                    <?php if ( ($field->required == ProfileField::REQUIRED_YES_SHOW_REG)
                        OR ($field->required == ProfileField::REQUIRED_NO)
                        OR ($field->required == ProfileField::REQUIRED_NO_SHOW_REG)) : ?>
                            <div class="form-group has-feedback">


                                <?php //echo $form->labelEx($profile,$field->varname); ?>
                                <?php
                                    if ($widgetEdit = $field->widgetEdit($profile))
                                    {
                                        echo $widgetEdit;
                                    }
                                    elseif ($field->range)
                                    {
                                        echo $form->dropDownList($profile,$field->varname,Profile::range($field->range));
                                    }
                                    elseif ($field->field_type=="TEXT")
                                    {
                                        echo $form->textArea($profile,$field->varname,array('class' => 'form-control', 'placeholder'=>$field->varname, 'rows'=>6, 'cols'=>50));
                                    } else
                                    {
                                        echo $form->textField($profile,$field->varname, array('class' => 'form-control', 'placeholder'=>$field->varname  ,'size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255)));
                                    }
                                ?>
                                <?php echo $form->error($profile,$field->varname); ?>
                            </div>
                    <?php endif; ?>
                <?php
                }
            }
        ?>

        <div class="form-group has-feedback">
            <?php echo $form->passwordField($model,'password', array('class' => 'form-control', 'placeholder'=>'Введите пароль...')); ?>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            <?php echo $form->error($model,'password'); ?>

        </div>

        <div class="form-group has-feedback">
            <?php echo $form->passwordField($model,'verifyPassword', array('class' => 'form-control', 'placeholder'=>'Подтверждение пароля...')); ?>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            <?php echo $form->error($model,'verifyPassword'); ?>
        </div>


        <?php if ($_GET['type'] == 'freelancer') : ?>
            <div class="col-md-12">
                <div class="row checkbox">
                    <label>
                        <input class="ch" type="checkbox"  id="confirm" >
                        &nbsp;Я согласен с <a href="/uploads/docs/reglament.pdf" id="reg">регламентом работы</a>
                    </label>
                </div>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-md-8">
                <?php if ($_GET['type'] == 'freelancer') : ?>
                    <?php echo CHtml::submitButton(UserModule::t("Register"), array('class' => 'btn btn-success btn-block btn-large', 'disabled' => 'disabled', 'id' => 'register')); ?>
                <?php else : ?>
                        <?php echo CHtml::submitButton(UserModule::t("Register"), array('class' => 'btn btn-success btn-block btn-large')); ?>
                <?php endif; ?>
            </div>
            <div class="col-md-4">
                <a href="/user/login" class="text-center">Вход</a>
            </div>
            <!-- /.col -->
        </div>

        <?php $this->endWidget(); ?>

    <?php endif; ?>

</div>