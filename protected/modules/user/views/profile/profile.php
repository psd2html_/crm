<?php

//$this->menu=array(
//    array('label'=> '<i class="fa fa-bars"></i><span>Профиль</span>', 'url'=>array('/user/profile')),
//    array('label'=> '<i class="fa fa-bars"></i><span>Редактировать профиль</span>', 'url'=>array('/user/profile/edit')),
//    array('label'=> '<i class="fa fa-bars"></i><span>Изменить пароль</span>', 'url'=>array('changepassword')),
//);
?>
<h1><?php echo UserModule::t('Your profile'); ?></h1>

<?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
<div class="success">
	<?php echo Yii::app()->user->getFlash('profileMessage'); ?>
</div>
<?php endif; ?>

<div class="col-sm-6">
    <div class="box">

        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <td><?php echo CHtml::encode($model->getAttributeLabel('username')); ?></td>
                    <td><?php echo CHtml::encode($model->username); ?></td>
                </tr>
                <?php $profileFields = ProfileField::model()->forOwner()->sort()->findAll(); ?>
                <?php if ($profileFields) : ?>


                    <?php
                        // какие не выводить
                        $dis = array(
                            'avatar',
                            'service',
                            'additions_data',
                            'skills',
                        );
                    ?>

                    <?php foreach($profileFields as $field) : ?>

                        <?php if (!in_array($field->varname, $dis)) : ?>


                        <tr>
                            <td><?php echo CHtml::encode(UserModule::t($field->title)); ?></td>
                            <td><?php echo (($field->widgetView($profile))?$field->widgetView($profile):CHtml::encode((($field->range)?Profile::range($field->range,$profile->getAttribute($field->varname)):$profile->getAttribute($field->varname)))); ?></td>
                        </tr>

                        <?php endif; ?>

                    <?php endforeach; ?>

                    <?php
                    //$profile->getAttribute($field->varname)
                    ?>

                <?php endif; ?>

                <tr>
                    <td><?php echo CHtml::encode($model->getAttributeLabel('email')); ?></td>
                    <td><?php echo CHtml::encode($model->email); ?></td>
                </tr>
                <tr>
                    <td><?php echo CHtml::encode($model->getAttributeLabel('create_at')); ?></td>
                    <td><?php echo $model->create_at; ?></td>
                </tr>

                <tr>
                    <td><?php echo CHtml::encode($model->getAttributeLabel('status')); ?></td>
                    <td><?php echo CHtml::encode(User::itemAlias("UserStatus",$model->status)); ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>





