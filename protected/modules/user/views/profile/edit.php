<?php


//$this->menu=array(
//    array('label'=> '<i class="fa fa-bars"></i><span>Профиль</span>', 'url'=>array('/user/profile')),
//    array('label'=> '<i class="fa fa-bars"></i><span>Редактировать профиль</span>', 'url'=>array('/user/profile/edit')),
//    array('label'=> '<i class="fa fa-bars"></i><span>Изменить пароль</span>', 'url'=>array('changepassword')),
//);
?>

<h1><?php echo UserModule::t('Edit profile'); ?></h1>

<?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
<div class="success">
<?php echo Yii::app()->user->getFlash('profileMessage'); ?>
</div>
<?php endif; ?>


<div class="row">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'profile-form',
	'enableAjaxValidation'=> false ,
	'htmlOptions' => array('enctype'=>'multipart/form-data'),
)); ?>



	<?php echo $form->errorSummary(array($model,$profile)); ?>



    <?php $profileFields = $profile->getFields(); ?>


    <div class="col-md-3">
        <?php if ($profileFields) : ?>



            <?php foreach($profileFields as $field) : ?>

                <?php if ($field->varname == 'avatar') : ?>
                    <div class="avatar">
                        <?php
                            // выводим аватар,
                            // а если он не установлен,
                            // то будет использовано изображение по умолчанию
                            echo '<div class="avatar-image"><img width="190" src="' . Yii::app()->params['path_module'] . $profile->attachment.'" /><br>';
                            // поле для загрузки изображения
                            //echo '<div class="fileUpload btn btn-success">';
                            echo '<br />';
                            echo '<div class="new-fileUpload">';
                            //echo '<span>Загрузить</span>';
                                echo $form->fileField($profile, 'avatar', array('class' => 'upload')) . '<br>';
                            echo '</div>';

                            // ссылка для удаления изображения
                            //echo CHtml::link('Удалить аватар', array('deleteimage'), array('class' => 'btn btn-danger no-margin'));
                            echo '</div>';
                        ?>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>

    <div class="col-md-7">



        <div class="form-horizontal">

            <p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>

            <div class="box-body">


                <div class="row">
                    <div class="form-group">
                        <?php echo $form->labelEx($model,'username', array('class'=>'col-sm-4 control-label')); ?>
                        <div class="col-sm-8">
                            <?php echo $form->textField($model,'username',array('size'=>20,'maxlength'=>20, 'class'=>'form-control', 'disabled'=>'disabled')); ?>
                        </div>
                        <?php echo $form->error($model,'username'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($model,'email', array('class'=>'col-sm-4 control-label')); ?>
                        <div class="col-sm-8">
                            <?php echo $form->textField($model,'email', array('size'=>60,'maxlength'=>128, 'class'=>'form-control')); ?>
                        </div>
                        <?php echo $form->error($model,'email'); ?>
                    </div>
                </div>



                <?php if ($profileFields) : ?>
                <?php foreach($profileFields as $field) : ?>
                        <?php
                        $parse_json = json_decode($field->widgetparams);
                        $user_type = (isset($parse_json->user_type) and !empty($parse_json->user_type)) ? ($parse_json->user_type) : '';
                        $block_type = (isset($parse_json->block_type) and !empty($parse_json->block_type)) ? ($parse_json->block_type) : '';
                        ?>
                        <?php if ($block_type == 'info') : ?>
                            <?php
                                echo '<div class="form-group col-sm-12">';
                                if (($field->varname != 'avatar') and ($field->varname != 'about'))
                                    echo $form->labelEx($profile,$field->varname, array('class'=>'col-sm-4 control-label'));
                                else
                                    echo $form->labelEx($profile,$field->varname);

                                if ($widgetEdit = $field->widgetEdit($profile))
                                {
                                    echo $widgetEdit;
                                }
                                elseif ($field->range)
                                {
                                    echo '<div class="col-sm-8">';
                                    echo $form->dropDownList($profile,$field->varname,Profile::range($field->range));
                                    echo '</div>';
                                }
                                elseif ($field->field_type=="TEXT")
                                {
                                    //echo '<div class="col-sm-8">';
                                    echo $form->textArea($profile,$field->varname,array('rows'=>6, 'cols'=>50, 'class'=>'form-control'));
                                    //echo '</div>';
                                }
                                else
                                {
                                    if ($field->varname != 'avatar')
                                    {
                                        echo '<div class="col-sm-8">';
                                        echo $form->textField($profile,$field->varname,array('size'=>60, 'class'=>'form-control', 'maxlength'=>(($field->field_size)?$field->field_size:255)));
                                        echo '</div>';
                                    }

                                }
                                echo '</div>';

                                echo $form->error($profile,$field->varname);
                            ?>
                        <?php endif; ?>
                <?php endforeach; ?>
                <?php endif; ?>







                <!-- Контактная инфа-->
                <div class="row">
                    <p class="bold col-sm-12">Контактная информация</p>
                    <?php if ($profileFields) : ?>
                        <?php foreach($profileFields as $field) : ?>
                            <?php
                                $parse_json = json_decode($field->widgetparams);
                                $user_type = (isset($parse_json->user_type) and !empty($parse_json->user_type)) ? ($parse_json->user_type) : '';
                                $block_type = (isset($parse_json->block_type) and !empty($parse_json->block_type)) ? ($parse_json->block_type) : '';
                            ?>

                                <?php if ($block_type == 'contact') : ?>

                                    <?php if ( ((C_Rights::isFreelancer()) and ($user_type == 'freelancer')) OR (!$user_type)) : ?>

                                            <?php
                                                echo '<div class="form-group col-sm-12">';
                                                if (($field->varname != 'avatar') and ($field->varname != 'about'))
                                                    echo $form->labelEx($profile,$field->varname, array('class'=>'col-sm-4 control-label'));
                                                else
                                                    echo $form->labelEx($profile,$field->varname);

                                                if ($widgetEdit = $field->widgetEdit($profile))
                                                {
                                                    echo $widgetEdit;
                                                }
                                                elseif ($field->range)
                                                {
                                                    echo '<div class="col-sm-8">';
                                                    echo $form->dropDownList($profile,$field->varname,Profile::range($field->range));
                                                    echo '</div>';
                                                }
                                                elseif ($field->field_type=="TEXT")
                                                {
                                                    //echo '<div class="col-sm-8">';
                                                    echo $form->textArea($profile,$field->varname,array('rows'=>6, 'cols'=>50, 'class'=>'form-control'));
                                                    //echo '</div>';
                                                }
                                                else
                                                {
                                                    if ($field->varname != 'avatar')
                                                    {
                                                        echo '<div class="col-sm-8">';
                                                        echo $form->textField($profile,$field->varname,array('size'=>60, 'class'=>'form-control', 'maxlength'=>(($field->field_size)?$field->field_size:255)));
                                                        echo '</div>';
                                                    }

                                                }
                                                echo '</div>';

                                                echo $form->error($profile,$field->varname);
                                            ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>

                <!-- Стоимость работы -->
                <div class="row">
                    <div class="form-group cost-cover">
                        <?php if ($profileFields) : ?>
                            <?php foreach($profileFields as $field) : ?>
                                <?php
                                $parse_json = json_decode($field->widgetparams);
                                $user_type = (isset($parse_json->user_type) and !empty($parse_json->user_type)) ? ($parse_json->user_type) : '';
                                $block_type = (isset($parse_json->block_type) and !empty($parse_json->block_type)) ? ($parse_json->block_type) : '';
                                ?>
                                <?php if ($block_type == 'rate') : ?>
                                    <?php if ( ((C_Rights::isFreelancer()) and ($user_type == 'freelancer')) OR (!$user_type)) : ?>

                                        <?php
                                        //echo '<div class="form-group">';

                                        if ($field->varname != 'avatar')
                                        {
                                            echo '<strong class="block cost col-sm-12">';
                                            echo $form->labelEx($profile,$field->varname);
                                            echo '</strong>';
                                        }

                                        if ($widgetEdit = $field->widgetEdit($profile))
                                        {
                                            echo $widgetEdit;
                                        }
                                        elseif ($field->range)
                                        {
                                            echo '<div class="col-sm-5 col-xs-5">';
                                            echo $form->dropDownList($profile,$field->varname, Profile::range($field->range), array('class'=>'form-control'));
                                            echo '</div>';
                                        }
                                        elseif ($field->field_type=="TEXT")
                                        {
                                            echo '<div class="col-sm-5 col-xs-5">';
                                            echo $form->textArea($profile,$field->varname,array('rows'=>6, 'cols'=>50, 'class'=>'form-control'));
                                            echo '</div>';
                                        }
                                        else
                                        {
                                            if ($field->varname != 'avatar')
                                            {
                                                echo '<div class="col-sm-5 col-xs-5">';
                                                echo $form->textField($profile,$field->varname,array('size'=>60, 'class'=>'form-control', 'maxlength'=>(($field->field_size)?$field->field_size:255)));
                                                echo '</div>';
                                            }

                                        }
                                        //echo '</div>';

                                        echo $form->error($profile,$field->varname);
                                        ?>

                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>


                        <!--                    <span class="col-sm-2 col-xs-2">за</span>-->
                        <!--                    <div class="col-sm-5 col-xs-5">-->
                        <!--                        <select class="form-control">-->
                        <!--                            <option selected="" value="за час">за час</option>-->
                        <!--                            <option value="за день">за день</option>-->
                        <!--                            <option value="за месяц">за месяц</option>-->
                        <!--                        </select>-->
                        <!--                    </div>-->
                    </div>
                </div>

                <!-- Профессиональные данные -->
                <div class="row">


                    <?php if ($profileFields) : ?>
                        <?php foreach($profileFields as $field) : ?>
                            <?php
                            $parse_json = json_decode($field->widgetparams);
                            $user_type = (isset($parse_json->user_type) and !empty($parse_json->user_type)) ? ($parse_json->user_type) : '';
                            $block_type = (isset($parse_json->block_type) and !empty($parse_json->block_type)) ? ($parse_json->block_type) : '';



                            ?>
                            <?php if ($block_type == 'prof_data') : ?>
                                <?php if ( ((C_Rights::isFreelancer()) and ($user_type == 'freelancer')) OR (!$user_type)) : ?>
                                    <p class="bold col-sm-12">Профессиональные данные</p>
                                    <?php
                                        if ($field->varname != 'avatar')
                                        {
                                            echo '<strong class="block cost col-sm-12">';
                                            echo $form->labelEx($profile,$field->varname);
                                            echo '</strong>';
                                        }

                                        if ($widgetEdit = $field->widgetEdit($profile))
                                        {
                                            echo $widgetEdit;
                                        }
                                        elseif ($field->range)
                                        {
                                            echo '<div class="form-group col-md-4">';
                                            echo $form->dropDownList($profile,$field->varname,Profile::range($field->range), array('class'=>'form-control'));
                                            echo '</div>';
                                        }
                                        elseif ($field->field_type=="TEXT")
                                        {
                                                echo '<div class="col-md-12">';
                                                //echo $form->textArea($profile,$field->varname,array('rows'=>6, 'cols'=>50, 'class'=>'form-control'));
                                                $_skills = Yii::app()->params['skills'];

                                                echo '<div class="skills">';
                                                echo '<div class="form-group">';
                                                echo '<select name="Profile[skills][]" class="form-control select2 select2-container-green select2-hidden-accessible" multiple="1,2" style="width: 100%;" tabindex="-1" aria-hidden="true">';
                                                foreach ($_skills as $_s_value)
                                                {
                                                    echo '<option value="'.$_s_value.'">'.$_s_value.'</option>';
                                                }
                                                echo '</select>';
                                                echo '</div>';
                                                echo '</div>';
                                                echo '<input type="hidden" id="e10" style="width:300px"/>';

                                                echo '</div>';
                                        }
                                        else
                                        {
                                            if ($field->varname != 'avatar')
                                            {
                                                echo '<div class="form-group col-sm-6">';
                                                echo $form->textField($profile, $field->varname, array('size'=>60, 'class'=>'form-control', 'maxlength'=>(($field->field_size)?$field->field_size:255)));
                                                echo '</div>';
                                            }

                                        }

                                        echo $form->error($profile,$field->varname);
                                    ?>
                                    <input type="hidden" class="preloaded-skills" value='<?php echo $profile->skills; ?>'>


                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>


                <div class="row">
                    <div class="field-of-activity">


                        <?php if ($profileFields) : ?>
                            <?php foreach($profileFields as $field) : ?>
                                <?php
                                    $parse_json = json_decode($field->widgetparams);
                                    $user_type = (isset($parse_json->user_type) and !empty($parse_json->user_type)) ? ($parse_json->user_type) : '';
                                    $block_type = (isset($parse_json->block_type) and !empty($parse_json->block_type)) ? ($parse_json->block_type) : '';
                                    $data_service = ($profile['service']) ? ($profile['service']) : '';
                                    $_services = (array)json_decode($data_service);
                                ?>
                                <?php if ($block_type == 'services') : ?>

                                    <?php if ( ((C_Rights::isFreelancer()) and ($user_type == 'freelancer')) OR (!$user_type)) : ?>
                                        <p class="bold col-sm-12">Сфера деятельности</p>
                                        <div style="min-height: 450px;">
                                            <!-- Навигация -->
                                            <ul class="nav col-sm-4 col-xs-4" role="tablist">
                                                <?php $list_top = Yii::app()->params['services']; ?>
                                                <?php $x=0; foreach ($list_top as $key => $value) : $x++; ?>
                                                    <li <?php if ($x==1) { echo 'class="active"'; } ?>><a href="#tab<?php echo $x; ?>" role="tab" data-toggle="tab"><?php echo $key; ?></a></li>
                                                <?php endforeach; ?>
                                            </ul>
                                            <!-- Содержимое вкладок -->
                                            <div class="tab-content col-sm-8 col-xs-8">

                                                <?php $list_top = Yii::app()->params['services']; ?>
                                                <?php $x=0; foreach ($list_top as $key => $value) : $x++; ?>

                                                    <div role="tabpanel" class="tab-pane <?php if ($x==1) { echo 'active'; } ?>" id="tab<?php echo $x; ?>">
                                                        <div class="form-group">

                                                            <?php $y=0; foreach ($value as $key => $_value_name) : $y++; ?>
                                                                    <?php
                                                                        $checked = '';
                                                                        if (isset($_services) and !empty($_services))
                                                                        {
                                                                            if (in_array($key, $_services)) {
                                                                                $checked = 'checked';
                                                                            } else $checked = '';
                                                                        }
                                                                    ?>
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input type="checkbox" class="flat-red" name="Profile[service][]" value="<?php echo $key; ?>" <?php echo $checked; ?>>
                                                                            <?php echo $_value_name; ?>
                                                                        </label>
                                                                    </div>
                                                            <?php endforeach; ?>
                                                        </div>
                                                    </div>

                                                <?php endforeach; ?>
                                            </div>
                                        </div>

                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>


                    </div>
                </div>


<!--                <div class="skills row">-->
<!--                    <div class="form-group col-sm-12">-->
<!--                        <label>Профессиональные навыки (15 штук)*</label>-->
<!--                        <div class="form-group">-->
<!--                            <label>Multiple</label>-->
<!--                            <select class="form-control select2" multiple="multiple" data-placeholder="Select a State" style="width: 100%;">-->
<!--                                <option>Alabama</option>-->
<!--                                <option>Alaska</option>-->
<!--                                <option>California</option>-->
<!--                                <option>Delaware</option>-->
<!--                                <option>Tennessee</option>-->
<!--                                <option>Texas</option>-->
<!--                                <option>Washington</option>-->
<!--                            </select>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->


            </div>

            <div class="box-footer no-gutter">
                <div class="col-sm-4 col-md-4">
                    <?php echo CHtml::submitButton($model->isNewRecord ? UserModule::t('Create') : UserModule::t('Save'), array('class'=>'btn btn-block btn-success btn-lg')); ?>
                </div>
            </div>

        </div>
    </div>











<?php $this->endWidget(); ?>

</div><!-- form -->
