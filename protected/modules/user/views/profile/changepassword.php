<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Change Password");



//$this->menu=array(
//    array('label'=> '<i class="fa fa-bars"></i><span>Профиль</span>', 'url'=>array('/user/profile')),
//    array('label'=> '<i class="fa fa-bars"></i><span>Редактировать профиль</span>', 'url'=>array('edit')),
//    array('label'=> '<i class="fa fa-bars"></i><span>Изменить пароль</span>', 'url'=>array('changepassword')),
//);
?>

<h1><?php echo UserModule::t("Change password"); ?></h1>

<div class="row">


    <div class="col-md-6">

        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'changepassword-form',
            'enableAjaxValidation'=>true,
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
            ),
        )); ?>

        <p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>
        <?php echo $form->errorSummary($model); ?>

        <div class="row2">
            <div class="form-group">
                <?php echo $form->labelEx($model,'oldPassword'); ?>
                <?php echo $form->passwordField($model,'oldPassword', array('class'=>'form-control')); ?>
                <?php echo $form->error($model,'oldPassword'); ?>
            </div>

        </div>

        <div class="row2">
            <?php echo $form->labelEx($model,'password'); ?>
            <?php echo $form->passwordField($model,'password', array('class'=>'form-control')); ?>
            <?php echo $form->error($model,'password'); ?>
            <p class="hint">
                <?php echo UserModule::t("Minimal password length 4 symbols."); ?>
            </p>
        </div>

        <div class="row2">
            <?php echo $form->labelEx($model,'verifyPassword'); ?>
            <?php echo $form->passwordField($model,'verifyPassword', array('class'=>'form-control')); ?>
            <?php echo $form->error($model,'verifyPassword'); ?>
        </div>


        <div class="row box-footer no-gutter">
            <div class="col-md-4">
                <?php echo CHtml::submitButton(UserModule::t("Save"), array('class'=>'btn btn-block btn-success btn-lg')); ?>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div>



</div><!-- form -->