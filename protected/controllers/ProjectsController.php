<?php

class ProjectsController extends RController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
        return array(
            'rights',
        );
	}


    /**
     * Закрыть проект/сделку
     *
     * @param $project_id
     * @param $project_proposal_id
     * @param $manager_id
     */
    public function actionClose($project_id, $project_proposal_id, $manager_id)
    {
        // обновить статус проекта
        Projects::model()->updateAll(
            array('status' => Projects::STATUS_CLOSED),
            'id = :pID',
            array(':pID' => $project_id)
        );

        // обновить статус сделки
        ProjectsAssignUser::model()->updateAll(
            array('status' => ProjectsAssignUser::STATUS_CLOSE),
            'project_proposal_id = :ppID AND project_id = :pID',
            array(':ppID' => $project_proposal_id, ':pID' => $project_id)
        );



        // отправляем менеджеру уведомление
        C_Mail::sendMailUserByID('simple', 'Проект закрыт', $manager_id, 'Юзер сдал проект');

        $this->render('invoice');
    }


    /**
     * Обвновить статус проекта
     */
    public function actionUnpublished($id)
    {
        // обновить статус проекта
        Projects::model()->updateAll(
            array('status' => Projects::STATUS_UNPUBLISHED),
            'id = :pID',
            array(':pID' => $id)
        );

        Yii::app()->user->setFlash('success', Yii::t('app', 'Проект снят с публикации'));
        $this->redirect(array('view', 'id' => $id));

        // отправляем менеджеру уведомление
        //C_Mail::sendMailUserByID('simple', 'Проект закрыт', $manager_id, 'Юзер сдал проект');


    }


    /**
     * Обвновить статус проекта
     *
     * @param $id
     */
    public function actionArchived($id)
    {
        // обновить статус проекта
        Projects::model()->updateAll(
            array('status' => Projects::STATUS_ARCHIVED),
            'id = :pID',
            array(':pID' => $id)
        );

        Yii::app()->user->setFlash('success', Yii::t('app', 'Проект в корзине'));
        $this->redirect(array('view', 'id' => $id));
    }


    /**
     * ВСе открытые проекты менеджера где уже есть заявки
     */
    public function actionProposal()
    {
        // paginations
        $criteria = new CDbCriteria();
        $criteria->condition = 't.status = :status AND t.user_id = :userID';
        $criteria->join = ' INNER JOIN tbl_projects_proposal as LinkProposal ON `LinkProposal`.`project_id`=`t`.`id`';
        $criteria->distinct = true;
        $criteria->params = array(
            ':userID' => Yii::app()->user->id,
            ':status' => Projects::STATUS_OPEN
        );
        $criteria->together = true;
        //$criteria->with = array('LinkProposal');
        $criteria->order = 't.create_at DESC';

        $count = Projects::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);
        $model = Projects::model()->findAll($criteria);

        // todo: нужно или объединить этот запрос с верхним или переделать, потому как во вьюхе идет сильный форыч
        // вытащить все предложения с комментами
        $ProposalComments = ProjectsProposal::model()->with('ppComments', 'ppCountComments')->findAll();


//        echo '<pre>';
//        print_r($ProposalComments);
//        echo '</pre>';
//        die('stop');

        $this->render('index_manager', array(
            'model' => $model,
            'ProposalComments' => $ProposalComments,
            'pages' => $pages
        ));
    }


	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        C_User::getManagerEmailByProjectID($id);
        if (isset($_POST['ProjectsProposal']))
        {
            $model = new ProjectsProposal;
            $model->attributes = $_POST['ProjectsProposal'];
            $model->project_id = $id;

            if ($model->save())
            {
                // уведомление менеджеру
                $data = array(
                    'title' => 'Новая заявка',
                    'data' => 'В вашем проекте есть новая заявка'
                );
                C_Mail::sendSimpleMail('Новая заявка', C_User::getManagerEmailByProjectID($id), $data);

                Yii::app()->user->setFlash('success', 'Ваша заявка отправлена. Увидеть ее можете внизу данной страницы.');
                $this->redirect(array('view','id'=>$id));
            }
        }

        $files = '';

        // находим все заявки со всеми комментариями
        $modelProjectProposal = ProjectsProposal::model()
            ->with('ppComments')
            ->findAll('t.project_id = :pID', array(':pID' => $id));

        if (C_Rights::checkRoleCurrentUser('manager')) {
            // помечаем все заявки как прочитанные
            ProjectsProposal::model()->updateAll(
                array('status' => ProjectsProposal::STATUS_READ),
                'project_id = :pID',
                array(':pID' => $id)
            );
        }

        // показ файлов по проекту
        $path = C_Base::getBasePath() . Yii::app()->params['uploads_projects'] .  $id;
        if (is_dir($path))
            $files = CFileHelper::findFiles(realpath($path), array('absolutePaths' => false));

        $__model = Projects::model()->with('pAssignUser')->findByPk($id);

		$this->render('view',array(
			//'model' => $this->loadModel($id),
			'model' => $__model,
            'modelProposal' => ProjectsProposal::model(), // форма
            'modelProjectProposal' => $modelProjectProposal,
            'files' => $files
		));
	}


    /**
     * Получить файл на загрузку
     *
     * @param $project_id
     */
    public function actionDownload($project_id)
    {
        $name = $_GET['file'];
        $upload_path = C_Base::getBasePath() . Yii::app()->params['uploads_projects'] . $project_id . DIRECTORY_SEPARATOR;

        if (file_exists($upload_path . $name))
            Yii::app()->getRequest()->sendFile( $name , file_get_contents( $upload_path.$name ) );
    }


    /**
     * Выбрать исполнителя
     *
     * @param $id
     */
    public function actionAssign($id, $project_id, $project_proposal_id)
    {
        $dataProvider = new CActiveDataProvider('Projects');

        $model = new ProjectsAssignUser;
        $model->user_id = $id;
        $model->project_id = $project_id;
        $model->manager_id = Yii::app()->user->id;
        $model->project_proposal_id = $project_proposal_id;

        if ($model->save(false))
        {
            // меняем статус проекта
            C_Projects::ChangeStatusProject($project_id, Projects::STATUS_ASSIGN);

            // Добавляем сообщение о том что пользователя выбрали
            $info_comments = "
                ---- ВАС ВЫБРАЛИ ИСПОЛНИТЕЛЕМ ---
            ";
            $projectProposalComments = new ProjectsProposalComments();
            $projectProposalComments->project_proposal_id = $project_proposal_id;
            $projectProposalComments->user_id = Yii::app()->user->id;
            $projectProposalComments->project_id = $project_id;
            $projectProposalComments->comments = $info_comments;
            $projectProposalComments->save(false);

            $this->redirect('/projects/active');
        }
    }


    public function actionMyProjects()
    {
        $model = Projects::model()->with('pAssignUser')->findAll(
            'pAssignUser.user_id = :uID AND t.status = :status',
            array(
                ':uID' => Yii::app()->user->id,
                ':status' => Projects::STATUS_ASSIGN
            )
        );

//        $criteria = new CDbCriteria();
//        $criteria->condition = 'pAssignUser.user_id = :uID';
//        $criteria->order = 'create_at DESC';
//        $criteria->params = array(':uID' => Yii::app()->user->id);
//        $count = Projects::model()->count($criteria);
//        $pages = new CPagination($count);
//        $pages->pageSize = 10;
//        $pages->applyLimit($criteria);
//        $model = Projects::model()->with('pAssignUser')->findAll($criteria);

        $this->render('myprojects',array(
            'model' => $model,
        ));
    }


	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Projects;
		if (isset($_POST['Projects']))
		{
			$model->attributes = $_POST['Projects'];
            $model->tags = ($_POST['tags']) ? ($_POST['tags']) : '';
            if ($model->save())
            {
                C_Upload::uploadFiles($model);

                // шлем сообщение всем фрилансерам
                $data = array(
                    'title' => 'Добавлен новый проект',
                    'data' =>  'Добавлен проект "' . CHtml::link($_POST['Projects']['title'], C_Base::getBaseURL() . '/projects/view?id='. $model->id) . '"'
                );
                C_Mail::sendAllFreelancers('Добавлен новый проект', $data);

                $this->redirect(array('view', 'id' => $model->id));
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Projects']))
		{
			$model->attributes=$_POST['Projects'];
			if($model->save(false))
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}




	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	/**
	 * Все открытые проекты для фрилансера
	 */
	public function actionIndex()
	{
        // todo: борода с запросом: нужно вывести все проекты где нет пользователя в таблице предложение но есть там другие предложение других юзеров
        $criteria = new CDbCriteria();
        //$criteria->condition = 'LinkProposal.user_id IS NULL AND `t`.`status` = :status';
        //$criteria->condition = 'LinkProposal.user_id IS NULL AND `t`.`status` = :status';
        $criteria->condition = '`t`.`status` = :status';

//        $criteria->addCondition("
//                t.`id` NOT IN(
//                 SELECT `user_id`
//                 FROM `tbl_projects_proposal`)
//                 ");

        $criteria->params = array(
            ':status' => Projects::STATUS_OPEN,
            //':uID' => Yii::app()->user->id,
        );
        $criteria->order = '`t`.`create_at` DESC';
        $criteria->together = true;
        $criteria->with = array('LinkProposal');


        $count = Projects::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);

        $model = Projects::model()->findAll($criteria);

        $this->render('index',array(
            'model' => $model,
            'pages' => $pages
        ));
	}


    /**
     * Все свои открытые проекты менеджера
     */
    public function actionIndexManager()
    {
        // paginations
        $criteria = new CDbCriteria();
        $criteria->condition = 't.user_id = :userID AND status = :status';
        $criteria->params = array(
            ':userID' => Yii::app()->user->id,
            ':status' => Projects::STATUS_OPEN
        );
        $criteria->order = 'create_at DESC';

        $count = Projects::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);
        $model = Projects::model()->findAll($criteria);


        $this->render('index_manager', array(
            'model' => $model,
            'pages' => $pages
        ));
    }


    /**
     * Все преокты которые в работе, с выбранным исполнителем
     */
    public function actionActive()
    {
        // paginations
        $criteria = new CDbCriteria();
        $criteria->condition = 't.user_id = :userID AND status = :status';
        $criteria->params = array(
            ':userID' => Yii::app()->user->id,
            ':status' => Projects::STATUS_ASSIGN
        );
        $criteria->order = 'create_at DESC';

        $count = Projects::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);
        $model = Projects::model()->findAll($criteria);


        $this->render('index_manager', array(
            'model' => $model,
            'pages' => $pages
        ));
    }


    /**
     * неопубликованные/скрытые проекты менеджера
     */
    public function actionHidden()
    {
        // paginations
        $criteria = new CDbCriteria();
        $criteria->condition = 'user_id = :userID AND status = :status';
        $criteria->params = array(
            ':userID' => Yii::app()->user->id,
            ':status' => Projects::STATUS_UNPUBLISHED
        );
        $criteria->order = 'create_at DESC';

        $count = Projects::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);
        $model = Projects::model()->findAll($criteria);

        $this->render('index_manager', array(
            'model' => $model,
            'pages' => $pages
        ));

        return;
    }


    /**
     * выполненные/закрытые проекты
     */
    public function actionCompleted()
    {
        if (C_Rights::isFreelancer())
        {
            // paginations
            $criteria = new CDbCriteria();
            $criteria->condition = 't.status = :status AND pAssignUser.user_id = :userID';
            $criteria->with = array('pAssignUser');
            $criteria->params = array(
                ':userID' => Yii::app()->user->id,
                ':status' => Projects::STATUS_CLOSED
            );
            $criteria->order = 't.create_at DESC';

            $count = Projects::model()->count($criteria);
            $pages = new CPagination($count);
            $pages->pageSize = 10;
            $pages->applyLimit($criteria);
            $model = Projects::model()->findAll($criteria);

            $this->render('index', array(
                'model' => $model,
                'pages' => $pages
            ));
        }
        else
        {
            // paginations
            $criteria = new CDbCriteria();
            $criteria->condition = 'user_id = :userID AND status = :status';
            $criteria->params = array(
                ':userID' => Yii::app()->user->id,
                ':status' => Projects::STATUS_CLOSED
            );
            $criteria->order = 'create_at DESC';

            $count = Projects::model()->count($criteria);
            $pages = new CPagination($count);
            $pages->pageSize = 10;
            $pages->applyLimit($criteria);
            $model = Projects::model()->findAll($criteria);

            $this->render('index_manager', array(
                'model' => $model,
                'pages' => $pages
            ));
        }



        return;
    }


    /**
     * архивные/удаленные проекты менеджера
     */
    public function actionDeleted()
    {
        // paginations
        $criteria = new CDbCriteria();
        $criteria->condition = 'user_id = :userID AND status = :status';
        $criteria->params = array(
            ':userID' => Yii::app()->user->id,
            ':status' => Projects::STATUS_ARCHIVED
        );
        $criteria->order = 'create_at DESC';

        $count = Projects::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);
        $model = Projects::model()->findAll($criteria);

        $this->render('index_manager', array(
            'model' => $model,
            'pages' => $pages
        ));

        return;
    }


    /**
     * Все проекты исполнителя где есть его заявки
     */
    public function actionOffers()
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'LinkProposal.user_id = :userID AND pAssignUser.project_id IS NULL  AND `t`.`status` = :status';
        $criteria->params = array(
            ':userID' => Yii::app()->user->id,
            ':status' => Projects::STATUS_OPEN,
            );
        $criteria->order = '`t`.`create_at` DESC';
        $criteria->together = true;
        $criteria->with = array('LinkProposal', 'pAssignUser');


        $count = Projects::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);

        // так не работает
        //$model = Projects::model()->with('LinkProposal')->recently()->published()->findAll($criteria);
        $model = Projects::model()->findAll($criteria);


        // todo: нужно или объединить этот запрос с верхним или переделать, потому как во вьюхе идет сильный форыч
        // вытащить все предложения с комментами
        $Comments = ProjectsProposal::model()->with('ppComments')->findAll();

        $this->render('offers',array(
            'model' => $model,
            'Comments' => $Comments,
            'pages' => $pages
        ));
    }


	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Projects('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Projects']))
			$model->attributes=$_GET['Projects'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Projects the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Projects::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Projects $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='projects-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
