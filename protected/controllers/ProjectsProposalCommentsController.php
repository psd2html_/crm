<?php

class ProjectsProposalCommentsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new ProjectsProposalComments;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ProjectsProposalComments']))
		{
			$model->attributes=$_POST['ProjectsProposalComments'];
			if ($model->save())
            {
                $this->redirect(array('view', 'id' => $model->id));
            }

		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ProjectsProposalComments']))
		{
			$model->attributes=$_POST['ProjectsProposalComments'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($project_proposal_id)
	{
        $modelProjectProposal = ProjectsProposal::model()->with('lProjects')->findByPk($project_proposal_id);

        if (isset($_POST['ProjectsProposalComments']))
        {
            $model_new = new ProjectsProposalComments;
            $model_new->attributes = $_POST['ProjectsProposalComments'];
            $model_new->project_proposal_id = $project_proposal_id;
            $model_new->project_id = $modelProjectProposal->project_id;

            if ($model_new->save(false))
            {

                if (C_Rights::isFreelancer())
                {
                    // отправляем сообщение менеджеру
                    $data=  array(
                        'title'=>Yii::app()->name . ' - новое сообщение',
                        'data'=>'пользователь '.C_User::getUsernameByID($modelProjectProposal->user_id).' оставил новое сообщение в проекте.'
                    );
                    C_Mail::sendMailUserByID('simple',
                        Yii::app()->name . ' - новое сообщение',
                        $modelProjectProposal->lProjects->user_id,
                        $data
                    );
                }

                if (C_Rights::checkRoleCurrentUser('manager'))
                {
                    // отсправляем сообщение фрилансеру
                    $data=  array(
                        'title'=>Yii::app()->name . ' - новое сообщение',
                        'data'=>'менеджер '.C_User::getUsernameByID($modelProjectProposal->user_id).' оставил новое сообщение в проекте.'
                    );
                    C_Mail::sendMailUserByID('simple',
                        Yii::app()->name . ' - новое сообщение',
                        $modelProjectProposal->user_id,
                        $data
                    );
                }



                $this->redirect(array('index', 'project_proposal_id' => $project_proposal_id));
            }
        }

        $form = new ProjectsProposalComments();
        $model = ProjectsProposalComments::model()->findAll('project_proposal_id = :ppID', array(':ppID' => $project_proposal_id));



        // находим пользователя, который выбран исполнителем по предложени.
        $findAssignUser = ProjectsAssignUser::model()->findAll(
            'project_proposal_id = :ppID',
            array(
                ':ppID' => $project_proposal_id
            )
        );


        // помечаем все сообщения чужих пользователей как прочитанные
        ProjectsProposalComments::model()->updateAll(
            array('status' => ProjectsProposalComments::STATUS_READ),
            'project_proposal_id = :pID AND user_id NOT IN (:uID)',
            array(
                ':pID' => $project_proposal_id,
                ':uID' => Yii::app()->user->id
                )
        );


        $this->render('index',
            array('model' => $model,
                'form' => $form,
                'modelProjectProposal' => $modelProjectProposal,
                //'modelProjects' => $modelProjects // сам проект,
                'findAssignUser' => $findAssignUser // пользователь, которого выбрали
            ));

	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new ProjectsProposalComments('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ProjectsProposalComments']))
			$model->attributes=$_GET['ProjectsProposalComments'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ProjectsProposalComments the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ProjectsProposalComments::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ProjectsProposalComments $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='projects-proposal-comments-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
