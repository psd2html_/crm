<?php

class AdminController extends RController
{
    public $layout='//layouts/main';

    public function filters()
    {
        return array(
            'rights',
        );
    }


    /**
     * Для администратора - список фрилансеров в формате
     */
    public function actionFreelancers()
    {
        // вытаскиваем всех фрилансеров
        $modelAllFr = Yii::app()->db->createCommand()
            ->select('*')
            ->from('AuthAssignment aa')
            ->join('tbl_users u', 'u.id = aa.userid')
            ->join('tbl_profiles p', 'p.user_id = u.id')
            ->where('aa.itemname = "Freelancer"')
            ->queryAll();

        if ($modelAllFr)
        {
            $data = '';
            foreach ($modelAllFr as $freelancer)
            {
                if ($freelancer['first_name'])
                    $data .= $freelancer['first_name'] . ',' . $freelancer['email'];
                else
                    $data .= $freelancer['username']. ',' . $freelancer['email'];

                $data .= "<br />";
            }
        }

        $this->render('index',array(
            'data'=> $data,
        ));

    }
}