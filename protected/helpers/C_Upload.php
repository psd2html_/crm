<?php
/**
 * Created by PhpStorm.
 * User: astepochkin
 * Date: 16.03.2017
 * Time: 20:02
 */

class C_Upload
{
    public static function uploadFiles($model)
    {
        $folder = C_Base::getBasePath() . Yii::app()->params['uploads_projects'];

        $model->files = CUploadedFile::getInstances($model, 'file');

        if (!is_dir($folder . $model->id))
            mkdir($folder . $model->id, 0777);

        foreach($model->files as $file)
        {
            $path = $folder . $model->id . '/' . $file->getName();
            $file->saveAs($path);
        }
    }
}
