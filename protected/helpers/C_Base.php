<?php
/**
 * Created by PhpStorm.
 * User: astepochkin
 * Date: 16.03.2017
 * Time: 20:11
 */
class C_Base
{
    public static function getBasePath()
    {
        $path = Yii::getPathOfAlias('webroot');
        return $path;
    }


    /**
     * �������� domain
     */
    public static function getBaseURL()
    {
        //return Yii::app()->request->baseURL; // �� ��������
        return Yii::app()->getBaseUrl(true);
    }


    public static function getRealIpAddr()
    {
        if (!empty($_SERVER['HTTP_X_REAL_IP']))
        {
            $ip = $_SERVER['HTTP_X_REAL_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }


}