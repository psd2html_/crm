<?php
/**
 * Created by PhpStorm.
 * User: astepochkin
 * Date: 15.03.2017
 * Time: 20:57
 */
class C_User
{
    public static function getUsernameByID($user_id)
    {
        $user = User::model()->findByPk($user_id);

        if (isset($user->username) and !empty($user->username))
            return $user->username;
        else
            return '';
    }


    public static function getCurrentUser()
    {
        return User::model()->with('profile')->findByPk(Yii::app()->user->id);
    }


    /**
     * Определяем email менеджера по ID проекта
     *
     * @param $project_id
     * @return bool
     */
    public static function getManagerEmailByProjectID($project_id)
    {
        $model = Projects::model()->with('LinkUser')->findByPk($project_id);
        if ($model)
        {
            if ($model->LinkUser['email'])
                return $model->LinkUser['email'];
            else
                return false;
        }
        else
            return false;
    }


    public static function getAvatarUserByID($user_id)
    {
        $user = User::model()->with('profile')->findByPk($user_id);
        if (isset($user->profile->avatar) and !empty($user->profile->avatar)) {
            return C_Base::getBaseURL() .'/'. $user->profile->avatar;
        }
        else
            return C_Base::getBaseURL() . '/' . Yii::app()->params['folder_user_avatar'] . '/' . 'avatar_default.png';

    }


    public static function getAvatarCurrentUser()
    {
        $user = self::getCurrentUser();
        if (isset($user->profile->avatar) and !empty($user->profile->avatar)) {
            return C_Base::getBaseURL() .'/'. $user->profile->avatar;
        }
        else
            return C_Base::getBaseURL() . '/' . Yii::app()->params['folder_user_avatar'] . '/' . 'avatar_default.png';
    }

    /**
     * Все непрочитанные заявки для менеджера
     */
    public static function getUnreadProposalForManager()
    {
        $model = Projects::model()->with('LinkProposal', 'proposalCount')->findAll(
            'LinkProposal.status = :status AND t.user_id = :uID',
            array(
                ':uID' => Yii::app()->user->id,
                ':status' => ProjectsProposal::STATUS_UNREAD
            )
        );
        return $model;
    }


    /**
     * Все непрочитанные комментарии для менеджера
     */
    public static function getUnreadCommentsForManager()
    {
        $model = ProjectsProposal::model()->with('ppComments', 'ppCountComments', 'lProjects')->findAll(
            'ppComments.status = :status AND ppComments.user_id NOT IN (:uID) AND lProjects.user_id = :uID2',
            array(
                ':uID' => Yii::app()->user->id,
                ':uID2' => Yii::app()->user->id,
                ':status' => ProjectsProposal::STATUS_UNREAD
            )
        );
        return $model;
    }


    /**
     * Все непрочитанные сообщения для фрилансера
     */
    public static function getUnreadCommentsForFreelancer()
    {
        $model = ProjectsProposal::model()->with('ppComments', 'ppCountComments')->findAll(
            'ppComments.status = :status AND t.user_id IN (:uID) AND ppComments.user_id NOT IN (:uID)',
            array(
                ':uID' => Yii::app()->user->id,
                ':status' => ProjectsProposal::STATUS_UNREAD
            )
        );

//        echo '<pre>';
//        print_r($model);
//        echo '</pre>';
//        die('stop');

        return $model;
    }


    /**
     * Все сообщение по проекту
     *
     * @param $id
     * @return mixed
     */
    public static function getUnreadCommentsByProjectID($id)
    {
        $model = ProjectsProposalComments::model()->findAll(
            't.user_id NOT IN (:uID) AND t.project_id = :pID',
            array(
                ':pID' => $id,
                ':uID' => Yii::app()->user->id,
            )
        );

        return $model;
    }


    /**
     * Все сообщение по проекту
     *
     * @param $id
     * @return mixed
     */
    public static function getUnreadCommentsFreelancerByProjectID($id)
    {
        $model = ProjectsProposalComments::model()->with('ProjectProposal')->findAll(
            'ProjectProposal.user_id = :uID AND t.project_id = :pID',
            array(
                ':pID' => $id,
                ':uID' => Yii::app()->user->id,
            )
        );

        return $model;
    }


    public static function inJob()
    {
        $model = Projects::model()->with('pAssignUser')->findAll(
              't.status = :status AND pAssignUser.user_id = :uID',
            array(
                ':uID' => Yii::app()->user->id,
                ':status' => Projects::STATUS_ASSIGN
            )
        );

        if (isset($model) and !empty($model))
        {
            $count = count($model);
            return '<span class="label label-danger">'. $count  .'</span>';
        }

    }


}