<?php
/**
 * Created by PhpStorm.
 * User: astepochkin
 * Date: 20.03.2017
 * Time: 20:16
 */
class C_Image
{
    /**
     * ��������� ����������� ����� �����
     *
     * @param $name - file name
     * @return string
     */
    public static function uniqName($name)
    {
        $imageFileName = substr(md5(uniqid(rand(), true)), 0, rand(7, 13)).preg_replace('/(^.*)(\.)/','$2', $name);
        return $imageFileName;
    }


    /**
     * Upload images
     *
     * @param $obj - file object
     * @param $imageFileName file name
     */
    public static function saveImage($obj, $folder, $imageFileName, $size = 'large')
    {
        if ($size == 'big')
        {
            $width = Yii::app()->params['thumbnails']['b_width'];
            $height = Yii::app()->params['thumbnails']['b_height'];
        }
        else
        {
            $width = Yii::app()->params['thumbnails']['l_width'];
            $height = Yii::app()->params['thumbnails']['l_height'];
        }

        $obj->saveAs($folder . '/' . $imageFileName);
        $image = Yii::app()->image->load($folder . '/' . $imageFileName);
        $image->resize($width, $height, Image::NONE);
        $image->save($folder . '/' . $imageFileName);
    }
}