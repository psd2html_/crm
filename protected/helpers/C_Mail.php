<?php

/**
 * Class C_Mail
 */
class C_Mail extends YiiMailer
{
    /**
     * ПРостое письмоа
     *
     * @param $subject
     * @param $data
     * @throws CException
     * @throws Exception
     * @throws phpmailerException
     */
    public static function sendSimpleMail($subject, $email, $data)
    {
        if (Yii::app()->params['disable_send_mail'] == false) {
            $mail = new YiiMailer();
            $mail->setLayout('mail');
            $mail->setView('simple');
            $mail->setData($data);
            $mail->setFrom(Yii::app()->params['noreplyEmail'], Yii::app()->name . ' - Уведомление');
            $mail->setTo($email);
            $mail->setSubject($subject);
            $mail->send();
        }
    }



    /**
     * Отправка по email (1)
     *
     * @param $subject
     * @param $user_id
     * @param $messages
     */
    public static function sendMailUser($view = 'simple', $subject, $email, $data, $add_Subject = null)
    {
        if (Yii::app()->params['disable_send_mail'] == false)
        {
            //$user = Users::model()->findByPk($user_id);
            if ($email)
            {
                $mail = new YiiMailer();
                $mail->setLayout('mail');
                $mail->setView($view);
                $mail->setData($data);
                $mail->setFrom(Yii::app()->params['noreplyEmail'], Yii::app()->name . $add_Subject);
                $mail->setTo($email);
                $mail->setSubject($subject);
                //$mail->setBody($messages);

                if ($mail->send())
                {
                    // log
                    //C_Log::addLog(Log::TYPE_INFO, null, 'Отправка почты ' . $email);
                    return true;
                }
                else
                {
                    // log
                    //C_Log::addLog(Log::TYPE_INFO, null, 'Невозможно отправить почту ' . $email);
                    return false;
                }
            }
        }
        else
        {
            return true;
        }
    }


    /**
     * Шлем сообщение всем фрилансерам
     */
    public static function sendAllFreelancers($subject, $data, $view = 'simple')
    {
        if (Yii::app()->params['disable_send_mail'] == false)
        {
            // вытаскиваем всех фрилансеров
            $modelAllFr = Yii::app()->db->createCommand()
                ->select('*')
                ->from('AuthAssignment aa')
                ->join('tbl_users u', 'u.id = aa.userid')
                ->where('aa.itemname = "Freelancer"')
                ->queryAll();

            if ($modelAllFr)
            {
                foreach ($modelAllFr as $freelancer)
                {
                    if ($freelancer['email'])
                    {
                        $mail = new YiiMailer();
                        $mail->setLayout('mail');
                        $mail->setView($view);
                        $mail->setData($data);
                        $mail->setFrom(Yii::app()->params['noreplyEmail'], Yii::app()->name);
                        $mail->setTo($freelancer['email']);
                        $mail->setSubject($subject);
                        $mail->send();
                    }
                }
            }
            else
                return true;
        }
        else
            return true;
    }


    /**
     * Отправка по $user_id
     *
     * @param $subject
     * @param $user_id
     * @param $messages
     */
    public static function sendMailUserByID($view, $subject, $user_id, $data, $add_Subject = null)
    {
        if (Yii::app()->params['disable_send_mail'] == false)
        {
            if (isset($user_id) and !empty($user_id))
            {
                $user = User::model()->findByPk($user_id);

                $mail = new YiiMailer();
                $mail->setLayout('mail');
                $mail->setView($view);
                $mail->setData($data);
                $mail->setFrom(Yii::app()->params['noreplyEmail'], Yii::app()->name . $add_Subject);
                $mail->setTo($user->email);
                $mail->setSubject($subject);
                if ($mail->send())
                {
                    // log
                    //C_Log::addLog(Log::TYPE_INFO, null, 'Отправка почты ' . $user->email);
                    return true;
                }
                else
                {
                    // log
                    //C_Log::addLog(Log::TYPE_INFO, null, 'Невозможно отправить почту ' . $user->email);
                    return false;
                }
            }
        }
        else
        {
            return true;
        }
    }


    /**
     * Отправка почты Супер-администратору
     *
     * @param $data
     */
    public static function sendMailSuperAdmin($data)
    {
        if (Yii::app()->params['disable_send_mail'] == false)
        {
            $view = 'root_notification';
            $subject = 'Artwebit.CRM';
            $email = Yii::app()->params['adminEmail'];
            if (isset($email) and !empty($email))
            {
                $mail = new YiiMailer();
                $mail->setLayout('mail');
                $mail->setView($view);
                $mail->setData($data);
                $mail->setFrom(Yii::app()->params['noreplyEmail'], Yii::app()->name . ' - Уведомление');
                $mail->setTo($email);
                $mail->setSubject($subject);
                $mail->send();

                // log
                //C_Log::addLog(Log::TYPE_INFO, null, 'Отправка почты супер-администратору.');
            }
        }
    }


    public static function sendContactForm($data)
    {
        if (Yii::app()->params['disable_send_mail'] == false)
        {
            $view = 'contact';
            $subject = 'Artwebit.CRM';
            $email = Yii::app()->params['adminEmail'];
            if (isset($email) and !empty($email))
            {
                $mail = new YiiMailer();
                $mail->setLayout('mail');
                $mail->setView($view);
                $mail->setData($data);
                $mail->setFrom(Yii::app()->params['noreplyEmail'], Yii::app()->name . ' - Уведомление');
                $mail->setTo($email);
                $mail->setSubject($subject);

                if ($mail->send())
                    return true;
                else
                    return false;

                // log
                //C_Log::addLog(Log::TYPE_INFO, null, 'Отправка почты супер-администратору.');
            }
        }
        return false;
    }
}