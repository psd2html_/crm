<?php

/**
 * Class C_Rights - extends Rights module
 */
class C_Rights extends Rights
{
    /**
     * @param null $userId -  user ID
     * @param array $roles - roles on system (get Rights module)
     * @return bool
     */
    public static function checkRole($roles = array())
    {
        $role_name = '';
        $as_role = parent::getAssignedRoles(Yii::app()->user->id);

        foreach ($as_role as $_role)
        {
            $role_name = $_role->name;
        }

        if (isset($role_name) and !empty($role_name))
        {
            if (in_array($role_name, $roles)) {
                return true;
            } else
                return false;
        } else {
            return false;
        }
    }


    /**
     * Определеяем роль пользователя
     * возвращаем true если пользователь соотв. нужной роли
     *
     * @param $id - user ID
     * @param array $roles - name role
     * @return bool
     */
    public static function checkRoleByID($id, $roles = array())
    {
        $as_role = parent::getAssignedRoles($id);
        foreach ($as_role as $_role) {
            $role_name = $_role->name;
        }

        if (isset($role_name) and !empty($role_name))
        {
            if (in_array($role_name, $roles)) {
                return true;
            } else
                return false;
        } else {
            return false;
        }
    }


    /**
     * Текущий пользователь фрилансер
     */
    public static function isFreelancer()
    {
        return self::checkRoleCurrentUser('freelancer');
    }


    /**
     * Определеяем роль текущего пользователя
     * возвращаем true если пользователь соотв. нужной роли
     *
     * @param $id - user ID
     * @param array $roles - name role
     * @return bool
     */
    public static function checkRoleCurrentUser($role)
    {
        $as_role = parent::getAssignedRoles(Yii::app()->user->id);

        foreach ($as_role as $_role) {
            $role_name = $_role->name;
        }

        if (isset($role_name) and !empty($role_name))
        {
            if ($role_name == Yii::app()->params['roles'][$role])
                return true;
            else
                return false;
        } else {
            return false;
        }
    }

}