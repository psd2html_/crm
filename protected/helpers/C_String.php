<?php
/**
 * Created by PhpStorm.
 * User: astepochkin
 * Date: 26.03.2017
 * Time: 17:47
 */
class C_String
{
    /**
     * ��������� �� ������
     *
     * @param $content - ������������ �����
     * @param int $quantity_of_words - ���-�� ���� ��� ������
     * @return string
     */
    public static function printContent( $content, $quantity_of_words = 20 )
    {
        $text = strip_tags( $content );
        $blah = explode( ' ', $text );
        if ( count( $blah ) > $quantity_of_words ) {
            $current_quantity_of_words = $quantity_of_words;
            $use_dotdotdot = 1;
        } else {
            $current_quantity_of_words = count( $blah );
            $use_dotdotdot = 0;
        }
        $excerpt = '';
        for ( $i = 0; $i < $current_quantity_of_words; $i++ ) {
            if ( $i + 1 == $current_quantity_of_words and $use_dotdotdot ) {
                $excerpt .= $blah[$i] . '...';
            } else {
                $excerpt .= $blah[$i] . ' ';
            }
        }
        return $excerpt;
    }
}