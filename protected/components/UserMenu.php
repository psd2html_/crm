<?php

Yii::import('zii.widgets.CPortlet');

/**
 * Class UserMenu - Common top-footer menu
 */
class UserMenu extends CPortlet
{
    public function init()
    {
        parent::init();
    }

    protected function renderContent()
    {
        $this->render('userMenu');
    }
}