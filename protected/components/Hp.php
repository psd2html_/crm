<?php

/**
 * Class Hp - my helpers
 */
class Hp
{
	/**
	 * Get the current user
	 *
	 * @return WebUser
	 */
	public static function user() {
		return Yii::app ()->user;
	}


	/**
	 * Get the current request
	 *
	 * @return CHttpRequest
	 */
	public static function request() {
		return Yii::app ()->request;
	}


	/**
	 * Get the current client script
	 *
	 * @return client script
	 */
	public static function cs() {
		return Yii::app ()->getClientScript();
	}
	
	/**
	 * A shorthand method for getting the AssetManager
	 */
	public static function am() {
		return Yii::app ()->getAssetManager ();
	}


	/**
	 * A shorthand method for getting value of a param
	 *
	 * @return mixed
	 */
	public static function param() {
		$value = Yii::app ()->params [func_get_arg ( 0 )];
		$totalArgs = func_num_args ();
		if ($totalArgs > 1) {
			$i = 1;
			do {
				$value = $value [func_get_arg ( $i )];
				$i ++;
			} while ( $i < $totalArgs );
		}
		return $value;
	}


	/**
	 * An extend method for the built-in md5
	 *
	 * @return string 32 bytes string
	 */
	public static function md5($str) {
		if ($str)
			$str = md5 ( $str . self::param ( 'salt' ) );
		return $str;
	}
	
	/**
	 * Get website's base url
	 *
	 * @return string
	 */
	public static function baseUrl() {
		return Yii::app ()->request->baseUrl;
	}


	/**
	 * Get website's theme url
	 *
	 * @return string
	 */
	public static function themeUrl() {
		return Yii::app ()->theme->baseUrl;
	}


	/**
	 * A shorthand method for creating absolute app urls
	 *
	 * @param string $route        	
	 * @param array $params        	
	 * @return string
	 */
	public static function url($route, $params = array(), $absolute = false) {
		if ($absolute)
			return Yii::app ()->createAbsoluteUrl ( $route, $params );
		return Yii::app ()->createUrl ( $route, $params );
	}
}